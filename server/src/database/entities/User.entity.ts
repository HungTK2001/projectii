import { identity } from 'rxjs';
import { nowInMillis } from 'src/shared/Utils';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  public id: number;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 90,
    unique: true,
    nullable: false,
  })
  public username: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 190,
    nullable: true,
    unique: true,
  })
  public email: string;
  @Column({
    name: 'full_name',
    type: 'nvarchar',
    length: 190,
    nullable: false,
  })
  public fullName: string;

  @Column({ name: 'password', type: 'varchar', length: 90, nullable: false })
  public password: string;

  @Column({ name: 'role_id', type: 'int', nullable: false })
  public roleID: number;

  @Column({ name: 'create_at', type: 'bigint', nullable: true })
  public createAt: number;

  @Column({ name: 'update_at', type: 'bigint', nullable: true })
  public updateAt: number;

  @BeforeInsert()
  public updateCreateDates() {
    this.createAt = nowInMillis();
    this.updateAt = nowInMillis();
  }

  @BeforeUpdate()
  public updateUpdateDates() {
    this.updateAt = nowInMillis();
  }
}
