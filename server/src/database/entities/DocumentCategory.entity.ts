import { nowInMillis } from 'src/shared/Utils';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('document_category')
export class DocumentCategory {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  public id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 200,
    unique: true,
    nullable: false,
  })
  public name: string;

  @Column({
    name: 'detail',
    type: 'text',
    unique: false,
    nullable: false,
  })
  public detail: string;

  @Column({
    name: 'more_detail',
    type: 'text',
    unique: false,
    nullable: false,
  })
  public moreDetail: string;

  @Column({ name: 'create_at', type: 'bigint', nullable: false })
  public createAt: number;

  @Column({ name: 'update_at', type: 'bigint', nullable: false })
  public updateAt: number;

  @BeforeInsert()
  public updateCreateDates() {
    this.createAt = nowInMillis();
    this.updateAt = nowInMillis();
  }

  @BeforeUpdate()
  public updateUpdateDates() {
    this.updateAt = nowInMillis();
  }
}
