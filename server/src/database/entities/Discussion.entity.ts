import { nowInMillis } from 'src/shared/Utils';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('discussion')
export class Discussion {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  public id: number;

  @Column({ name: 'title', type: 'varchar', nullable: false })
  public title: string;

  @Column({ name: 'content', type: 'varchar', nullable: false })
  public content: string;

  @Column({ name: 'image_url', type: 'varchar', nullable: true })
  public imageUrl: string;

  @Column({ name: 'views', type: 'int', nullable: true, default: 0 })
  public views: number;

  @Column({ name: 'user_id', type: 'int', nullable: false })
  public userID: number;

  @Column({ name: 'discussion_category_id', type: 'int', nullable: false })
  public discussionCategoryID: number;

  @Column({ name: 'create_at', type: 'bigint', nullable: false })
  public createAt: number;

  @Column({ name: 'update_at', type: 'bigint', nullable: false })
  public updateAt: number;

  @BeforeInsert()
  public updateCreateDates() {
    this.createAt = nowInMillis();
    this.updateAt = nowInMillis();
  }

  @BeforeUpdate()
  public updateUpdateDates() {
    this.updateAt = nowInMillis();
  }
}
