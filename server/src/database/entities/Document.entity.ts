import { nowInMillis } from 'src/shared/Utils';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('document')
export class Document {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  public id: number;

  @Column({ name: 'name', type: 'varchar', nullable: false })
  public name: string;

  @Column({ name: 'title', type: 'varchar', nullable: false })
  public title: string;

  @Column({
    name: 'google_drive_link',
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  public googleDriveLink: string;

  @Column({ name: 'file_id', type: 'varchar', nullable: false })
  public fileID: string;

  @Column({ name: 'document_category_id', type: 'int', nullable: false })
  public documentCategoryID: number;

  @Column({ name: 'views', type: 'int', default: 0 })
  public views: number;

  @Column({ name: 'create_at', type: 'bigint', nullable: false })
  public createAt: number;

  @Column({ name: 'update_at', type: 'bigint', nullable: false })
  public updateAt: number;

  @BeforeInsert()
  public updateCreateDates() {
    this.createAt = nowInMillis();
    this.updateAt = nowInMillis();
  }

  @BeforeUpdate()
  public updateUpdateDates() {
    this.updateAt = nowInMillis();
  }
}
