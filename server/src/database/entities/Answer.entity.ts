import { nowInMillis } from 'src/shared/Utils';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('answer')
export class Answer {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  public id: number;

  @Column({ name: 'content', type: 'varchar', nullable: false })
  public content: string;

  @Column({ name: 'image_url', type: 'varchar', nullable: true })
  public imageUrl: string;

  @Column({ name: 'is_correct', type: 'tinyint', width: 1, nullable: false })
  public isCorrect: boolean;

  @Column({ name: 'question_id', type: 'int', nullable: false })
  public questionID: number;

  @Column({ name: 'create_at', type: 'bigint', nullable: false })
  public createAt: number;

  @Column({ name: 'update_at', type: 'bigint', nullable: false })
  public updateAt: number;

  @BeforeInsert()
  public updateCreateDates() {
    this.createAt = nowInMillis();
    this.updateAt = nowInMillis();
  }

  @BeforeUpdate()
  public updateUpdateDates() {
    this.updateAt = nowInMillis();
  }
}
