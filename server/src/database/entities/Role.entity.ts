import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('role')
export class Role {
  @PrimaryColumn({ name: 'id', type: 'int' })
  id: number;

  @Column({ name: 'name', type: 'varchar', nullable: false })
  name: string;
}
