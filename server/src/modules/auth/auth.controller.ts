import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { AuthService } from './auth.service';
import { LoginRequestDTO } from './dto/request/login.dto';
import { RegisterRequestDTO } from './dto/request/register.dto';
import { UpdatePasswordDTO } from './dto/request/update-password.dto';

@ApiTags('Auth')
@Controller({
  path: 'auth',
  version: '1',
})
export class AuthController {
  constructor(public service: AuthService) {}

  @Post('/login')
  @HttpCode(HttpStatus.OK)
  public async login(@Body() loginRequestDTO: LoginRequestDTO) {
    return this.service.validateLogin(loginRequestDTO, false);
  }

  @Post('admin/login')
  @HttpCode(HttpStatus.OK)
  public async adminLogin(@Body() LoginRequestDTO: LoginRequestDTO) {
    return this.service.validateLogin(LoginRequestDTO, true);
  }

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  async register(@Body() createUserDTO: RegisterRequestDTO) {
    return this.service.register(createUserDTO);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @Roles(RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post(':id/change-password')
  @HttpCode(HttpStatus.OK)
  async changePassword(
    @Param('id') userID: number,
    @Body() updatePasswordDTO: UpdatePasswordDTO,
  ) {
    return this.service.updatePassword(userID, updatePasswordDTO);
  }
}
