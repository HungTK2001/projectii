import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { infinityPagination } from 'src/shared/utils/infinity_pagination';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { QuizCategoryService } from './quiz-category.service';
import { CreateQuizCategoryRequestDto } from './dto/request/create-quiz-category.dto';
import { UpdateQuizCategoryRequestDto } from './dto/request/update-quiz-category.dto';

@ApiTags('quiz category')
@Controller({
  path: 'quiz-categories',
  version: '1',
})
export class QuizCategoryController {
  constructor(private readonly quizCategoriesService: QuizCategoryService) {}

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createQuizCategoryDto: CreateQuizCategoryRequestDto) {
    return this.quizCategoriesService.create(createQuizCategoryDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    if (limit > 50) {
      limit = 50;
    }
    const findManyResult =
      await this.quizCategoriesService.findManyWithCondition({});
    return infinityPagination(
      await this.quizCategoriesService.findManyWithPagination({
        page,
        limit,
      }),
      { page, limit },
      findManyResult.length,
    );
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.quizCategoriesService.findOne({ id: +id });
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  update(
    @Param('id') id: number,
    @Body() updateQuizCategoryDto: UpdateQuizCategoryRequestDto,
  ) {
    return this.quizCategoriesService.update(id, updateQuizCategoryDto);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.quizCategoriesService.softDelete(id);
  }
}
