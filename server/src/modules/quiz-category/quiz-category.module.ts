import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuizCategory } from 'src/database/entities/QuizCategory.entity';
import { QuizzesModule } from '../quizzes/quizzes.module';
import { QuizCategoryController } from './quiz-category.controller';
import { QuizCategoryService } from './quiz-category.service';

@Module({
  imports: [TypeOrmModule.forFeature([QuizCategory]), QuizzesModule],
  controllers: [QuizCategoryController],
  providers: [QuizCategoryService],
  exports: [QuizCategoryService],
})
export class QuizCategoriesModule {}
