import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QuizCategory } from 'src/database/entities/QuizCategory.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { QuizzesService } from '../quizzes/quizzes.service';
import { CreateQuizCategoryRequestDto } from './dto/request/create-quiz-category.dto';
import { UpdateQuizCategoryRequestDto } from './dto/request/update-quiz-category.dto';

@Injectable()
export class QuizCategoryService {
  constructor(
    @InjectRepository(QuizCategory)
    private quizCategoriesRepository: Repository<QuizCategory>,
    private quizService: QuizzesService,
  ) {}

  create(createQuizCategoryDto: CreateQuizCategoryRequestDto) {
    return this.quizCategoriesRepository.save(
      this.quizCategoriesRepository.create(createQuizCategoryDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.quizCategoriesRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: EntityCondition<QuizCategory>,
  ): Promise<QuizCategory[] | undefined> {
    return this.quizCategoriesRepository.find({ where: fields });
  }

  async findOne(fields: EntityCondition<QuizCategory>) {
    const quizCategory = await this.quizCategoriesRepository.findOne({
      where: fields,
    });
    const quiz = await this.quizService.findManyWithCondition({
      quizCategoryID: quizCategory.id,
    });
    return {
      ...quizCategory,
      quiz: quiz,
    };
  }

  async update(
    id: number,
    updateQuizCategoryDto: UpdateQuizCategoryRequestDto,
  ): Promise<UpdateQuizCategoryRequestDto> {
    let quizCategory = await this.quizCategoriesRepository.findOne(id);
    return this.quizCategoriesRepository.save({
      ...quizCategory,
      ...updateQuizCategoryDto,
      updateAt: Date.now(),
    });
  }
  async softDelete(id: number): Promise<void> {
    await this.quizCategoriesRepository.delete(id);
  }
}
