import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateQuizCategoryRequestDto {
  @ApiProperty({
    type: String,
    example: 'Math',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: String,
    example: 'Math',
  })
  @IsNotEmpty()
  detail: string;

  @ApiProperty({
    type: String,
    example: 'Math',
  })
  @IsNotEmpty()
  moreDetail: string;
}
