import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Quiz } from 'src/database/entities/Quiz.entity';
import { QuizCategory } from 'src/database/entities/QuizCategory.entity';
import { User } from 'src/database/entities/User.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { AnswerService } from '../answer/answer.service';
import { QuestionService } from '../question/question.service';
import { QuizResultService } from '../quiz-result/quiz-result.service';
import { CreateQuizRequestDto } from './dto/request/create-quiz.dto';
import { UpdateQuizRequestDto } from './dto/request/update-quiz.dto';

@Injectable()
export class QuizzesService {
  constructor(
    @InjectRepository(Quiz)
    private quizRepository: Repository<Quiz>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(QuizCategory)
    private quizCategoryRepository: Repository<QuizCategory>,
    private questionService: QuestionService,
    private quizResultService: QuizResultService,
    private answerService: AnswerService,
  ) {}

  create(createQuizDto: CreateQuizRequestDto) {
    return this.quizRepository.save(this.quizRepository.create(createQuizDto));
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.quizRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  checkAnswer = async (userAnswer: any) => {
    const { userID, quizID, answers, quizStartAt, quizEndAt } = userAnswer;
    let trueAnswer = 0;
    for (const questionID in answers) {
      if (answers[questionID]) {
        let question = await this.questionService.findOne({
          id: parseInt(questionID),
        });
        let answerIDs = [];
        let resultAnswers = question.answers;
        for (let i = 0; i < question.answers.length; i++) {
          if (resultAnswers[i].isCorrect == true) {
            answerIDs.push(resultAnswers[i].id.toString());
          }
        }
        if (typeof answers[questionID] === 'string') {
          let arrayCheck = answerIDs.filter((answerID) =>
            answers[questionID].split(' ').includes(answerID),
          );
          if (arrayCheck.toString() == answerIDs.toString()) {
            trueAnswer++;
          }
        } else {
          let arrayCheck = answerIDs.filter((answerID) =>
            answers[questionID].includes(answerID),
          );
          if (arrayCheck.toString() == answerIDs.toString()) {
            trueAnswer++;
          }
        }
      }
    }
    const result = await this.quizResultService.create({
      userID: userID,
      quizID: quizID,
      score: trueAnswer * 10,
      quizStartAt: quizStartAt,
      quizEndAt: quizEndAt,
    });
    return result;
  };

  async findQuestionForUserWithQuizID(quizID: number) {
    const result = [];
    const questions = await this.questionService.findManyWithCondition({
      quizID: quizID,
    });
    const quiz = await this.quizRepository.findOne({ id: quizID });
    const user = await this.userRepository.findOne({
      where: {
        id: quiz.userID,
      },
    });

    const quizCategory = await this.quizCategoryRepository.findOne({
      where: { id: quiz.quizCategoryID },
    });
    let answers = null;
    for (let question of questions) {
      let answerUsers = [];
      let numOfTrue = 0;
      answers = await this.answerService.findManyWithCondition({
        questionID: question.id,
      });
      for (let i = 0; i < answers.length; i++) {
        if (answers[i].isCorrect == 1) numOfTrue++;
        delete answers[i]['isCorrect'];
        answerUsers.push(answers[i]);
      }
      result.push({
        multiChoice: numOfTrue > 1 ? true : false,
        ...question,
        answers: answerUsers,
      });
    }
    return {
      ...quiz,
      author: user.fullName,
      quizCategory: quizCategory,
      questions: result,
    };
  }

  async findQuestionWithQuizID(quizID: number) {
    const result = [];
    const questions = await this.questionService.findManyWithCondition({
      quizID: quizID,
    });
    const quiz = await this.quizRepository.findOne({ id: quizID });
    const user = await this.userRepository.findOne({
      where: {
        id: quiz.userID,
      },
    });

    const quizCategory = await this.quizCategoryRepository.findOne({
      where: { id: quiz.quizCategoryID },
    });
    let answer = null;
    for (let question of questions) {
      answer = await this.answerService.findManyWithCondition({
        questionID: question.id,
      });
      result.push({
        ...question,
        answers: answer,
      });
    }
    return {
      ...quiz,
      author: user.fullName,
      quizCategory: quizCategory,
      questions: result,
    };
  }

  findManyWithCondition(fields: Partial<Quiz>): Promise<Quiz[] | undefined> {
    return this.quizRepository.find({ where: fields });
  }

  async findOne(fields: EntityCondition<Quiz>) {
    const quiz = await this.quizRepository.findOne({ where: fields });
    const questions = await this.questionService.findQuestionWithQuizID(
      quiz.id,
    );
    const user = await this.userRepository.findOne({
      where: {
        id: quiz.userID,
      },
    });

    const quizCategory = await this.quizCategoryRepository.findOne({
      where: { id: quiz.quizCategoryID },
    });
    return {
      ...quiz,
      author: user?.fullName,
      quizCategory: quizCategory,
      questions: questions,
    };
  }

  async update(id: number, updateQuizDto: UpdateQuizRequestDto): Promise<Quiz> {
    let user = await this.quizRepository.findOne(id);
    return this.quizRepository.save({
      ...user,
      ...updateQuizDto,
      updateAt: Date.now(),
    });
  }

  async softDelete(id: number): Promise<void> {
    await this.quizRepository.softDelete(id);
  }

  async deleteByProps(props: Partial<Quiz>): Promise<void> {
    const quizzes = await this.findManyWithCondition(props);
    for (let i = 0; i < quizzes.length; i++) {
      await this.questionService.deleteByProps({ quizID: quizzes[i].id });
      await this.quizResultService.deleteByProps({ quizID: quizzes[i].id });
    }
    await this.quizRepository.delete(props);
  }
}
