import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CreateQuizRequestDto } from './create-quiz.dto';

export class UpdateQuizRequestDto extends PartialType(CreateQuizRequestDto) {
  @ApiProperty({
    type: String,
    example: 'Math quiz',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;

  @ApiProperty({
    type: Number,
    example: 10,
  })
  @IsNotEmpty()
  timeLimit: number;
}
