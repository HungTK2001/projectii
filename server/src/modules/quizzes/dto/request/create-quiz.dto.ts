import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateQuizRequestDto {
  @ApiProperty({
    type: String,
    example: 'Math quiz',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;

  @ApiProperty({
    type: Number,
    example: 10,
  })
  @IsNotEmpty()
  timeLimit: number;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  quizCategoryID: number;
}
