import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Quiz } from 'src/database/entities/Quiz.entity';
import { QuizCategory } from 'src/database/entities/QuizCategory.entity';
import { User } from 'src/database/entities/User.entity';
import { AnswerModule } from '../answer/answer.module';
import { QuestionsModule } from '../question/question.module';
import { QuizResultModule } from '../quiz-result/quiz-result.module';
import { QuizzesController } from './quizzes.controller';
import { QuizzesService } from './quizzes.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Quiz, User, QuizCategory]),
    QuestionsModule,
    QuizResultModule,
    AnswerModule,
  ],
  controllers: [QuizzesController],
  providers: [QuizzesService],
  exports: [QuizzesService],
})
export class QuizzesModule {}
