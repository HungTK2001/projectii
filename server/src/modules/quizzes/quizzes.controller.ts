import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { infinityPagination } from 'src/shared/utils/infinity_pagination';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { CreateQuizRequestDto } from './dto/request/create-quiz.dto';
import { UpdateQuizRequestDto } from './dto/request/update-quiz.dto';
import { QuizzesService } from './quizzes.service';

@ApiTags('Quizzes')
@Controller({
  path: 'quizzes',
  version: '1',
})
export class QuizzesController {
  constructor(private readonly quizzesService: QuizzesService) {}

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createQuizDto: CreateQuizRequestDto) {
    return this.quizzesService.create(createQuizDto);
  }
  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post('/check-answer')
  @HttpCode(HttpStatus.OK)
  checkAnswers(@Body() userAnswer: any) {
    return this.quizzesService.checkAnswer(userAnswer);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    if (limit > 50) {
      limit = 50;
    }
    const findManyResult = await this.quizzesService.findManyWithCondition({});
    return infinityPagination(
      await this.quizzesService.findManyWithPagination({
        page,
        limit,
      }),
      { page, limit },
      findManyResult.length,
    );
  }

  @Get('filterBy/:userID')
  @HttpCode(HttpStatus.OK)
  async findWithUserID(@Param('userID') userID: number) {
    const findManyResult = await this.quizzesService.findManyWithCondition({
      userID: userID,
    });
    return findManyResult;
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.quizzesService.findOne({ id: +id });
  }

  @Get(':id/details')
  @HttpCode(HttpStatus.OK)
  findWithDetails(@Param('id') id: string) {
    return this.quizzesService.findQuestionWithQuizID(parseInt(id));
  }
  @Get(':id/user-details')
  @HttpCode(HttpStatus.OK)
  findWithUserDetails(@Param('id') id: string) {
    return this.quizzesService.findQuestionForUserWithQuizID(parseInt(id));
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  update(@Param('id') id: number, @Body() updateQuizDto: UpdateQuizRequestDto) {
    return this.quizzesService.update(id, updateQuizDto);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.quizzesService.deleteByProps({ id: id });
  }
}
