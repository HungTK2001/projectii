import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { infinityPagination } from 'src/shared/utils/infinity_pagination';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { DiscussionService } from './discussion.service';
import { CreateDiscussionRequestDto } from './dto/request/create-discussion.dto';
import { UpdateDiscussionRequestDto } from './dto/request/update-discussion.dto';

@ApiTags('Discussions')
@Controller({
  path: 'discussions',
  version: '1',
})
export class DiscussionsController {
  constructor(private readonly discussionsService: DiscussionService) {}

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createDiscussionDto: CreateDiscussionRequestDto) {
    return this.discussionsService.create(createDiscussionDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    if (limit > 50) {
      limit = 50;
    }
    const findManyResult = await this.discussionsService.findManyWithCondition(
      {},
    );
    return infinityPagination(
      await this.discussionsService.findManyWithPagination({
        page,
        limit,
      }),
      { page, limit },
      findManyResult.length,
    );
  }

  @Get('/filterBy/:userID')
  @HttpCode(HttpStatus.OK)
  async findWithUserID(@Param('userID') userID: number) {
    console.log(userID);
    const findManyResult = await this.discussionsService.findManyWithCondition({
      userID: userID,
    });
    return findManyResult;
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.discussionsService.findOne({ id: +id });
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  update(
    @Param('id') id: number,
    @Body() updateDiscussionDto: UpdateDiscussionRequestDto,
  ) {
    return this.discussionsService.update(id, updateDiscussionDto);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.discussionsService.deleteByProps({ id: id });
  }
}
