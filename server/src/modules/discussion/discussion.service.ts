import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Discussion } from 'src/database/entities/Discussion.entity';
import { DiscussionCategory } from 'src/database/entities/DiscussionCategory.entity';
import { User } from 'src/database/entities/User.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { CommentService } from '../comment/comment.service';
import { CreateDiscussionRequestDto } from './dto/request/create-discussion.dto';
import { UpdateDiscussionRequestDto } from './dto/request/update-discussion.dto';

@Injectable()
export class DiscussionService {
  constructor(
    @InjectRepository(Discussion)
    private discussionsRepository: Repository<Discussion>,
    private commentService: CommentService,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(DiscussionCategory)
    private discussionCategoryRepository: Repository<DiscussionCategory>,
  ) {}

  create(createDiscussionDto: CreateDiscussionRequestDto) {
    return this.discussionsRepository.save(
      this.discussionsRepository.create(createDiscussionDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.discussionsRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: Partial<Discussion>,
  ): Promise<Discussion[] | undefined> {
    return this.discussionsRepository.find({ where: fields });
  }

  async findOne(fields: EntityCondition<Discussion>) {
    const discussion = await this.discussionsRepository.findOne({
      where: fields,
    });

    const user = await this.userRepository.findOne({
      where: {
        id: discussion.userID,
      },
    });

    const discussionCategory = await this.discussionCategoryRepository.findOne({
      where: { id: discussion.discussionCategoryID },
    });

    const comment = await this.commentService.findManyWithCondition({
      discussionID: discussion.id,
    });

    return {
      ...discussion,
      author: user.fullName,
      discussionCategory: discussionCategory,
      comment: comment,
    };
  }

  async update(
    id: number,
    updateDiscussionDto: UpdateDiscussionRequestDto,
  ): Promise<Discussion> {
    let discussion = await this.discussionsRepository.findOne(id);
    return this.discussionsRepository.save({
      ...discussion,
      ...updateDiscussionDto,
      updateAt: Date.now(),
    });
  }
  async softDelete(id: number): Promise<void> {
    await this.discussionsRepository.softDelete(id);
  }
  async deleteByProps(props: Partial<Discussion>): Promise<void> {
    const discussions = await this.findManyWithCondition(props);
    for (let i = 0; i < discussions.length; i++) {
      await this.commentService.deleteAllByProperties({
        discussionID: discussions[i].id,
      });
    }
    await this.discussionsRepository.delete(props);
  }
}
