import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateDiscussionRequestDto {
  @ApiProperty({
    type: String,
    example: 'Math discussion',
  })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    type: String,
    example: 'Solve x+1=1',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'http://image.v.vv',
  })
  @IsOptional()
  imageUrl: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  discussionCategoryID: number;
}
