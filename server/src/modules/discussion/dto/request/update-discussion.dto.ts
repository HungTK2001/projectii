import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CreateDiscussionRequestDto } from './create-discussion.dto';

export class UpdateDiscussionRequestDto extends PartialType(
  CreateDiscussionRequestDto,
) {
  @ApiProperty({
    type: String,
    example: 'Math discussion',
  })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    type: String,
    example: 'Solve x+1=1',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'http://image.v.vv',
  })
  imageUrl: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  discussionCategoryID: number;
}
