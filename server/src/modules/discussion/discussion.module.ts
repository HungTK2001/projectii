import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Discussion } from 'src/database/entities/Discussion.entity';
import { DiscussionCategory } from 'src/database/entities/DiscussionCategory.entity';
import { User } from 'src/database/entities/User.entity';
import { CommentsModule } from '../comment/comment.module';
import { DiscussionsController } from './discussion.controller';
import { DiscussionService } from './discussion.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Discussion, DiscussionCategory, User]),
    CommentsModule,
  ],
  controllers: [DiscussionsController],
  providers: [DiscussionService],
  exports: [DiscussionService],
})
export class DiscussionModule {}
