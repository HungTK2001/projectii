import { Optional } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateAnswerRequestDto {
  @ApiProperty({
    type: String,
    example: 'x=0',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'Image url',
  })
  @Optional()
  imageUrl: string;

  @ApiProperty({
    type: Boolean,
    example: true,
  })
  @IsNotEmpty()
  isCorrect: boolean;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  questionID: number;
}
