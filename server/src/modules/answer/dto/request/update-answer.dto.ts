import { Optional } from '@nestjs/common';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateAnswerRequestDto {
  @ApiProperty({
    type: String,
    example: 'x=0',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'Image url',
  })
  @Optional()
  imageUrl: string;

  @ApiProperty({
    type: Boolean,
    example: true,
  })
  @IsNotEmpty()
  isCorrect: boolean;
}
