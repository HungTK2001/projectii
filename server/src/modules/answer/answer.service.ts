import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Answer } from 'src/database/entities/Answer.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { CreateAnswerRequestDto } from './dto/request/create-answer.dto';
import { UpdateAnswerRequestDto } from './dto/request/update-answer.dto';

@Injectable()
export class AnswerService {
  constructor(
    @InjectRepository(Answer)
    private answersRepository: Repository<Answer>,
  ) {}

  create(createAnswerDto: CreateAnswerRequestDto) {
    return this.answersRepository.save(
      this.answersRepository.create(createAnswerDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.answersRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: EntityCondition<Answer>,
  ): Promise<Answer[] | undefined> {
    return this.answersRepository.find({ where: fields });
  }

  findOne(fields: EntityCondition<Answer>): Promise<Answer | undefined> {
    return this.answersRepository.findOne({ where: fields });
  }

  async update(
    id: number,
    updateAnswerDto: UpdateAnswerRequestDto,
  ): Promise<Answer> {
    let answer = await this.answersRepository.findOne(id);
    return this.answersRepository.save({
      ...answer,
      ...updateAnswerDto,
      updateAt: Date.now(),
    });
  }
  async deleteMultiple(ids: Array<number>): Promise<void> {
    if (ids.length > 0) {
      await this.answersRepository.delete(ids);
    }
  }
  async deleteByProperties(fields: Partial<Answer>): Promise<void> {
    await this.answersRepository.delete(fields);
  }
}
