import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { infinityPagination } from 'src/shared/utils/infinity_pagination';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { AnswerService } from './answer.service';
import { CreateAnswerRequestDto } from './dto/request/create-answer.dto';
import { UpdateAnswerRequestDto } from './dto/request/update-answer.dto';

@ApiTags('Answer')
@Controller({
  path: 'answers',
  version: '1',
})
export class AnswerController {
  constructor(private readonly answerService: AnswerService) {}

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createAnswerDto: CreateAnswerRequestDto) {
    return this.answerService.create(createAnswerDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    if (limit > 50) {
      limit = 50;
    }
    const findManyResult = await this.answerService.findManyWithCondition({});

    return infinityPagination(
      await this.answerService.findManyWithPagination({
        page,
        limit,
      }),
      { page, limit },
      findManyResult.length,
    );
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.answerService.findOne({ id: +id });
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post('/del-multiple')
  @HttpCode(HttpStatus.OK)
  deleteMultiple(@Body() ids: Array<number>) {
    return this.answerService.deleteMultiple(ids);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  update(
    @Param('id') id: number,
    @Body() updateAnswerDto: UpdateAnswerRequestDto,
  ) {
    return this.answerService.update(id, updateAnswerDto);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin, RoleEnum.user)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.answerService.deleteByProperties({ id: id });
  }
}
