import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiscussionCategory } from 'src/database/entities/DiscussionCategory.entity';
import { DiscussionModule } from '../discussion/discussion.module';
import { DiscussionCategoryController } from './discussion-category.controller';
import { DiscussionCategoryService } from './discussion-category.service';

@Module({
  imports: [TypeOrmModule.forFeature([DiscussionCategory]), DiscussionModule],
  controllers: [DiscussionCategoryController],
  providers: [DiscussionCategoryService],
  exports: [DiscussionCategoryService],
})
export class DiscussionCategoriesModule {}
