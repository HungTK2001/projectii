import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateDiscussionCategoryRequestDto {
  @ApiProperty({
    type: String,
    example: 'Giải tích III',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: String,
    example: 'Thảo luận về môn học giải tích III',
  })
  @IsNotEmpty()
  detail: string;

  @ApiProperty({
    type: String,
    example:
      'Tổng hợp các bài thảo luận liên quan đến môn giải tích III đại học bách khoa Hà Nội.',
  })
  @IsNotEmpty()
  moreDetail: string;
}
