import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { infinityPagination } from 'src/shared/utils/infinity_pagination';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { DiscussionCategoryService } from './discussion-category.service';
import { CreateDiscussionCategoryRequestDto } from './dto/request/create-discussion-category.dto';
import { UpdateDiscussionCategoryRequestDto } from './dto/request/update-discussion-category.dto';

@ApiTags('Discussion category')
@Controller({
  path: 'discussion-categories',
  version: '1',
})
export class DiscussionCategoryController {
  constructor(
    private readonly discussionCategoriesService: DiscussionCategoryService,
  ) {}

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(
    @Body() createDiscussionCategoryDto: CreateDiscussionCategoryRequestDto,
  ) {
    return this.discussionCategoriesService.create(createDiscussionCategoryDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    if (limit > 50) {
      limit = 50;
    }
    const findManyResult =
      await this.discussionCategoriesService.findManyWithCondition({});
    return infinityPagination(
      await this.discussionCategoriesService.findManyWithPagination({
        page,
        limit,
      }),
      { page, limit },
      findManyResult.length,
    );
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.discussionCategoriesService.findOne({ id: +id });
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  update(
    @Param('id') id: number,
    @Body() updateDiscussionCategoryDto: UpdateDiscussionCategoryRequestDto,
  ) {
    return this.discussionCategoriesService.update(
      id,
      updateDiscussionCategoryDto,
    );
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.discussionCategoriesService.softDelete(id);
  }
}
