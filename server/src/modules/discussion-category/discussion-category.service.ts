import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DiscussionCategory } from 'src/database/entities/DiscussionCategory.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { DiscussionService } from '../discussion/discussion.service';
import { CreateDiscussionCategoryRequestDto } from './dto/request/create-discussion-category.dto';
import { UpdateDiscussionCategoryRequestDto } from './dto/request/update-discussion-category.dto';

@Injectable()
export class DiscussionCategoryService {
  constructor(
    @InjectRepository(DiscussionCategory)
    private discussionCategoriesRepository: Repository<DiscussionCategory>,
    private discussionService: DiscussionService,
  ) {}

  create(createDiscussionCategoryDto: CreateDiscussionCategoryRequestDto) {
    return this.discussionCategoriesRepository.save(
      this.discussionCategoriesRepository.create(createDiscussionCategoryDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.discussionCategoriesRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: EntityCondition<DiscussionCategory>,
  ): Promise<DiscussionCategory[] | undefined> {
    return this.discussionCategoriesRepository.find({ where: fields });
  }

  async findOne(fields: EntityCondition<DiscussionCategory>) {
    const discussionCategory =
      await this.discussionCategoriesRepository.findOne({
        where: fields,
      });
    const discussion = await this.discussionService.findManyWithCondition({
      discussionCategoryID: discussionCategory.id,
    });
    return {
      ...discussionCategory,
      discussion: discussion,
    };
  }

  async update(
    id: number,
    updateDiscussionCategoryDto: UpdateDiscussionCategoryRequestDto,
  ): Promise<UpdateDiscussionCategoryRequestDto> {
    let discussionCategory = await this.discussionCategoriesRepository.findOne(
      id,
    );
    return this.discussionCategoriesRepository.save({
      ...discussionCategory,
      ...updateDiscussionCategoryDto,
      updateAt: Date.now(),
    });
  }
  async softDelete(id: number): Promise<void> {
    await this.discussionCategoriesRepository.delete(id);
  }
}
