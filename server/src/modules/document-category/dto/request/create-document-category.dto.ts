import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateDocumentCategoryRequestDto {
  @ApiProperty({
    type: String,
    example: 'Giải tích III',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: String,
    example: 'Tài liệu về môn học giải tích III',
  })
  @IsNotEmpty()
  detail: string;

  @ApiProperty({
    type: String,
    example:
      'Dưới đây là file đề thi các năm môn giải tích 3 đại học bách khoa Hà Nội. Đề thi sẽ liên tục được cập nhật.',
  })
  @IsNotEmpty()
  moreDetail: string;
}
