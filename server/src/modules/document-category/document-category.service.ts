import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DocumentCategory } from 'src/database/entities/DocumentCategory.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { DocumentService } from '../document/document.service';
import { CreateDocumentCategoryRequestDto } from './dto/request/create-document-category.dto';
import { UpdateDocumentCategoryRequestDto } from './dto/request/update-document-category.dto';

@Injectable()
export class DocumentCategoryService {
  constructor(
    @InjectRepository(DocumentCategory)
    private documentCategoriesRepository: Repository<DocumentCategory>,
    private documentService: DocumentService,
  ) {}

  create(createDocumentCategoryDto: CreateDocumentCategoryRequestDto) {
    return this.documentCategoriesRepository.save(
      this.documentCategoriesRepository.create(createDocumentCategoryDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.documentCategoriesRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: EntityCondition<DocumentCategory>,
  ): Promise<DocumentCategory[] | undefined> {
    return this.documentCategoriesRepository.find({ where: fields });
  }

  async findOne(fields: EntityCondition<DocumentCategory>) {
    const documentCategory = await this.documentCategoriesRepository.findOne({
      where: fields,
    });
    const document = await this.documentService.findManyWithCondition({
      documentCategoryID: documentCategory.id,
    });
    return {
      ...documentCategory,
      document: document,
    };
  }

  async update(
    id: number,
    updateDocumentCategoryDto: UpdateDocumentCategoryRequestDto,
  ): Promise<UpdateDocumentCategoryRequestDto> {
    let documentCategory = await this.documentCategoriesRepository.findOne(id);
    return this.documentCategoriesRepository.save({
      ...documentCategory,
      ...updateDocumentCategoryDto,
      updateAt: Date.now(),
    });
  }
  async softDelete(id: number): Promise<void> {
    await this.documentCategoriesRepository.delete(id);
  }
}
