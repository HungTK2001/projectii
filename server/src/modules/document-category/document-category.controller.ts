import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { infinityPagination } from 'src/shared/utils/infinity_pagination';
import { Roles } from '../roles/roles.decorator';
import { RoleEnum } from '../roles/roles.enum';
import { RolesGuard } from '../roles/roles.guard';
import { DocumentCategoryService } from './document-category.service';
import { CreateDocumentCategoryRequestDto } from './dto/request/create-document-category.dto';
import { UpdateDocumentCategoryRequestDto } from './dto/request/update-document-category.dto';

@ApiTags('Document category')
@Controller({
  path: 'document-categories',
  version: '1',
})
export class DocumentCategoryController {
  constructor(
    private readonly documentCategoriesService: DocumentCategoryService,
  ) {}

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createDocumentCategoryDto: CreateDocumentCategoryRequestDto) {
    return this.documentCategoriesService.create(createDocumentCategoryDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    if (limit > 50) {
      limit = 50;
    }
    const findManyResult =
      await this.documentCategoriesService.findManyWithCondition({});
    return infinityPagination(
      await this.documentCategoriesService.findManyWithPagination({
        page,
        limit,
      }),
      { page, limit },
      findManyResult.length,
    );
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.documentCategoriesService.findOne({ id: +id });
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  update(
    @Param('id') id: number,
    @Body() updateDocumentCategoryDto: UpdateDocumentCategoryRequestDto,
  ) {
    return this.documentCategoriesService.update(id, updateDocumentCategoryDto);
  }

  @ApiBearerAuth()
  @Roles(RoleEnum.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.documentCategoriesService.softDelete(id);
  }
}
