import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentCategory } from 'src/database/entities/DocumentCategory.entity';
import { DocumentsModule } from '../document/document.module';
import { DocumentCategoryController } from './document-category.controller';
import { DocumentCategoryService } from './document-category.service';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentCategory]), DocumentsModule],
  controllers: [DocumentCategoryController],
  providers: [DocumentCategoryService],
  exports: [DocumentCategoryService],
})
export class DocumentCategoriesModule {}
