import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateQuizResultRequestDto {
  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  quizID: number;

  @ApiProperty({
    type: Number,
    example: 100,
  })
  @IsNotEmpty()
  score: number;

  @ApiProperty({
    type: Number,
    example: 10000000000,
  })
  @IsNotEmpty()
  quizStartAt: number;

  @ApiProperty({
    type: Number,
    example: 10000600000,
  })
  @IsNotEmpty()
  quizEndAt: number;
}
