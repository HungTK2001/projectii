import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuizResult } from 'src/database/entities/QuizResult.entity';
import { QuizResultController } from './quiz-result.controller';
import { QuizResultService } from './quiz-result.service';

@Module({
  imports: [TypeOrmModule.forFeature([QuizResult])],
  controllers: [QuizResultController],
  providers: [QuizResultService],
  exports: [QuizResultService],
})
export class QuizResultModule {}
