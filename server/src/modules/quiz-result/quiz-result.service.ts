import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QuizResult } from 'src/database/entities/QuizResult.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { CreateQuizResultRequestDto } from './dto/request/create-quiz-result.dto';
import { UpdateQuizResultRequestDto } from './dto/request/update-quiz-result.dto';

@Injectable()
export class QuizResultService {
  constructor(
    @InjectRepository(QuizResult)
    private quizResultRepository: Repository<QuizResult>,
  ) {}

  create(createQuizResultDto: CreateQuizResultRequestDto) {
    return this.quizResultRepository.save(
      this.quizResultRepository.create(createQuizResultDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.quizResultRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: EntityCondition<QuizResult>,
  ): Promise<QuizResult[] | undefined> {
    return this.quizResultRepository.find({ where: fields });
  }
  findOne(
    fields: EntityCondition<QuizResult>,
  ): Promise<QuizResult | undefined> {
    return this.quizResultRepository.findOne({ where: fields });
  }

  async update(
    id: number,
    updateQuizResultDto: UpdateQuizResultRequestDto,
  ): Promise<QuizResult> {
    let quizResult = await this.quizResultRepository.findOne(id);
    return this.quizResultRepository.save({
      ...quizResult,
      ...updateQuizResultDto,
      updateAt: Date.now(),
    });
  }
  async softDelete(id: number): Promise<void> {
    await this.quizResultRepository.softDelete(id);
  }
  async deleteByProps(props: Partial<QuizResult>): Promise<void> {
    await this.quizResultRepository.delete(props);
  }
}
