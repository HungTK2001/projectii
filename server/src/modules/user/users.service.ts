import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/database/entities/User.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { UpdatePasswordDTO } from '../auth/dto/request/update-password.dto';
import { CommentService } from '../comment/comment.service';
import { DiscussionService } from '../discussion/discussion.service';
import { QuizResultService } from '../quiz-result/quiz-result.service';
import { QuizzesService } from '../quizzes/quizzes.service';
import { CreateUserRequestDTO } from './dto/request/create_user.dto';
import { UpdateUserRequestDTO } from './dto/request/update_user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private quizzesService: QuizzesService,
    private quizResultService: QuizResultService,
    private discussionService: DiscussionService,
    private commentService: CommentService,
  ) {}

  create(createProfileDTO: CreateUserRequestDTO) {
    return this.usersRepository.save(
      this.usersRepository.create(createProfileDTO),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.usersRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(fields: Partial<User>): Promise<User[] | undefined> {
    return this.usersRepository.find({ where: fields });
  }

  findOne(fields: EntityCondition<User>): Promise<User | undefined> {
    return this.usersRepository.findOne({ where: fields });
  }

  async update(
    id: number,
    updateProfileDTO: UpdateUserRequestDTO,
  ): Promise<User> {
    let user = await this.usersRepository.findOne({ where: { id } });
    return this.usersRepository.save({
      ...user,
      ...updateProfileDTO,
      updateAt: Date.now(),
    });
  }

  async softDelete(id: number): Promise<void> {
    await this.usersRepository.softDelete(id);
  }

  async updatePassword(id: number, updatePasswordDTO: UpdatePasswordDTO) {
    const saltOrRounds = 10;
    const hashPassword = await bcrypt.hash(
      updatePasswordDTO.password,
      saltOrRounds,
    );
    let user = await this.usersRepository.findOne({ id });
    return this.usersRepository.save({
      ...user,
      password: hashPassword,
      updateAt: Date.now(),
    });
  }

  async deleteByProps(props: Partial<User>): Promise<void> {
    const users = await this.findManyWithCondition(props);
    for (let i = 0; i < users.length; i++) {
      await this.quizzesService.deleteByProps({ userID: users[i].id });
      await this.quizResultService.deleteByProps({ userID: users[i].id });
      await this.discussionService.deleteByProps({ userID: users[i].id });
      await this.commentService.deleteAllByProperties({ userID: users[i].id });
    }
    await this.usersRepository.delete(props);
  }
}
