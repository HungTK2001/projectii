import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users.controller';
import { User } from 'src/database/entities/User.entity';
import { QuizResultModule } from '../quiz-result/quiz-result.module';
import { QuizzesModule } from '../quizzes/quizzes.module';
import { DiscussionModule } from '../discussion/discussion.module';
import { CommentsModule } from '../comment/comment.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    QuizResultModule,
    QuizzesModule,
    DiscussionModule,
    CommentsModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
