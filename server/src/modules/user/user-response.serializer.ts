import { User } from 'src/database/entities/User.entity';

const userResponseSerializer = (user: User) => {
  delete user.password;
};

export default userResponseSerializer;
