import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from 'src/database/entities/Comment.entity';
import { User } from 'src/database/entities/User.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { FindCondition, FindConditions, Repository } from 'typeorm';
import { CreateCommentRequestDto } from './dto/request/create-comment.dto';
import { UpdateCommentRequestDto } from './dto/request/update-comment.dto';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private commentsRepository: Repository<Comment>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  create(createCommentDto: CreateCommentRequestDto) {
    return this.commentsRepository.save(
      this.commentsRepository.create(createCommentDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.commentsRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  async findManyWithCondition(fields: EntityCondition<Comment>) {
    const comments = await this.commentsRepository.find({ where: fields });
    const result = [];
    for (let i = 0; i < comments.length; i++) {
      let user = await this.userRepository.findOne({
        where: { id: comments[i].userID },
      });
      result.push({
        ...comments[i],
        author: user.fullName,
      });
    }
    return result;
  }

  findOne(fields: EntityCondition<Comment>): Promise<Comment | undefined> {
    return this.commentsRepository.findOne({ where: fields });
  }

  async update(
    id: number,
    updateCommentDto: UpdateCommentRequestDto,
  ): Promise<UpdateCommentRequestDto> {
    let comment = await this.commentsRepository.findOne(id);
    return this.commentsRepository.save({
      ...comment,
      ...updateCommentDto,
      updateAt: Date.now(),
    });
  }

  async softDelete(id: number): Promise<void> {
    await this.commentsRepository.delete(id);
  }

  async deleteAllByProperties(fields: Partial<Comment>) {
    await this.commentsRepository.delete(fields);
  }
}
