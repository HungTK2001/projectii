import { Optional } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateCommentRequestDto {
  @ApiProperty({
    type: String,
    example: 'Math is very good',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'https://images.google.com/klsfkslfsslfksfksl',
  })
  @Optional()
  imageUrl: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  discussionID: number;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;
}
