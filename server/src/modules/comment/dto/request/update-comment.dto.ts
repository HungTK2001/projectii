import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CreateCommentRequestDto } from './create-comment.dto';

export class UpdateCommentRequestDto extends PartialType(
  CreateCommentRequestDto,
) {
  @ApiProperty({
    type: String,
    example: 'Math is very good',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'https://images.google.com/klsfkslfsslfksfksl',
  })
  imageUrl: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  discussionID: number;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  userID: number;
}
