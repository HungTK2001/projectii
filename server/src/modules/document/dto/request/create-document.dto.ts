import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Validate } from 'class-validator';
import { IsNotExist } from 'src/shared/utils/validators/is_not_exists.validator';

export class CreateDocumentRequestDto {
  @ApiProperty({
    type: String,
    example: 'Math',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: String,
    example: 'Math study',
  })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    type: String,
    example: 'https://drive.google.com/klsfkslfsslfksfksl',
  })
  @IsNotEmpty()
  @Validate(IsNotExist, ['Document'], {
    message: 'fileIsAlreadyExist',
  })
  googleDriveLink: string;

  @ApiProperty({
    type: String,
    example: 'jskdsksdssmsddslsdklsdkl',
  })
  @IsNotEmpty()
  @Validate(IsNotExist, ['Document'], {
    message: 'fileIsAlreadyExist',
  })
  fileID: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  documentCategoryID: number;
}
