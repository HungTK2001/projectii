import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty, Validate } from 'class-validator';
import { IsNotExist } from 'src/shared/utils/validators/is_not_exists.validator';
import { CreateDocumentRequestDto } from './create-document.dto';

export class UpdateDocumentRequestDto extends PartialType(
  CreateDocumentRequestDto,
) {
  @ApiProperty({
    type: String,
    example: 'Math',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: String,
    example: 'Math study',
  })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    type: String,
    example: 'https://drive.google.com/klsfkslfsslfksfksl',
  })
  @IsNotEmpty()
  googleDriveLink: string;

  @ApiProperty({
    type: String,
    example: 'jskdsksdssmsddslsdklsdkl',
  })
  @IsNotEmpty()
  fileID: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  documentCategoryID: number;
}
