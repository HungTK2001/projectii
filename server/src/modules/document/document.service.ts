import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Document } from 'src/database/entities/Document.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Readable } from 'stream';
import { Repository } from 'typeorm';
import { CreateDocumentRequestDto } from './dto/request/create-document.dto';
import { UpdateDocumentRequestDto } from './dto/request/update-document.dto';
const fs = require('fs');
const { google } = require('googleapis');
const path = require('path');
const CLIENT_ID =
  '145951594024-fpo36aurk9210rjcodmj8dd1n85rlme2.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';

const REFRESH_TOKEN =
  '1//04vKeiHEdFONbCgYIARAAGAQSNwF-L9IrJtT-kE36gGw5spGyV3oGeETvOimgmwCg_qFCF2atCiHbvOQBnL4yWjFhiuINAmd3G80';
const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI,
);

oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive = google.drive({
  version: 'v3',
  auth: oAuth2Client,
});

@Injectable()
export class DocumentService {
  constructor(
    @InjectRepository(Document)
    private documentsRepository: Repository<Document>,
  ) {}

  create(createDocumentDto: CreateDocumentRequestDto) {
    return this.documentsRepository.save(
      this.documentsRepository.create(createDocumentDto),
    );
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.documentsRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: EntityCondition<Document>,
  ): Promise<Document[] | undefined> {
    return this.documentsRepository.find({ where: fields });
  }

  findOne(fields: EntityCondition<Document>): Promise<Document | undefined> {
    return this.documentsRepository.findOne({ where: fields });
  }

  async update(
    id: number,
    updateDocumentDto: UpdateDocumentRequestDto,
  ): Promise<UpdateDocumentRequestDto> {
    let document = await this.documentsRepository.findOne(id);
    return this.documentsRepository.save({
      ...document,
      ...updateDocumentDto,
      updateAt: Date.now(),
    });
  }

  async softDelete(id: number, fileID: string) {
    try {
      await this.deleteFile(fileID);
      return await this.documentsRepository.delete(id);
    } catch (error) {
      return error;
    }
  }

  async uploadFile(file: any) {
    try {
      const response = await drive.files.create({
        requestBody: {
          name: Date.now() + file.originalname,
          mineType: file.minetype,
          parents: ['1JrNnYIh3IJoLNTXH5g2bJ-J3VOuOnNUK'],
        },
        media: {
          mineType: file.minetype,
          body: this.bufferToStream(file.buffer),
        },
      });
      const uploadData = await this.generatePublicUrl(response.data.id);
      return {
        fileID: response.data.id,
        ...uploadData.data,
      };
    } catch (error) {
      return error.message;
    }
  }

  async deleteFile(fileId: string) {
    try {
      const response = await drive.files.delete({
        fileId: fileId,
      });
      return response;
    } catch (error) {
      return error.message;
    }
  }

  async generatePublicUrl(fileId: string) {
    try {
      await drive.permissions.create({
        fileId: fileId,
        requestBody: {
          role: 'reader',
          type: 'anyone',
        },
      });
      const result = await drive.files.get({
        fileId: fileId,
        fields: 'webViewLink, webContentLink',
      });
      return result;
    } catch (error) {
      return error.message;
    }
  }

  bufferToStream = (buffer: Buffer) => {
    const stream = new Readable();
    stream.push(buffer);
    stream.push(null);
    return stream;
  };
}
