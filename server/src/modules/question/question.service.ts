import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Question } from 'src/database/entities/Question.entity';
import { EntityCondition } from 'src/shared/utils/types/entity_condition.type';
import { IPaginationOptions } from 'src/shared/utils/types/pagination_options.type';
import { Repository } from 'typeorm';
import { AnswerService } from '../answer/answer.service';
import { CreateAnswerRequestDto } from '../answer/dto/request/create-answer.dto';
import { UpdateAnswerRequestDto } from '../answer/dto/request/update-answer.dto';
import { CreateQuestionRequestDto } from './dto/request/create-question.dto';
import { UpdateQuestionRequestDto } from './dto/request/update-question.dto';

@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,
    private answerService: AnswerService,
  ) {}

  async create(createQuestionDto: CreateQuestionRequestDto) {
    const { answers, ...newQuestion } = createQuestionDto;
    const question = await this.questionsRepository.save(
      this.questionsRepository.create(newQuestion),
    );
    for (let i = 0; i < answers.length; i++) {
      await this.answerService.create({
        ...answers[i],
        questionID: question.id,
      });
    }
    return question;
  }

  findManyWithPagination(paginationOptions: IPaginationOptions) {
    return this.questionsRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findManyWithCondition(
    fields: Partial<Question>,
  ): Promise<Question[] | undefined> {
    return this.questionsRepository.find({ where: fields });
  }

  async findQuestionWithQuizID(quizID: number) {
    const result = [];
    const questions = await this.findManyWithCondition({ quizID: quizID });
    let answer = null;
    for (let question of questions) {
      answer = await this.answerService.findManyWithCondition({
        questionID: question.id,
      });
      result.push({
        ...question,
        answers: answer,
      });
    }
    return result;
  }

  async findOne(fields: EntityCondition<Question>) {
    const question = await this.questionsRepository.findOne({ where: fields });
    const answers = await this.answerService.findManyWithCondition({
      questionID: question.id,
    });
    return {
      ...question,
      answers,
    };
  }

  async update(
    id: number,
    updateQuestionDto: UpdateQuestionRequestDto,
  ): Promise<Question> {
    let question = await this.questionsRepository.findOne(id);
    return this.questionsRepository.save({
      ...question,
      ...updateQuestionDto,
      updateAt: Date.now(),
    });
  }

  async softDelete(id: number): Promise<void> {
    await this.questionsRepository.softDelete(id);
  }

  async deleteByProps(props: Partial<Question>): Promise<void> {
    const questions = await this.findManyWithCondition(props);
    for (let i = 0; i < questions.length; i++) {
      await this.answerService.deleteByProperties({
        questionID: questions[i].id,
      });
    }
    await this.questionsRepository.delete(props);
  }
}
