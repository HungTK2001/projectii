import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { CreateAnswerRequestDto } from 'src/modules/answer/dto/request/create-answer.dto';

export class CreateQuestionRequestDto {
  @ApiProperty({
    type: String,
    example: 'Solve x=x+1',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'http://klsdfjsfjslfjsl',
  })
  @IsOptional()
  imageUrl: string;

  @ApiProperty({
    type: Number,
    example: 1,
  })
  @IsNotEmpty()
  quizID: number;

  @ApiProperty({
    type: Array,
    example: [
      {
        content: 'x=0',
        imageUrl: 'http://kldskddklskl',
        isCorrect: true,
      },
      {
        content: 'x=1',
        imageUrl: 'http://kldskddklskl',
        isCorrect: false,
      },
    ],
  })
  @IsOptional()
  answers: Array<CreateAnswerRequestDto>;
}
