import { Optional } from '@nestjs/common';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateQuestionRequestDto {
  @ApiProperty({
    type: String,
    example: 'Solve x=x+1',
  })
  @IsNotEmpty()
  content: string;

  @ApiProperty({
    type: String,
    example: 'http://klsdfjsfjslfjsl',
  })
  @Optional()
  imageUrl: string;
}
