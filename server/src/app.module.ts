import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import appConfig from './config/app.config';
import authConfig from './config/auth.config';
import { AnswerModule } from './modules/answer/answer.module';
import { AuthModule } from './modules/auth/auth.module';
import { CommentsModule } from './modules/comment/comment.module';
import { DiscussionCategoriesModule } from './modules/discussion-category/discussion-category.module';
import { DiscussionModule } from './modules/discussion/discussion.module';
import { DocumentCategoriesModule } from './modules/document-category/document-category.module';
import { DocumentsModule } from './modules/document/document.module';
import { QuestionsModule } from './modules/question/question.module';
import { QuizCategoriesModule } from './modules/quiz-category/quiz-category.module';
import { QuizResultModule } from './modules/quiz-result/quiz-result.module';
import { QuizzesModule } from './modules/quizzes/quizzes.module';
import { UsersModule } from './modules/user/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [authConfig, appConfig],
      envFilePath: ['.env'],
    }),
    UsersModule,
    AuthModule,
    AnswerModule,
    CommentsModule,
    DiscussionModule,
    DiscussionCategoriesModule,
    DocumentsModule,
    DocumentCategoriesModule,
    QuestionsModule,
    QuizzesModule,
    QuizCategoriesModule,
    QuizResultModule,
  ],
})
export class AppModule {}
