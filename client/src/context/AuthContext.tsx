import React, { createContext, useState, FC, useEffect } from 'react';

interface IUser {
  id: number;
  username: string;
  email: string;
  fullName: string;
  password: string;
  roleID: number;
  createAt: string;
  updateAt: string;
}
interface IAuthContext {
  token: string | null;
  roleID: number | null;
  isLogin: boolean | null;
  user: IUser | null;
  setAuthData?: (token: string, roleID: number, user: IUser) => void;
  clearAuthData?: () => void;
  setTokenRefresh?: () => void;
}

const defaultAuthContext: IAuthContext = {
  token: '',
  roleID: -1,
  isLogin: false,
  user: null
};

export const AuthContext = createContext<IAuthContext>(defaultAuthContext);

export const AuthProvider: FC<any> = ({ children }) => {
  const [token, setToken] = useState<string | null>(localStorage.getItem('token'));
  const [roleID, setRoleID] = useState<number>(parseInt(localStorage.getItem('roleID') !== null ? localStorage.getItem('roleID')! : '0'));
  const [isLogin, setIsLogin] = useState<boolean | null>(localStorage.getItem('isLogin') === 'true');
  const [user, setUser] = useState<IUser | null>(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null);

  const setTokenRefresh = () => {
    const roleID = localStorage.getItem('roleID') !== null ? localStorage.getItem('roleID')! : '0';
    setToken(localStorage.getItem('token'));
    setRoleID(parseInt(roleID));
    setIsLogin(localStorage.getItem('isLogin') === 'true');
    setUser(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null);
  };

  useEffect(() => {
    const roleID = localStorage.getItem('roleID') !== null ? localStorage.getItem('roleID')! : '0';
    setToken(localStorage.getItem('token'));
    setRoleID(parseInt(roleID));
    setIsLogin(localStorage.getItem('isLogin') === 'true');
    setUser(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null);
  }, [token]);
  const setAuthData = (token: string, roleID: number, user: any) => {
    localStorage.setItem('token', token);
    localStorage.setItem('roleID', JSON.stringify(roleID));
    localStorage.setItem('isLogin', JSON.stringify(true));
    localStorage.setItem('user', JSON.stringify(user));
    setToken(token);
    setIsLogin(true);
    setRoleID(roleID);
    setUser(user);
  };
  const clearAuthData = () => {
    localStorage.clear();
    setIsLogin(false);
    setRoleID(-1);
    setToken('');
    setUser(null);
  };

  const contextData = {
    token,
    roleID,
    isLogin,
    user,
    setAuthData,
    clearAuthData,
    setTokenRefresh
  };

  return <AuthContext.Provider value={contextData}>{children}</AuthContext.Provider>;
};
