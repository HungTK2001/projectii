export const USER_ROUTER = {
  home: {
    path: '/'
  }
};

export const ADMIN_ROUTER_FIRST_PART = [
  {
    title: 'Home',
    path: '/admin',
    icon: 'bi bi-house-door-fill'
  },
  {
    title: 'Users',
    path: '/admin/user',
    icon: 'bi bi-people-fill'
  },
  {
    title: 'Documents',
    path: '/admin/document',
    icon: 'bi bi-journal-text'
  },
  {
    title: 'Discussion',
    path: '/admin/discussion',
    icon: 'bi bi-chat-square-text-fill'
  },
  {
    title: 'Quizzes',
    path: '/admin/quizzes',
    icon: 'bi bi-question-circle-fill'
  }
];

export const ADMIN_ROUTER_SECOND_PART = [
  {
    title: 'Change password',
    path: '/admin/change-password',
    icon: 'bi bi-gear-fill'
  },
  {
    title: 'Log out',
    path: '/admin/log-out',
    icon: 'bi bi-box-arrow-in-right'
  }
];
