import ChangePassword from '../components/pages/admin/ChangePassword';
import Discussion from '../components/pages/admin/Discussion/Discussion';
import Document from '../components/pages/admin/Document/Document';
import Home from '../components/pages/admin/Home';
import LoginPage from '../components/pages/admin/Login/Login';
import Logout from '../components/pages/admin/Logout';
import Quizzes from '../components/pages/admin/Quizzes/Quizzes';
import User from '../components/pages/admin/User';
import DiscussionDetailPage from '../components/pages/user/DiscussionDetail';
import DocumentDetailPage from '../components/pages/user/DocumentDetail';
import UserHomePage from '../components/pages/user/Home';
import UserLoginPage from '../components/pages/user/Login';
import QuizDetailPage from '../components/pages/user/QuizDetail';
import Layout from '../components/templates/admin/Layout';
import UserLayout from '../components/templates/user/Layout';
import { ADMIN_ROUTER_FIRST_PART, ADMIN_ROUTER_SECOND_PART } from './ConstRoute';
import DocumentCategory from '../components/pages/admin/Document/DocumentCategory';
import CreateQuizzes from '../components/pages/admin/Quizzes/CreateQuizzes';
import DiscussionDetail from '../components/pages/admin/Discussion/DiscussionDetail';
import QuizCategory from '../components/pages/admin/Quizzes/QuizzCategory';
import DiscussionCategory from '../components/pages/admin/Discussion/DiscussionCategory';
import UserQuizCategory from '../components/pages/user/QuizCategory';
import UserDiscussionCategory from '../components/pages/user/DiscussionCategory';
import UserDocumentCategory from '../components/pages/user/DocumentCategory';
import UserDiscussion from '../components/pages/user/Discussion';
import UserQuizzes from '../components/pages/user/Quizes';
import CreateDiscussionPage from '../components/pages/user/CreateDiscussionPage';
import CreateQuizPage from '../components/pages/user/CreateQuizPage';
import CreateQuiz from '../components/pages/user/CreateQuiz';
import OwnerDiscussion from '../components/pages/user/OwnerDiscussion';
import OwnerQuizzes from '../components/pages/user/OwnerQuizzes';
import UserDiscussionDetail from '../components/pages/user/DiscussionDetailPage';
import DoQuizPage from '../components/pages/user/DoQuizPage';

export const routes = [
  {
    path: '/admin',
    element: <Layout />,
    children: [
      { path: ADMIN_ROUTER_FIRST_PART[0].path, element: <Home /> },
      { path: ADMIN_ROUTER_FIRST_PART[1].path, element: <User /> },
      { path: ADMIN_ROUTER_FIRST_PART[2].path, element: <Document /> },
      { path: '/admin/document-category', element: <DocumentCategory /> },
      { path: ADMIN_ROUTER_FIRST_PART[3].path, element: <Discussion /> },
      { path: ADMIN_ROUTER_FIRST_PART[4].path, element: <Quizzes /> },
      { path: '/admin/quizzes/:id', element: <CreateQuizzes /> },
      { path: '/admin/quiz-category', element: <QuizCategory /> },
      { path: ADMIN_ROUTER_SECOND_PART[0].path, element: <ChangePassword /> },
      { path: '/admin/discussions/:id', element: <DiscussionDetail /> },
      { path: '/admin/discussion-category', element: <DiscussionCategory /> },
      { path: ADMIN_ROUTER_SECOND_PART[1].path, element: <Logout /> }
    ]
  },
  {
    path: '/admin/login',
    element: <LoginPage />
  },
  {
    path: '/',
    element: <UserLayout />,
    children: [
      {
        index: true,
        element: <UserHomePage />
      },
      {
        path: '/document-category',
        element: <UserDocumentCategory />
      },
      {
        path: '/document-category/:id',
        element: <DocumentDetailPage />
      },
      {
        path: '/quiz-category',
        element: <UserQuizCategory />
      },
      {
        path: '/quiz-category/:id',
        element: <UserQuizzes />
      },
      {
        path: '/quiz-detail/:id',
        element: <CreateQuizPage />
      },
      {
        path: '/quiz/create',
        element: <CreateQuiz />
      },
      {
        path: '/quiz/:id',
        element: <QuizDetailPage />
      },
      {
        path: '/do-quiz/:id',
        element: <DoQuizPage />
      },
      {
        path: '/discussion-category',
        element: <UserDiscussionCategory />
      },
      {
        path: '/discussion-category/:id',
        element: <UserDiscussion />
      },
      {
        path: '/discussion-detail/:id',
        element: <UserDiscussionDetail />
      },
      {
        path: '/owner-discussion/:id',
        element: <OwnerDiscussion />
      },
      {
        path: '/owner-quiz/:id',
        element: <OwnerQuizzes />
      },
      {
        path: '/discussion/create',
        element: <CreateDiscussionPage />
      },
      {
        path: '/discussion/:id',
        element: <DiscussionDetailPage />
      },
      { path: '*', element: <div>This url is incorrect</div> }
    ]
  },
  {
    path: '/user/login',
    element: <UserLoginPage />
  }
];
