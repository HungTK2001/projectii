import { useLocation } from 'react-router-dom';

interface recordData {
  data: Record<string, unknown>;
  keys: Array<string>;
  index: number;
  handleDelete?: (id: number) => void;
  handleToggleUpdate?: (id: number) => void;
}

export default function Record(props: recordData) {
  const location = useLocation();
  const { data, keys, index, handleDelete, handleToggleUpdate } = props;
  return (
    <tr className='bg-gray-100 text-center border-b text-sm text-gray-600'>
      <td className='p-2 border-r'>
        <input type='checkbox' />
      </td>
      <td className='p-2 border-r'>{index + 1}</td>
      {keys.map(key => (
        <td className='p-2 border-r' key={key}>
          {(data as any)[key]}
        </td>
      ))}
      <td className=' flex gap-2 justify-center items-center h-full'>
        <button
          onClick={() => {
            handleToggleUpdate?.(data as any['id']);
          }}
          className='bg-blue-500 p-2 text-white hover:shadow-lg text-xs font-thin'
        >
          {location.pathname.includes('/admin/discussion') ? 'View' : 'Edit'}
        </button>
        <button
          onClick={() => {
            handleDelete?.(data as any['id']);
          }}
          className='bg-red-500 p-2 text-white hover:shadow-lg text-xs font-thin'
        >
          Remove
        </button>
      </td>
    </tr>
  );
}
