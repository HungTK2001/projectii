import { Link } from 'react-router-dom';
import { useScrollTo } from 'react-use-window-scroll';

interface Content {
  name: string;
  detail: string;
  to: number;
}

export default function CategoryCard(content: Content) {
  const scrollTo = useScrollTo();
  return (
    <div
      className=' p-4 w-full rounded-md bg-white hover:shadow-lg  group cursor-pointer ctgCard'
      onClick={() => scrollTo({ top: content.to, left: 0, behavior: 'smooth' })}
    >
      <div className='flex justify-center mt-4'>
        <svg
          width='150'
          height='150'
          viewBox='0 0 150 150'
          className='bg-[#fde8f1] group-hover:bg-gradient-to-r group-hover:from-[#111c58] group-hover:via-[#3d1375] group-hover:to-[#111c58]'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
          xmlnsXlink='http://www.w3.org/1999/xlink'
        >
          <rect width='150' height='150' fill='url(#pattern0)' />
          <defs>
            <pattern id='pattern0' patternContentUnits='objectBoundingBox' width='1' height='1'>
              <use xlinkHref='#image0_17_1003' transform='scale(0.00591716)' />
            </pattern>
            <image
              id='image0_17_1003'
              width='169'
              height='169'
              xlinkHref='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKkAAACpCAYAAABQ1R0vAAANL0lEQVR4Ae2dD+heVRnHv48GFpq2FjmHYtE0Ka2VOs3YbGrips5KikxmIv0vCowsgkowiISahIRBK4uS/qC5xaaZgqJl4Z9K06I2aOpqmmXGslXOJ5567nx/+91z33vfc8+9577v98CPe3/3z3Of+znf99zz/4iqKhhIIGMC+2TsG10jgf8RoEgphOwJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJUKTZRxEdpEipgewJPCd7D5s5+DcAvwOwDcB2AA8D+DOAxwE8BuAfAHYB+DeApwA8LSI77RGqaj/YA/1xxuUFAA4CsMC3tn8wgBcDWATgMP9bDGDaODajnvhqGfC4+60A7vK/+wA8KCJ/TMxrnnlV3XdEsEcAOBKAbV8BYAkFPA9Z4wNDEqkJ8AYAtwG4SUQebfy2Hd+gqvu5aF8JYCmAVwN4jafIHXsz3MflLtJfA9gEYAOAn4nIVMy2oqqWRTgOwPEAXufbIqsxXDUl8jw3kT4NYLOLctMQUss24sWzDJY9WO5/KwCYkBkA5CLShwBcBeBrsyLMcepT1aMAnALgVN9aQW4mQ98i/QuATwNYLyL/mskYqPHSntIuA7AawOkAXjtLBbI+RXojgAuZctZQ6V6XqKpVhZ0J4K0AVgKwAtrUhr5Euh7Ae0Vk99SS7ejFVPUAAG8D8DEAlkWYutCHSH8C4GQKtF0teWPEmwFcAsCyBlMT+hDpChG5fWoIZvgiqroKwKXTItauRWrNks+flvrODPW5xyVVFQBnA/iMF7T2nBvaTtcdTJ6kQLuRiHEWkY3eUHAeAKvmG2ToOiW1yvpDRMQ6fDB0SMCbaC/2POug6ly7Tkmtt9AFHcYNH+UErB5aRD7nNQDfHBKYrlNSY2MV+K/qo8fSkCImta+qan0GrhxCfrXrlNTYLwRwjX9+UscF7QcIiMidXvr/pPezDVzZ/+E+UtLira8D8A42hxY4+tuqqvV7/arVX/fnRfjJfaSkhTdvAXC9t5gUx7jtgYCIbPFOLB8FkF0fij5T0iI6rA3/TBF5pjjAbX8EVPUYy44BOLo/L+Y+uc+UtPDkDAAfL/7htl8CInK/d8j+Sr+ePPv0HFJS88Y+MUtF5LfPusa9vgmo6tsBmFh7HTWQQ0pqcWFdzdb1HSl8/lwCIvIdACcAsEGPvYVcRGoAzvCOEb3B4IPnE/Cvm43HsrJDLyEnkRoA67nDkBkBEbH5DM7yT3/n3uUm0mWqaoPRGDIjYP1/ReR9AKzyv9OQm0jt5d/VKQE+rBEBb/+3/hfWWaiTkEvpfvRlrc/pomL6m9ET3M+HgKqeC+DbXYyvyjEl3d9bP/KJEXoyj4CIXAvAhqskb6HKUaQG5KR5VHggOwIiYtMeJRdqriI9MbsYoUOlBFyo56fMo+Yq0mzajUtjhgfnEPBP/0fmHGzxn1xFupD9TVuM5Q5MiciXAXw+xaNyLN0X77nAK5GL/7nNnICPUL0ewJo2Xc01JbV37Kwerk2gs2zLRwK/E0CrHYVyTUlt0NhzZznCh/zuPiOgDU9pZVRqrinpI0OOpFn33TulWIraSshVpL9q5e1opDcCPjHFFW04kKtIb2nj5WijdwKfAGBTykeFHPOkVmA6nOPyo+I1m5tV9Vhb7yBm0t8cU9LNFGg2Got2RETuAfCFGEM5pqQn+cQFMe/FezMi4A0zDwB42SRu5ZaSXkeBThKNed/jE4DYZGkThZxSUs4RNVEUDucmVb3ZV1Np5HROKela5kUbxd0QL55ofoVcRPpZ7/I1RPD0uSYBL0TZMOlGIYfPvc2VaUvlTMWSjI3oz+DF3mRqs6TUXtm675TUZse4iAKdHbV6k+kPmrxxnyK1obHv51I5TaJraq5tNFtNH597K8VbIcnGxzDMKAFV/XndJXy6TkmtTf44CnRGlTn3tWvP2tdVSmqpp63U9nXmP+fG1Kz+55Mn7wBgQ9grQxcpqf1iloiILRPOEnxldMzOSZ/8w8bujw+aLmxX1dPGe8ArZpWA6aOO/FDnogmu2aCqL5pV+HzvegRs0V5V3TFOX21/7q0vqK1l+Sauelcvomb5Kl8nwUaXVodxKm5wfqdPYlX9QJ4lgRECqrpmnMbaKt1b6f1sdrMboc/dWgS8lP9EVTNpG5/7RwGspEBrxQkv2ouAl/KtYj8YYkVqAn2DL6sSfAhPkMAYAndVnY8Rqc2j/kYua1OFl+dqEkiSktrEqauZgtaMAl42joCNJg2HcSWrwPm1YYs8QwLNCajqEwGt6SSV+Zc1d4F3kEA1AesVFRJp0zzpbVxrqRo2z05MIDgTXxORWl2orU+/e2I3eCMJhAm0ItIPcTRnmDDPRBOw6szSUDcl3eiLoZYa4UESaIHAn0I26ojUFv9KNml/yDEenzkCj4XeuI5IrxSRP4QM8DgJtETAyjzlIVTs9+NWd9XKlNLlT+dREvg/Aet/HNLiuJT0cq4AQhl1RCC4kEdVVz1r+jyUnZc7iqIZf4xPD7mrDENVSvpdCrQMGY91TaBKpFd17QyfN9MEnhd6+5BIt7ITcwgZjyciEJzALCTSHyZyhGZJIETggNCJkEg3hW7gcRJIRGBhyG6ZSK1Uf3voBh4ngUQEFoTslon0AZ+IP3QPj5NACgKHhIyWifQXoYt5nAQSEnhpyHaZSB8MXczjJJCQQHCNpzKRbkvoCE2TQIhAI5EGu0yFrPM4CcQQUFUBcEzIRllKauPpGUigSwKHAzgw9MAykf4zdDGPk0AiAsur7JaJtOp6niOBFAReX2W0TKTBhv4qQzxHAhEEGos02IYa4QRvJYFSAqr6EgBHl570g2Up6eKqG3iOBFomcME4e2UitZIWAwkkJ+AT6H543IPKRHrkuJt4ngRaIvAeAMHeT3ueUTJC7449J7lDAokI2JgmVbVllMaGspR0qarum8g3miWBgsAHAdQr/wRkfGxhiVsSaJuAzeVQNR/p3posS0nNp9Pbdoz2SGCEgK31VX/Skb1V6//fOmKQuyTQGgFVXaKquwK6Kz0cmun5P6p6cGue0RAJOAFVvblUiRUHQ597G156EcmSQJsEVNU0dWpjmxUCtmUYrcmKgQSiCajq4iaFpVFdhlJSc2p/AFezOio6fmbegGvomkaFpRFqwVkj/JqTfSGHT43cM/W7nh+3lrcjANgAMRvJaJ1yX+gvbxML29DvvwN4CMAjvr2bsxCWyuNSAKaliULVrHqjBs8RkY2jB6Zp30qcXu1mnW9X1K5kLodgor0bwC0AbhKRLeWXzcZRVT0NwA1VC9yOJTH67a/Yt/zpVFXwe1XIZap6f8V7t3Fqi6rac44aGxlTdoG986T50FHwoSqo0WuK/R2e4gwWpQ34UtVVk1SDFBAit3eo6lqfi3OwHOs47gUl+4FGhyYitYdZh4BBpgj22VHVe6KJtWNgm6pe7F3V6sT5oK6xPH2bX6imIrUoshR1MJ9+/0V/rx1ttW7lcVX9wDTVoLQtUCM+iUjtPsujnpvzz9si3lOrJ1uXVvsGLV+8KmeedXzzfP5v2sYzqUgLP9blmL9S1TVtfm6Kl+1ge+1Qm6NVdbmq2peh9RArUnPI8nnB2Sfq/ALbusZBWeFkyMGyU1l/pUbjywujlmVp1GmkSQS1IVJ7nnVIsVS1fver0TeN2FfVfTzlvLXJiw/gWstHZ90s7fnPDalZ1q3Mrysjm6JnHYAvisjOujdNcp1/Fs8H8G4Ag6xxqPHe1qp1NYArRCS4inENO61eYgkDgAsBXF5rjFLk09sWaeGONRt+C8A32lwgwqtszgJwHoDVUa0YhafD2d4I4EsAfiQiz/TlthfwrJlzWVc+pBLpqP/WTGjNYpsB/LTJ2lC2lB+A4/3vFAAnAthv1PgM7m8FsB7A97tqcvXCsSUOl3QpziJuuxBp8axia6L9PYDtAP4KwLIFTwE4yAW4CMBhAKw9nR2vC2rl23sBbADwYwD3tjmNvH/STwBwjn/ae4uLPkRajptHYwlYFssW5LgTwC8B3AfgYRHZXcewZ6VeDsCEaR1tVuaSSFCkdWJwuNdYwcu6Ee4AYEt12//29bIumtb10LJO9uU6NLLnV1JCFGlSvDTeBoGqnvlt2KcNEogmQJFGI6SB1AQo0tSEaT+aAEUajZAGUhOgSFMTpv1oAhRpNEIaSE2AIk1NmPajCVCk0QhpIDUBijQ1YdqPJkCRRiOkgdQEKNLUhGk/mgBFGo2QBlIToEhTE6b9aAIUaTRCGkhNgCJNTZj2owlQpNEIaSA1AYo0NWHajyZAkUYjpIHUBCjS1IRpP5oARRqNkAZSE6BIUxOm/WgCFGk0QhpITYAiTU2Y9qMJUKTRCGkgNQGKNDVh2o8mQJFGI6SB1AQo0tSEaT+aAEUajZAGUhP4Ly0E6H4c5aqfAAAAAElFTkSuQmCC'
            />
          </defs>
        </svg>
      </div>
      <div>
        <span className='block text-[#1f2471] p-3 font-bold text-3xl text-center mb-4 leading-[1.2] group-hover:scale-105 duration-300'>
          {content.name}
        </span>
        <span className='font-normal text-lg block text-[#1f2471] p-3  text-center mb-4 leading-[1.2]'>{content.detail}</span>
      </div>
      <div className='flex justify-center'>
        <div className='w-10 h-10 rounded-full border-[#f01f75] border group-hover:border-0 group-hover:bg-gradient-to-r group-hover:from-[#111c58] group-hover:via-[#3d1375] group-hover:to-[#111c58] flex justify-center items-center hover:scale-105'>
          <i className='bi bi-chevron-right group-hover:text-white'></i>
        </div>
      </div>
    </div>
  );
}
