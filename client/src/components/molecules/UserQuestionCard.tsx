const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function UserQuestionCard(props: any) {
  const { data, handleDelete, handleClickUpdate } = props;
  const numOfTrue = data.answers.filter((answer: any) => answer.isCorrect == 1).length;
  return (
    <div className='my-4 p-4 border border-solid bg-white rounded-xl' style={shadow}>
      <span className='block p-2 w-full'>{data.content}</span>
      <div className='flex justify-between'>
        <div className='p-2'>
          {data.answers.map((answer: any, index: number) => (
            <div className='flex gap-1 items-center' key={index}>
              <input type={numOfTrue > 1 ? 'checkbox' : 'radio'} id='1a' value='gia tri cua a la' checked={answer.isCorrect} readOnly />
              <label htmlFor='1a'>{answer.content}</label>
            </div>
          ))}
        </div>
        <div className='flex items-end p-2 gap-3'>
          <i className='bi bi-pencil-square cursor-pointer text-blue-600' onClick={() => handleClickUpdate(data)}></i>
          <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDelete(data)}></i>
        </div>
      </div>
    </div>
  );
}
