const viewPdfBox = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function CountdownRenderer({ hours, minutes, seconds, completed }: any) {
  if (completed) {
    // Render a completed state
    document.getElementById('submit')?.click();
  } else {
    // Render a countdown
    return (
      <div className='flex w-full justify-end'>
        <span className='block text-right font-medium text-base p-2 border-2 border-blue-500 border-solid rounded-md'>
          Thời gian làm bài còn lại:{' '}
          <span className='font-bold text-lg text-blue-600'>
            {' '}
            {hours}:{minutes}:{seconds}{' '}
          </span>
        </span>
      </div>
    );
  }
}
