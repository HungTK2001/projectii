const viewPdfBox = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function Question(props: any) {
  const { data, register } = props;
  return (
    <div className='rounded-md w-full mt-5' style={viewPdfBox}>
      <div className='w-full p-6  '>
        <span className='text-[#1f2471] text-xl font-bold'>{data.content}</span>
      </div>
      <div className='w-full p-4'>
        {data.answers.map((answer: any, index: number) => (
          <div className='my-2' key={index}>
            <label className='flex items-center gap-3'>
              <div>
                <input type={data.multiChoice ? 'checkbox' : 'radio'} {...register(`${data.id}`)} value={answer.id}></input>
              </div>
              <label className='text-xs  block'>{answer.content}</label>
            </label>
          </div>
        ))}
      </div>
    </div>
  );
}
