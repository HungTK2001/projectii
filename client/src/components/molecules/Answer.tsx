export default function Answer() {
  return (
    <div className=' text-[#1f2471] w-full flex justify-between gap-2 items-center hover:bg-blue-300  px-1 group cursor-pointer'>
      <div className='flex items-center gap-10'>
        <input
          id='orange-radio'
          type='radio'
          value=''
          name='colored-radio'
          className='w-4 h-4 text-orange-500 bg-gray-100 border-gray-300 group-focus:ring-orange-500 cursor-pointer dark:group-focus:ring-orange-600 dark:ring-offset-gray-800 group-focus:ring-2 dark:bg-gray-700 dark:border-gray-600'
        />
        <span className='block ml-4'>PUT</span>
        <div className='flex gap-2 my-2 items-center'>
          <svg xmlns='http://www.w3.org/2000/svg' className='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor' strokeWidth='2'>
            <path strokeLinecap='round' strokeLinejoin='round' d='M5 13l4 4L19 7' />
          </svg>
          <svg xmlns='http://www.w3.org/2000/svg' className='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor' strokeWidth='2'>
            <path strokeLinecap='round' strokeLinejoin='round' d='M6 18L18 6M6 6l12 12' />
          </svg>
        </div>
      </div>
      <svg
        width='30'
        height='30'
        viewBox='0 0 30 30'
        className='cursor-pointer justify-self-end '
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M26.121 3.879C25.5584 3.31659 24.7955 3.00064 24 3.00064C23.2045 3.00064 22.4416 3.31659 21.879 3.879L10.5 15.258V19.5H14.742L26.121 8.121C26.6834 7.55842 26.9994 6.79549 26.9994 6C26.9994 5.20451 26.6834 4.44158 26.121 3.879Z'
          fill='#1f2471'
        />
        <path
          fillRule='evenodd'
          clipRule='evenodd'
          d='M3 9C3 8.20435 3.31607 7.44129 3.87868 6.87868C4.44129 6.31607 5.20435 6 6 6H12C12.3978 6 12.7794 6.15804 13.0607 6.43934C13.342 6.72064 13.5 7.10218 13.5 7.5C13.5 7.89782 13.342 8.27936 13.0607 8.56066C12.7794 8.84196 12.3978 9 12 9H6V24H21V18C21 17.6022 21.158 17.2206 21.4393 16.9393C21.7206 16.658 22.1022 16.5 22.5 16.5C22.8978 16.5 23.2794 16.658 23.5607 16.9393C23.842 17.2206 24 17.6022 24 18V24C24 24.7956 23.6839 25.5587 23.1213 26.1213C22.5587 26.6839 21.7956 27 21 27H6C5.20435 27 4.44129 26.6839 3.87868 26.1213C3.31607 25.5587 3 24.7956 3 24V9Z'
          fill='#1f2471'
        />
      </svg>
    </div>
  );
}
