import { useLocation } from 'react-router-dom';
import Record from './Record';

interface TableData {
  headers: Array<string>;
  keys: Array<string>;
  records: Array<Record<string, unknown>>;
  handleDelete?: (id: number) => void;
  handleToggleUpdate?: (id: number) => void;
}

export default function Table(data: TableData) {
  const { headers, records, keys, handleDelete, handleToggleUpdate } = data;
  return (
    <div className='w-full px-2 my-auto'>
      {/* Table */}
      <table className='table-auto h-fit w-full border-2 border-solid font-medium'>
        <thead>
          <tr className='bg-gray-50 border-b border-solid'>
            <th className='border-r p-2'>
              <input type='checkbox' />
            </th>
            {headers.map((header, index) => {
              return (
                <th className='p-1 border-r cursor-pointer text-sm text-gray-500' key={index}>
                  <div className='flex items-center justify-center'>
                    {header}
                    <svg xmlns='http://www.w3.org/2000/svg' className='h-4 w-4' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                      <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M8 9l4-4 4 4m0 6l-4 4-4-4' />
                    </svg>
                  </div>
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          <tr className='bg-gray-50 text-center'>
            <td className='p-2 border-r'></td>
            {headers.map((record, index) => {
              return (
                <td className='p-2 border-r' key={index}>
                  <input type='text' className='border w-full p-[2px]' />
                </td>
              );
            })}
          </tr>
          {records.map((record, index) => {
            return (
              <Record
                data={record}
                key={index}
                keys={keys}
                index={index}
                handleDelete={handleDelete}
                handleToggleUpdate={handleToggleUpdate}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
