// interface IQuestion{
//     question: string,
//     answers: Array[]
// }

export default function QuestionCard(props: any) {
  const { data, handleDelete, handleClickUpdate } = props;
  return (
    <div className='w-[763px] p-3 border border-solid border-gray-400 rounded-xl'>
      <span className='block p-2 w-full'>{data.content}</span>
      <div className='flex justify-between'>
        <div className='p-2'>
          {data.answers.map((answer: any, index: number) => (
            <div className='flex gap-1 items-center' key={index}>
              <input type='checkbox' id='1a' value='gia tri cua a la' checked={answer.isCorrect} readOnly />
              <label htmlFor='1a'>{answer.content}</label>
            </div>
          ))}
        </div>
        <div className='flex items-end p-2 gap-3'>
          <i className='bi bi-pencil-square cursor-pointer text-blue-600' onClick={() => handleClickUpdate(data)}></i>
          <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDelete(data)}></i>
        </div>
      </div>
    </div>
  );
}
