interface Card {
  header?: string;
  date?: string;
  total?: number;
  percent?: number;
}
function DashboardCard03(props: Card) {
  return (
    <div className='bg-white w-full h-72 mt-8 shadow-lg rounded-sm border-2 border-slate-500'>
      <div className='px-5 pt-5'>
        <header className='flex justify-between items-start mb-2'>
          {/* Icon */}
          {/* Menu button */}
        </header>
        <h2 className='text-xl   font-semibold text-slate-800 mb-2'>{props.header}</h2>
        <div className='text-xs font-semibold text-slate-400 uppercase mb-1'>Today: {props.date}</div>
        <div className='flex items-start'>
          <div className='text-3xl font-bold text-slate-800 mr-2'>{props.total}</div>
          <div className='text-sm font-semibold text-white px-1.5 bg-green-500 rounded-full'>{props.percent}%</div>
        </div>
      </div>
    </div>
  );
}

export default DashboardCard03;
