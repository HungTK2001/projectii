const viewPdfBox = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function DiscussionContent(props: any) {
  const { title, content, imageUrl } = props;
  return (
    <div className='rounded-md w-full mt-5' style={viewPdfBox}>
      <div className='w-full p-6  '>
        <span className='text-[#1f2471] text-xl font-bold'>{title}</span>
      </div>
      <div className='w-full p-4'>{content}</div>
      <div className='w-full p-4'>{imageUrl}</div>
    </div>
  );
}
