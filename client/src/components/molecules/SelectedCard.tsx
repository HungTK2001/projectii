import { Link } from 'react-router-dom';

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function SelectedCard(data: any) {
  const { name, detail, moreDetail } = data.data;
  const { link, pos } = data;
  return (
    <Link to={link}>
      <div
        className='p-10 w-full  bg-white rounded-md  hover:bg-gradient-to-r hover:from-[#ec3380] hover:to-[#fea958] group cursor-pointer duration-500 delay-500'
        style={shadow}
      >
        <span className='block text-[#1f2471] p-3 font-bold text-[30px] text-center mb-4 leading-[1.2] group-hover:scale-105 duration-500 group-hover:text-white'>
          {name}
        </span>
        <div className='w-full flex justify-center items-center'>
          {pos % 3 == 0 && <img src='/assets/images/cate_img_10.png' alt='' />}
          {pos % 3 == 1 && <img src='/assets/images/cate_img_11.png' alt='' />}
          {pos % 3 == 2 && <img src='/assets/images/cate_img_12.png' alt='' />}
        </div>
        <div className='p-2'>
          <span className='block font-bold text-2xl mb-4 leading-[1.2] text-[#ef146e] group-hover:text-white group-hover:scale-105 duration-500'>
            {detail}
          </span>
          <span className='block text-[#505489] mt-2 group-hover:text-white'>{moreDetail}</span>
        </div>
      </div>
    </Link>
  );
}
