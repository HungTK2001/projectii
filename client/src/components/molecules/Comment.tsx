import { useContext, useState } from 'react';
import { toast } from 'react-toastify';
import { deleteCommentByID, updateCommentByID } from '../../api/user/discussion/request';
import { AuthContext } from '../../context/AuthContext';

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};

export default function Comment(props: any) {
  const { content, imageUrl, updateAt, userID, author, reload, setReload, id, pos } = props;
  const [toggleUpdateComment, setToggleUpdateComment] = useState(false);
  const [commentUpdateID, setCommentUpdateID] = useState(0);
  const [commentUpdateData, setCommentUpdateData] = useState({
    content: ''
  });
  const { token, user } = useContext(AuthContext);
  const handleToggleUpdateComment = () => {
    setToggleUpdateComment(true);
    setCommentUpdateData({ content: content });
  };
  const handleUpdateChangeComment = (event: any) => {
    setCommentUpdateData({ ...commentUpdateData, [event.target.name]: event.target.value });
  };
  const handleDeleteComment = async () => {
    const result = await deleteCommentByID(token, id);
    if (result.success) {
      toast.success('Delete comment successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete comment!');
    }
  };
  const handleUpdateComment = async (e: any) => {
    e.preventDefault();
    const result = await updateCommentByID(token, id, commentUpdateData);
    if (result) {
      toast.success('Update comment successful!');
      setToggleUpdateComment(false);
      setReload(reload + 1);
    } else {
      toast.error('Failed to update comment!');
    }
  };
  return (
    <div className='w-full justify-between mt-8 flex gap-5 p-4 rounded-md' style={shadow}>
      <div className='w-full'>
        <div className='w-[200px] bg-blue-300 rounded-sm' style={shadow}></div>
        <div className='w-full'>
          <div className='w-full flex justify-between items-center'>
            <div className='flex items-center justify-center gap-3'>
              {pos % 2 == 0 && <img src='/assets/images/avatar.png' alt='Avatar' width={48} height={48} />}
              {pos % 2 == 1 && <img src='/assets/images/avatar_2.png' alt='Avatar' width={48} height={48} />}
              <span className='block text-[#111942] leading-[18px] font-bold'>{author}</span>
            </div>
            <div className='flex items-center gap-2'>
              <svg width='30' height='30' viewBox='0 0 30 30' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M15 10V15L18.75 18.75L15 10ZM26.25 15C26.25 16.4774 25.959 17.9403 25.3936 19.3052C24.8283 20.6701 23.9996 21.9103 22.9549 22.9549C21.9103 23.9996 20.6701 24.8283 19.3052 25.3936C17.9403 25.959 16.4774 26.25 15 26.25C13.5226 26.25 12.0597 25.959 10.6948 25.3936C9.3299 24.8283 8.08971 23.9996 7.04505 22.9549C6.00039 21.9103 5.17172 20.6701 4.60636 19.3052C4.04099 17.9403 3.75 16.4774 3.75 15C3.75 12.0163 4.93526 9.15483 7.04505 7.04505C9.15483 4.93526 12.0163 3.75 15 3.75C17.9837 3.75 20.8452 4.93526 22.9549 7.04505C25.0647 9.15483 26.25 12.0163 26.25 15Z'
                  stroke='#f01f75'
                  strokeWidth='2'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                />
              </svg>
              <span className='block text-base font-normal text-[#f01f75]'>{updateAt}</span>
            </div>
          </div>
          <span className='block  text-[15px] text-[#505489] leading-[1.6] pl-2'>{content}</span>
          <span className='block  text-[15px] text-[#505489] leading-[1.6]'>{imageUrl}</span>
        </div>
      </div>
      {user !== null && user.id == userID && (
        <div className='flex items-end p-2 gap-3'>
          <i className='bi bi-pencil-square cursor-pointer text-blue-600' onClick={() => handleToggleUpdateComment()}></i>
          <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDeleteComment()}></i>
        </div>
      )}
      {toggleUpdateComment && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <div className=' p-8 pb-16 border border-solid bg-white rounded-xl' style={shadow}>
            <div className=' flex justify-end'>
              <i className='bi bi-x-lg hover:cursor-pointer' onClick={() => setToggleUpdateComment(false)}></i>
            </div>
            <form className='w-full'>
              <label htmlFor='content' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
                Cập nhật bình luận
              </label>
              <div className='flex gap-2 '>
                <textarea
                  id='content'
                  name='content'
                  placeholder='Please input your comment'
                  className='block w-full shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                  value={commentUpdateData.content}
                  onChange={e => handleUpdateChangeComment(e)}
                />
                <button
                  type='submit'
                  className='block w-24 text-sm md:text-base font-medium text-white bg-gradient-to-r from-[#ef146e] to-[#fea958]'
                  onClick={e => handleUpdateComment(e)}
                >
                  Cập nhật
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}
