const viewPdfBox = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function DocumentDetails(props: any) {
  const { name, title, googleDriveLink, updateAt } = props;
  console.log(googleDriveLink.indexOf('/view?'));
  return (
    <div className='w-full  rounded-md mt-10' style={viewPdfBox}>
      <div className='aspect-w-16 aspect-h-9 mb-2' style={viewPdfBox}>
        <iframe src={googleDriveLink.replace('/view?usp=drivesdk', '/preview')} frameBorder='0' allow='autoplay' allowFullScreen></iframe>
      </div>
      <div className='flex items-center p-3'>
        <svg width='24' height='26' viewBox='0 0 24 26' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <path
            d='M11.8988 8V13L15.5045 16.75L11.8988 8ZM22.716 13C22.716 14.4774 22.4362 15.9403 21.8926 17.3052C21.349 18.6701 20.5522 19.9103 19.5477 20.955C18.5432 21.9996 17.3508 22.8283 16.0383 23.3936C14.7259 23.959 13.3193 24.25 11.8988 24.25C10.4782 24.25 9.0716 23.959 7.7592 23.3936C6.44679 22.8283 5.25431 21.9996 4.24984 20.955C3.24536 19.9103 2.44857 18.6701 1.90496 17.3052C1.36134 15.9403 1.08154 14.4774 1.08154 13C1.08154 10.0163 2.22121 7.15483 4.24984 5.04505C6.27846 2.93526 9.02987 1.75 11.8988 1.75C14.7677 1.75 17.5191 2.93526 19.5477 5.04505C21.5763 7.15483 22.716 10.0163 22.716 13Z'
            stroke='#f01f75'
            strokeWidth='2'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
        <span className='text-[15px] block px-[12px] text-[#505489] font-medium'>{updateAt}</span>
      </div>
      <div className='p-3 pb-5'>
        <span className='block text-[1.5625rem] leading-[1.2] font-bold text-[#1f2471]'>{name}</span>
        <span className='block text-[15px] mt-3 leading-[1.6] text-[#505489]'>{title}</span>
        <span className='block mt-5'>
          <a href={googleDriveLink} className='underline cursor-pointer text-[#f01f75]' target='_blank' rel='noopener noreferrer'>
            Click vào đây để tải tài liệu
          </a>
        </span>
      </div>
    </div>
  );
}
