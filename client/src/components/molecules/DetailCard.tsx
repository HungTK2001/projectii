import { Link } from 'react-router-dom';

interface IDetailCard {
  detail: string;
  date: string;
  link: string;
  pos: number;
}

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function DetailCard(props: IDetailCard) {
  const { detail, date, link, pos } = props;
  return (
    <div className='w-full rounded-md mt-4 gap-2 p-[20px] cursor-pointer  group' style={shadow}>
      <div className='w-full h-[200px] min-w-[150px] max-w-[180] border-r border-solid justify-center  flex items-center group-hover:scale-105 duration-500 rounded-md'>
        {pos % 3 == 0 && <img src='/assets/images/cate_img.png' alt='' />}
        {pos % 3 == 1 && <img src='/assets/images/cate_img_5.png' alt='' />}
        {pos % 3 == 2 && <img src='/assets/images/cate_img_6.png' alt='' />}
      </div>
      <div className='mt-4'>
        <span className='block text-[#1f2471] text-[1.25rem] leading-[1.2] font-bold duration-500 group-hover:scale-105'>{detail}</span>
        <div className='flex justify-between mt-5'>
          <div className='flex items-center gap-2'>
            <svg width='24' height='26' viewBox='0 0 24 26' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M11.8988 8V13L15.5045 16.75L11.8988 8ZM22.716 13C22.716 14.4774 22.4362 15.9403 21.8926 17.3052C21.349 18.6701 20.5522 19.9103 19.5477 20.955C18.5432 21.9996 17.3508 22.8283 16.0383 23.3936C14.7259 23.959 13.3193 24.25 11.8988 24.25C10.4782 24.25 9.0716 23.959 7.7592 23.3936C6.44679 22.8283 5.25431 21.9996 4.24984 20.955C3.24536 19.9103 2.44857 18.6701 1.90496 17.3052C1.36134 15.9403 1.08154 14.4774 1.08154 13C1.08154 10.0163 2.22121 7.15483 4.24984 5.04505C6.27846 2.93526 9.02987 1.75 11.8988 1.75C14.7677 1.75 17.5191 2.93526 19.5477 5.04505C21.5763 7.15483 22.716 10.0163 22.716 13Z'
                stroke='#f01f75'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </svg>
            <span className='text-[15px] block text-[#505489] font-medium duration-500 group-hover:scale-105'>{date}</span>
          </div>
          <Link to={link}>
            <span className='block text-[15px] font-medium text-[#f01f75] duration-500 group-hover:scale-105 underline'>Read more ...</span>
          </Link>
        </div>
      </div>
    </div>
  );
}
