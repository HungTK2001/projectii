export default function Card() {
  return (
    <div className='flex flex-col col-span-full sm:col-span-6 xl:col-span-4 bg-white shadow-lg rounded-sm border border-slate-200'>
      <div className='px-5 pt-5'>
        <header className='flex justify-between items-start mb-2'>
          <div className='relative inline-flex'>
            <button className='text-slate-400 hover:text-slate-500 rounded-full false' aria-haspopup='true' aria-expanded='false'>
              <span className='sr-only'>Menu</span>
              <svg className='w-8 h-8 fill-current' viewBox='0 0 32 32'>
                <circle cx='16' cy='16' r='2'></circle>
                <circle cx='10' cy='16' r='2'></circle>
                <circle cx='22' cy='16' r='2'></circle>
              </svg>
            </button>
          </div>
        </header>
        <h2 className='text-lg font-semibold text-slate-800 mb-2'>Acme Plus</h2>
        <div className='text-xs font-semibold text-slate-400 uppercase mb-1'>Sales</div>
        <div className='flex items-start'>
          <div className='text-3xl font-bold text-slate-800 mr-2'>$24,780</div>
          <div className='text-sm font-semibold text-white px-1.5 bg-green-500 rounded-full'>+49%</div>
        </div>
      </div>
      <div className='grow'>
        <canvas width='472' height='248'></canvas>
      </div>
    </div>
  );
}
