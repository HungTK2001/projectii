import { ADMIN_ROUTER_FIRST_PART, ADMIN_ROUTER_SECOND_PART } from '../../../routes/ConstRoute';
import SearchBar from '../../molecules/SearchBar';
import SidebarItem from '../../molecules/SidebarItem';

export default function Sidebar() {
  return (
    <body className='bg-blue-600 hidden lg:block'>
      <div className='sidebar fixed top-0 bottom-0 lg:left-0 p-2 w-[300px] overflow-y-auto text-center bg-gray-900'>
        <div className='text-gray-100 text-xl'>
          <div className='p-2.5 mt-1 flex items-center'>
            <i className='bi bi-app-indicator px-2 py-1 rounded-md bg-blue-600'></i>
            <h1 className='font-bold text-gray-200 text-[15px] ml-3'>BK STUDY</h1>
          </div>
          <div className='my-2 bg-gray-600 h-[1px]'></div>
        </div>
        <SearchBar />
        {ADMIN_ROUTER_FIRST_PART.map((item, index) => (
          <SidebarItem title={item.title} path={item.path} icon={item.icon} key={index} />
        ))}
        <div className='my-4 bg-gray-600 h-[1px]'></div>
        {ADMIN_ROUTER_SECOND_PART.map((item, index) => (
          <SidebarItem title={item.title} path={item.path} icon={item.icon} key={index} />
        ))}
      </div>
    </body>
  );
}
