import { Link } from 'react-router-dom';

interface NavbarProps {
  toggleState: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
}
export default function Sidebar({ toggleState: [toggleNavbar, setToggleNavbar] }: NavbarProps) {
  return (
    <div className='fixed w-full flex lg:hidden top-0 left-0 z-50  h-full bg-black/60'>
      <div className='w-[280px] h-full bg-white'>
        <div className='px-[20px]'>
          <span className='block py-[25px]  text-center text-3xl text-[#262f5a] font-black'>BK STUDY</span>
          <ul className='block mt-[20px]'>
            <li className=' py-[8px] flex items-center border-b border-b-[#eee] border-b-solid'>
              <span className=' block  text-[15px] leading-[30px] text-[#262f5a] font-[600]'>
                <Link to='/'>Home</Link>
              </span>
            </li>
            <li className=' py-[8px] flex justify-between items-center border-b border-b-[#eee] border-b-solid'>
              <span className=' block  text-[15px] leading-[30px] text-[#262f5a]  font-[600]'>
                <Link to='/document'>Document</Link>
              </span>
              <div className='h-[30px] flex justify-center items-center w-[30px] bg-[#f01f75]'>
                <i className='bi bi-chevron-right text-white mt-1 font-bold'></i>
              </div>
            </li>
            <li className=' justify-between py-[8px] flex items-center border-b border-b-[#eee] border-b-solid'>
              <span className=' block  text-[15px] leading-[30px] text-[#262f5a]  font-[600]'>
                <Link to='/discussion'>Discussion</Link>
              </span>
              <div className='h-[30px] flex justify-center items-center w-[30px] bg-[#f01f75]'>
                <i className='bi bi-chevron-right text-white mt-1 font-bold'></i>
              </div>
            </li>
            <li className='justify-between py-[8px] flex items-center border-b border-b-[#eee] border-b-solid'>
              <span className=' block  text-[15px] leading-[30px] text-[#262f5a]  font-[600]'>
                <Link to='/quizzes'>Quizzes</Link>
              </span>
              <div className='h-[30px] flex justify-center items-center w-[30px] bg-[#f01f75]'>
                <i className='bi bi-chevron-right text-white mt-1 font-bold'></i>
              </div>
            </li>
          </ul>
          <div className='flex gap-4  w-full justify-center mt-4'>
            <a
              href=''
              className='w-[40px] h-[40px] rounded-full bg-white flex items-center justify-center border-solid border-[#f01f75] border'
            >
              <i className='bi bi-facebook text-[#f01f75] mt-1'></i>
            </a>
            <a
              href=''
              className='w-[40px] h-[40px] rounded-full bg-white flex items-center justify-center border-solid border-[#f01f75] border'
            >
              <i className='bi bi-twitter text-[#f01f75] mt-1'></i>
            </a>
            <a href='' className='w-[40px] h-[40px] rounded-full bg-white flex items-center justify-center border-[#f01f75] border'>
              <i className='bi bi-instagram text-[#f01f75] mt-1'></i>
            </a>
          </div>
        </div>
      </div>
      <div className=''>
        <i className='bi bi-x lg:hidden text-[30px] cursor-pointer text-white' onClick={() => setToggleNavbar(!toggleNavbar)}></i>
      </div>
    </div>
  );
}
