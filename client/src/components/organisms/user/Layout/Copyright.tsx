export default function Copyright() {
  return (
    <div className='w-full bg-[#121942]'>
      <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] py-[10px] mx-auto  bg-[#121942]'>
        <span className='text-[15px] text-center block w-full px-[15px] text-[#fff]'>Copyright © 2022 BK Study. All rights reserved.</span>
      </div>
    </div>
  );
}
