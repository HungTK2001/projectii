import { useContext, useState } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useScrollTo } from 'react-use-window-scroll';
import { AuthContext } from '../../../../context/AuthContext';
interface NavbarProps {
  toggleState: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
  toggleLogin: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
  toggleSignUp: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
}
export default function Navbar({
  toggleState: [toggleNavbar, setToggleNavbar],
  toggleLogin: [toggleLogin, setToggleLogin],
  toggleSignUp: [toggleSignUp, setToggleSignUp]
}: NavbarProps) {
  const { user, clearAuthData } = useContext(AuthContext);
  const [toggle, setToggle] = useState(false);
  const navigate = useNavigate();
  return (
    <div className='fixed w-full h-[85px] z-40 bg-white'>
      <nav className='flex justify-between w-full  xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[31px] mx-auto h-full items-center bg-white '>
        <h1 className='text-xl text-[#262f5a] font-bold block'>BK STUDY</h1>
        {!toggleNavbar && (
          <i className='bi bi-list block lg:hidden text-[30px] cursor-pointer' onClick={() => setToggleNavbar(!toggleNavbar)}></i>
        )}
        <div className='items-center hidden lg:flex '>
          <ul className='hidden lg:flex items-center space-x-6'>
            <li className='font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500 '>
              <Link to='/'>Home</Link>
            </li>
            <li className='font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500 '>
              <Link to='/document-category'>Document</Link>
            </li>
            <li className='font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500'>
              <Link to='/quiz-category'>Quizzes</Link>
            </li>
            <li className='font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500'>
              <Link to='/discussion-category'>Discussion</Link>
            </li>
            {!user && (
              <>
                <li>
                  <button
                    onClick={() => setToggleLogin(true)}
                    className='hover:rounded-br-xl hover:rounded-tl-xl w-[120px] ease-in-out hover:scale-105 duration-700 rounded-bl-xl rounded-tr-xl p-2 text-sm font-semibold text-white  bg-gradient-to-r from-[#2934cc] to-[#58a0fe]'
                  >
                    Log in
                  </button>
                </li>
                <li>
                  <button
                    onClick={() => setToggleSignUp(true)}
                    className='hover:rounded-br-xl hover:rounded-tl-xl  w-[120px] duration-700 hover:scale-105 rounded-bl-xl rounded-tr-xl p-2 text-sm font-semibold text-white  bg-gradient-to-r from-[#14ef14] to-[#58fe74]'
                  >
                    Sign up
                  </button>
                </li>
              </>
            )}
            {user && (
              <>
                <li
                  className='relative font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500'
                  onMouseOver={() => setToggle(true)}
                  onClick={() => setToggle(!toggle)}
                >
                  <img src='/assets/images/avatar.png' alt='Avatar' width={48} height={48} />
                  {toggle && (
                    <div className='w-48 bg-white absolute px-3 py-2 -left-12 top-14 border rounded-md'>
                      {user && (
                        <div className='font-medium text-[#f01f75] flex items-center'>
                          <img src='/assets/images/avatar.png' alt='Avatar' width={48} height={48} />
                          <span className='font-semibold text-[#262f5a]'>{user.fullName}</span>
                        </div>
                      )}
                      <ul>
                        <Link to={`owner-discussion/${user.id}`}>
                          <li className=' w-full flex items-center  py-1 px-2 gap-2 hover:cursor-pointer font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500'>
                            <i className='bi bi-chat-square-text-fill'></i>
                            <span className='inline-block'>Discussion</span>
                          </li>
                        </Link>
                        <Link to={`owner-quiz/${user.id}`}>
                          <li className='  w-full flex items-center py-1 px-2 gap-2 hover:cursor-pointer font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500'>
                            <i className='bi bi-question-circle-fill'></i>
                            <span className='inline-block'>Quiz</span>
                          </li>
                        </Link>
                        <li
                          className=' w-full flex items-center py-1 px-1 gap-2 hover:cursor-pointer font-semibold text-[#262f5a] hover:text-[#f01f75] cursor-pointer hover:scale-110 duration-500'
                          onClick={() => {
                            clearAuthData?.();
                            toast.success('Logout successful!');
                            navigate('/');
                          }}
                        >
                          <i className='bi bi-box-arrow-in-right'></i>
                          <span className='inline-block'>Log out</span>
                        </li>
                      </ul>
                    </div>
                  )}
                </li>
              </>
            )}
          </ul>
        </div>
      </nav>
    </div>
  );
}
