export default function NewFooter() {
  return (
    <div className='bg-gradient-to-r w-full  from-[#111c58] via-[#5c2782] to-[#111c58]'>
      <div className='pt-[70px] pb-[30px]'>
        <div className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto'>
          <div className='col-span-1 md:col-span-4 lg:col-span-1 px-0 xl:px-[15px] '>
            <span className='block font-black text-white text-4xl mb-[25px]'>BK STUDY</span>
            <span className='block font-[400px] text-white leading-[24px] mb-[20px] text-[15px]'>
              Tài liệu của các môn học từ cơ bản đến nâng cao và được cập nhật liên tục phù hợp với nhu cầu. Đông thời còn mang đến các bài
              thi trắc nghiệm phù hợp với mong muốn làm chủ kiến thức của các bạn
            </span>
            <div className='flex gap-1'>
              <a href='' className='w-[32px] h-[32px] rounded-full bg-white flex items-center justify-center'>
                <i className='bi bi-facebook text-[#f01f75] mt-1'></i>
              </a>
              <a href='' className='w-[32px] h-[32px] rounded-full bg-white flex items-center justify-center'>
                <i className='bi bi-twitter text-[#f01f75] mt-1'></i>
              </a>
              <a href='' className='w-[32px] h-[32px] rounded-full bg-white flex items-center justify-center'>
                <i className='bi bi-instagram text-[#f01f75] mt-1'></i>
              </a>
            </div>
          </div>
          <div className='col-span-1 mt-10 lg:mt-0'>
            <span className='block text-white pb-[10px] font-bold text-[24px] leading-[1.2]'>Our links</span>
            <div className='flex gap-[10px] w-full '>
              <div className='w-[15px] h-[4px] bg-white rounded-[20px]'></div>
              <div className='w-[45px] h-[4px] bg-white rounded-[20px]'></div>
            </div>
            <ul className='block mt-[20px]'>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Home</span>
              </li>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Document</span>
              </li>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Discussion</span>
              </li>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Quizzes</span>
              </li>
            </ul>
          </div>
          <div className='col-span-1 mt-10 lg:mt-0'>
            <span className='block text-white pb-[10px] font-bold text-[24px] leading-[1.2]'>Our links</span>
            <div className='flex gap-[10px] w-full '>
              <div className='w-[15px] h-[4px] bg-white rounded-[20px]'></div>
              <div className='w-[45px] h-[4px] bg-white rounded-[20px]'></div>
            </div>
            <ul className='block mt-[20px]'>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Home</span>
              </li>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Document</span>
              </li>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Discussion</span>
              </li>
              <li className=' py-[5px] flex items-center'>
                <i className='bi bi-chevron-right text-white font-semibold'></i>
                <span className=' block pl-[13px] text-[15px] text-[#fff] leading-[1.6] font-[400]'>Quizzes</span>
              </li>
            </ul>
          </div>
          <div className='col-span-1 mt-10 lg:mt-0'>
            <span className='block text-white pb-[10px] font-bold text-[24px] leading-[1.2]'>Contact Us</span>
            <div className='flex gap-[10px] w-full '>
              <div className='w-[15px] h-[4px] bg-white rounded-[20px]'></div>
              <div className='w-[45px] h-[4px] bg-white rounded-[20px]'></div>
            </div>
            <div className='flex gap-[10px] items-center mt-[20px]'>
              <div className='w-[40px] h-[40px] rounded-full bg-gradient-to-r from-[#ef146e] to-[#fea958] flex justify-center items-center'>
                <i className='bi bi-telephone-fill mt-1 text-white'></i>
              </div>
              <div>
                <span className='block text-[15px] text-[#fff] leading-[1.6] font-[400]'>+84 862-494-802</span>
                <span className='block text-[15px] text-[#fff] leading-[1.6] font-[400]'>+84 773-314-533</span>
              </div>
            </div>
            <div className='flex gap-[10px] items-center mt-[20px]'>
              <div className='w-[40px] h-[40px] rounded-full bg-gradient-to-r from-[#ef146e] to-[#fea958] flex justify-center items-center'>
                <i className='bi bi-envelope-fill mt-1 text-white'></i>
              </div>
              <div>
                <span className='block text-[13px] text-[#fff] leading-[1.6] font-[400]'>hungtk28102001@gmail.com</span>
                <span className='block text-[13px] text-[#fff] leading-[1.6] font-[400]'>hung.tk194294@sis.hust.edu.vn</span>
              </div>
            </div>
            <div className='flex gap-[10px] items-center mt-[20px]'>
              <div className='w-[40px] h-[40px] rounded-full bg-gradient-to-r from-[#ef146e] to-[#fea958] flex justify-center items-center'>
                <i className='bi bi-geo-alt-fill mt-1 text-white'></i>
              </div>
              <div>
                <span className='block text-[15px] text-[#fff] leading-[1.6] font-[400]'>Huong Khe, Ha Tinh, Viet Nam</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
