export default function Footer() {
  return (
    <div className=' w-full'>
      <img src='/assets/images/footer.png' alt='' className='w-screen aspect-auto' />
    </div>
  );
}
