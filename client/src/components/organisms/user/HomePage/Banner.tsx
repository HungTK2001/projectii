export default function Banner() {
  return (
    <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
      <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
        <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
          <div className='col-span-1 p-4 font-ssp'>
            <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>Nơi chia sẻ tài liệu học tập</span>
            <span className='block text-[30px] md:text-[30px] xl:text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>
              Hãy Học Khi Người Khác Ngủ, Lao Động Khi Người Khác Lười Nhác.
            </span>
            <span className='block text-[15px] leading-[1.6] mb-[30px] font-[400] text-white'>
              Tài liệu của các môn học từ cơ bản đến nâng cao và được cập nhật liên tục phù hợp với nhu cầu. Đông thời còn mang đến các bài
              thi trắc nghiệm phù hợp với mong muốn làm chủ kiến thức của các bạn.
            </span>
          </div>
          <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
        </div>
      </div>
    </div>
  );
}
