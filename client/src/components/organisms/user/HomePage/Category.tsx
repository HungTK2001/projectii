import CategoryCard from '../../../molecules/CategoryCard';

export default function Category() {
  const data = [
    {
      name: 'Tài liệu học tập',
      detail: 'Cung cấp tài liệu các môn học như giải tích, đại số, v.v',
      to: 1400
    },
    {
      name: 'Tạo và làm bài thi trắc nghiệm',
      detail: 'Tạo bài thi trắc nghiệm và chia sẻ cho những người bạn khác có thể truy cập và thử sức với nó.',
      to: 3350
    },
    {
      name: 'Thảo luận bài tập',
      detail: 'Cùng nhau chia sẻ các thắc mắc của mình cho những người khác để cùng nhau giải quyết nào.',
      to: 5350
    }
  ];
  return (
    <div className=''>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Danh Mục Chính</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='grid grid-cols-1 lg:flex gap-6 lg:gap-28 w-full justify-between'>
        {data.map((record, key) => (
          <CategoryCard name={record.name} detail={record.detail} to={record.to} key={key} />
        ))}
      </div>
    </div>
  );
}
