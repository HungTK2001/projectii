import { useContext, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getAllQuizCategory } from '../../../../api/user/quizzes/request';
import { AuthContext } from '../../../../context/AuthContext';
import SelectedCard from '../../../molecules/SelectedCard';

export default function QuizzesCategory() {
  const [quizDetails, setQuizDetails] = useState([]);
  const [page, setPage] = useState(1);
  const [reload, setReload] = useState(0);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);

  useEffect(() => {
    getAllQuizCategory(token, page, 9).then(result => {
      console.log(result);
      if (result.success) {
        setQuizDetails(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);
  return (
    <div className='mt-10'>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Bài thi trắc nghiệm</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='grid grid-cols-1 lg:grid-cols-3 gap-6 lg:gap-17 w-full justify-between mt-5'>
        {quizDetails.map((quizDetail: any, key) => (
          <SelectedCard key={key} data={quizDetail} link={`quiz-category/${quizDetail.id}`} pos={key}/>
        ))}
      </div>
      <div className='flex justify-end mt-3'>
        <Link to='/quiz-category'>
          <button className='hover:rounded-br-xl hover:rounded-tl-xl  hover:rounded-bl-[0] hover:rounded-tr-[0] duration-500 rounded-bl-xl rounded-tr-xl p-2 text-sm font-semibold text-white mt-5 bg-gradient-to-r from-[#ef146e] to-[#fea958]'>
            See more
          </button>
        </Link>
      </div>
    </div>
  );
}
