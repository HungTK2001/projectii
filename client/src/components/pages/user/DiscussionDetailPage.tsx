import moment from 'moment';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  createNewComment,
  deleteCommentByID,
  deleteDiscussionByID,
  getAllDiscussionCategory,
  getDiscussionByID,
  updateCommentByID,
  updateDiscussionByID
} from '../../../api/user/discussion/request';
import { AuthContext } from '../../../context/AuthContext';
import Comment from '../../molecules/Comment';
import DiscussionContent from '../../molecules/DiscussionContent';

const viewPdfBox = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function UserDiscussionDetail() {
  const { id } = useParams();
  const [discussion, setDiscussion] = useState({
    title: '',
    content: '',
    imageUrl: '',
    author: '',
    views: 0,
    discussionCategoryID: 0,
    discussionCategory: {
      name: ''
    },
    comment: [
      {
        id: 0,
        content: '',
        imageUrl: '',
        updateAt: '',
        author: 0,
        userID: 0
      }
    ]
  });
  const [discussionUpdateData, setDiscussionUpdateData] = useState({
    title: '',
    content: '',
    discussionCategoryID: 0
  });
  const [commentUpdateData, setCommentUpdateData] = useState({
    content: ''
  });

  const { register, control, handleSubmit, reset } = useForm({});
  const [reload, setReload] = useState(0);
  const [discussionCategories, setDiscussionCategories] = useState([]);
  const [toggleUpdateDiscussion, setToggleUpdateDiscussion] = useState(false);
  const [toggleUpdateComment, setToggleUpdateComment] = useState(false);
  const [commentUpdateID, setCommentUpdateID] = useState(0);
  const { token, user } = useContext(AuthContext);
  const [page, setPage] = useState(1);
  const navigate = useNavigate();

  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    getDiscussionByID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setDiscussion(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
    getAllDiscussionCategory(token, page, 9).then(result => {
      console.log(result);
      if (result.success) {
        setDiscussionCategories(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const handleToggleUpdateDiscussion = (data: any) => {
    setToggleUpdateDiscussion(true);
    setDiscussionUpdateData(data);
  };

  const handleToggleUpdateComment = (data: any) => {
    setToggleUpdateComment(true);
    setCommentUpdateData({ content: data.content });
    setCommentUpdateID(data.commentID);
  };
  const handleUpdateChangeDiscussion = (event: any) => {
    setDiscussionUpdateData({ ...discussionUpdateData, [event.target.name]: event.target.value });
  };
  const handleUpdateChangeComment = (event: any) => {
    setCommentUpdateData({ ...commentUpdateData, [event.target.name]: event.target.value });
  };

  const handleDeleteDiscussion = async (data: any) => {
    const result = await deleteDiscussionByID(token, data.id);
    if (result.success) {
      toast.success('Delete discussion successful!');
      navigate(`/owner-discussion/${user!.id}`);
    } else {
      toast.error('Failed to delete discussion!');
    }
  };
  const handleDeleteComment = async (data: any) => {
    const result = await deleteCommentByID(token, data.id);
    if (result.success) {
      toast.success('Delete comment successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete comment!');
    }
  };
  const onSubmit = async (data: any) => {
    const result = await createNewComment(token, {
      content: data.content,
      discussionID: id,
      userID: user!.id
    });
    if (result.success) {
      toast.success('Create new comment successful!');
      setReload(reload + 1);
    } else {
      if (result.message == 'Unauthorized') {
        toast.error('You are not login!');
      } else {
        toast.error(result.message);
      }
    }
  };

  const handleUpdateDiscussion = async (e: any) => {
    e.preventDefault();
    const result = await updateDiscussionByID(token, parseInt(id!), discussionUpdateData);
    if (result) {
      toast.success('Update discussion successful!');
      setToggleUpdateDiscussion(false);
      setReload(reload + 1);
    } else {
      toast.error('Failed to update quiz!');
    }
  };
  const handleUpdateComment = async (e: any) => {
    e.preventDefault();
    const result = await updateCommentByID(token, commentUpdateID, commentUpdateData);
    if (result) {
      toast.success('Update comment successful!');
      setToggleUpdateComment(false);
      setReload(reload + 1);
    } else {
      toast.error('Failed to update comment!');
    }
  };
  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>
                BK STUDY -- THẢO LUẬN -- CHI TIẾT
              </span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>
                {discussion.discussionCategory.name}
              </span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Author: {discussion.author}
              </span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Views: {discussion.views}
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Chi Tiết Bài Thảo Luận</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full 2xl:max-w-[1500px] xl:max-w-[1200px] lg:max-w-[1000px] md:max-w-[730px] sm:max-w-[600px] px-[15px] mx-auto'>
        <div className='flex justify-between rounded-md w-full mt-5' style={viewPdfBox}>
          <div>
            <div className='w-full p-6  '>
              <span className='text-[#1f2471] text-xl font-bold'>{discussion.title}</span>
            </div>
            <div className='w-full p-4'>{discussion.content}</div>
          </div>
          <div className='flex items-end p-2 gap-3'>
            <i
              className='bi bi-pencil-square cursor-pointer text-blue-600'
              onClick={() =>
                handleToggleUpdateDiscussion({
                  title: discussion.title,
                  content: discussion.content,
                  discussionCategoryID: discussion.discussionCategoryID
                })
              }
            ></i>
            <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDeleteDiscussion(discussion)}></i>
          </div>
        </div>
        <div className='mb-8'>
          <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Danh Sách Bình Luận</span>
          <div className='flex gap-[10px] w-full justify-center'>
            <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
            <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          </div>
        </div>
        <form className='w-full' onSubmit={handleSubmit(onSubmit)}>
          <label htmlFor='content' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
            Bình luận mới
          </label>
          <div className='flex gap-2 '>
            <textarea
              id='content'
              {...register('content')}
              placeholder='Please input your comment'
              className='block w-full shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            />
            <button
              type='submit'
              className='block w-24 text-sm md:text-base font-medium text-white bg-gradient-to-r from-[#ef146e] to-[#fea958]'
            >
              Tạo mới
            </button>
          </div>
        </form>
        {discussion.comment.map((record, key) => (
          <div className='mt-8 w-full flex gap-5 p-4 rounded-md justify-between' style={viewPdfBox} key={key}>
            <div className='w-full'>
              <div className='w-full'>
                <div className='w-full flex justify-between items-center'>
                  <span className='block text-[#111942] leading-[18px] font-bold'>Author: {record.author}</span>
                  <div className='flex items-center gap-2'>
                    <svg width='30' height='30' viewBox='0 0 30 30' fill='none' xmlns='http://www.w3.org/2000/svg'>
                      <path
                        d='M15 10V15L18.75 18.75L15 10ZM26.25 15C26.25 16.4774 25.959 17.9403 25.3936 19.3052C24.8283 20.6701 23.9996 21.9103 22.9549 22.9549C21.9103 23.9996 20.6701 24.8283 19.3052 25.3936C17.9403 25.959 16.4774 26.25 15 26.25C13.5226 26.25 12.0597 25.959 10.6948 25.3936C9.3299 24.8283 8.08971 23.9996 7.04505 22.9549C6.00039 21.9103 5.17172 20.6701 4.60636 19.3052C4.04099 17.9403 3.75 16.4774 3.75 15C3.75 12.0163 4.93526 9.15483 7.04505 7.04505C9.15483 4.93526 12.0163 3.75 15 3.75C17.9837 3.75 20.8452 4.93526 22.9549 7.04505C25.0647 9.15483 26.25 12.0163 26.25 15Z'
                        stroke='#f01f75'
                        strokeWidth='2'
                        strokeLinecap='round'
                        strokeLinejoin='round'
                      />
                    </svg>
                    <span className='block text-base font-normal text-[#f01f75]'>{moment(parseInt(record.updateAt)).format('LL')}</span>
                  </div>
                </div>
                <span className='block  text-[15px] text-[#505489] leading-[1.6]'>{record.content}</span>
                <span className='block  text-[15px] text-[#505489] leading-[1.6]'>{record.imageUrl}</span>
                <div className='flex gap-3 group cursor-pointer'>
                  <i className='bi bi-reply-fill text-[#f01f75] text-[15px] leading-[1.6] group-hover:text-[#505489]'></i>
                  <span className='block font-bold text-[15px] text-[#f01f75] leading-[1.6] group-hover:text-[#505489]'>Reply</span>
                </div>
              </div>
            </div>
            <div className='flex items-end p-2 gap-3'>
              {user?.id == record.userID && (
                <i
                  className='bi bi-pencil-square cursor-pointer text-blue-600'
                  onClick={() =>
                    handleToggleUpdateComment({
                      content: record.content,
                      commentID: record.id
                    })
                  }
                ></i>
              )}
              <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDeleteComment(record)}></i>
            </div>
          </div>
        ))}
      </div>
      {toggleUpdateDiscussion && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <div className=' p-4 border border-solid bg-white rounded-xl' style={viewPdfBox}>
            <form>
              <div className=' flex justify-end'>
                <i className='bi bi-x-lg hover:cursor-pointer' onClick={() => setToggleUpdateDiscussion(false)}></i>
              </div>
              <label htmlFor='title' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
                Tiêu đề bài thảo luận
              </label>
              <textarea
                name='title'
                value={discussionUpdateData.title}
                placeholder='Please input your title'
                className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                onChange={e => handleUpdateChangeDiscussion(e)}
              />
              <label htmlFor='title' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
                Nội dung bài thảo luận
              </label>
              <textarea
                name='content'
                value={discussionUpdateData.content}
                placeholder='Please input your content'
                rows={10}
                cols={50}
                className='shadow appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                onChange={e => handleUpdateChangeDiscussion(e)}
              />
              <label htmlFor='discussion-category' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
                Chủ đề bài thảo luận
              </label>
              <select
                id='discussion-category'
                className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                name='discussionCategoryID'
                value={discussionUpdateData.discussionCategoryID}
                onChange={e => handleUpdateChangeDiscussion(e)}
              >
                {discussionCategories.map((discussionCategory: any, index) => (
                  <option value={discussionCategory.id} className='bg-white w-full' key={index}>
                    {discussionCategory.name}
                  </option>
                ))}
              </select>
              <div className='w-full p-1 flex justify-center'>
                <button
                  type='submit'
                  className='hover:rounded-br-3xl hover:rounded-tl-3xl  hover:rounded-bl-[0] hover:rounded-tr-[0] duration-500 rounded-bl-3xl rounded-tr-3xl p-3 text-sm md:text-xl font-semibold text-white mt-10 bg-gradient-to-r from-[#ef146e] to-[#fea958]'
                  onClick={e => handleUpdateDiscussion(e)}
                >
                  Cập nhật bài thảo luận
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
      {toggleUpdateComment && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <div className=' p-4 border border-solid bg-white rounded-xl' style={viewPdfBox}>
            <div className=' flex justify-end'>
              <i className='bi bi-x-lg hover:cursor-pointer' onClick={() => setToggleUpdateComment(false)}></i>
            </div>
            <form className='w-full'>
              <label htmlFor='content' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
                Bình luận mới
              </label>
              <div className='flex gap-2 '>
                <textarea
                  id='content'
                  name='content'
                  placeholder='Please input your comment'
                  className='block w-full shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                  value={commentUpdateData.content}
                  onChange={e => handleUpdateChangeComment(e)}
                />
                <button
                  type='submit'
                  className='block w-24 text-sm md:text-base font-medium text-white bg-gradient-to-r from-[#ef146e] to-[#fea958]'
                  onClick={e => handleUpdateComment(e)}
                >
                  Tạo mới
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}
