import { useContext, useEffect, useState } from 'react';
import Countdown from 'react-countdown';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { checkAnswer, findUserQuizWithQuizID, findWithQuizID } from '../../../api/user/quizzes/request';
import { AuthContext } from '../../../context/AuthContext';
import CountdownRenderer from '../../molecules/Countdown';
import Question from '../../molecules/Question';

export default function DoQuizPage() {
  const { register, control, handleSubmit, reset } = useForm({});
  const [data, setData] = useState({
    name: '',
    author: '',
    timeLimit: 0,
    questions: []
  });
  const { token, user } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [toggle, setToggle] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [score, setScore] = useState(0);
  const [finish, setFinish] = useState(false);
  const [quizStartAt, setQuizStartAt] = useState(0);
  useEffect(() => {
    reset();
    findUserQuizWithQuizID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setData(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
    setQuizStartAt(Date.now());
  }, []);
  const onSubmit = async (data: any) => {
    const result = await checkAnswer(token, {
      userID: user?.id,
      quizID: id,
      answers: data,
      quizStartAt: quizStartAt,
      quizEndAt: Date.now()
    });
    if (result.success) {
      toast.success(`Your score is ${result.data.score}`);
      setScore(result.data.score);
      setFinish(true);
    } else {
      toast.error(result.message);
    }
  };

  if (data.timeLimit == 0) {
    return <></>;
  }

  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>
                BK STUDY -- TRẮC NGHIỆM -- CHI TIẾT
              </span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>{data.name}</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>Author: {data.author}</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Time limit: {data.timeLimit} minutes
              </span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Total question: {data.questions.length} questions
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Chi Tiết Bài Thi</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full 2xl:max-w-[1500px] xl:max-w-[1200px] lg:max-w-[1000px] md:max-w-[730px] sm:max-w-[600px] px-[15px] mx-auto'>
        {!finish && <Countdown date={Date.now() + data.timeLimit * 60 * 1000} renderer={CountdownRenderer} />}
        {finish && (
          <div className='flex w-full justify-end'>
            <span className='block text-right font-medium text-base p-2 border-2 border-blue-500 border-solid rounded-md'>
              Điểm số của bạn là:
              <span className='font-bold text-lg text-blue-600'>
                {score}/{data.questions.length * 10}
              </span>
            </span>
          </div>
        )}
        <form onSubmit={handleSubmit(onSubmit)}>
          {data.questions.map((question, key) => (
            <Question key={key} data={question} register={register} />
          ))}
          {!finish && (
            <div className='flex justify-center'>
              <button
                id='submit'
                type='submit'
                className='hover:rounded-br-3xl hover:rounded-tl-3xl  hover:rounded-bl-[0] hover:rounded-tr-[0] duration-500 rounded-bl-3xl rounded-tr-3xl p-3 text-sm md:text-xl font-semibold text-white mt-10 bg-gradient-to-r from-[#ef146e] to-[#fea958]'
              >
                Nộp bài
              </button>
            </div>
          )}
        </form>
      </div>
    </div>
  );
}
