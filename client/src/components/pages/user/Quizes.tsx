import moment from 'moment';
import { useContext, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getQuizCategoryByID } from '../../../api/user/quizzes/request';
import { AuthContext } from '../../../context/AuthContext';
import DetailCard from '../../molecules/DetailCard';

export default function UserQuizzes() {
  const [quizzes, setQuizzes] = useState([]);
  const [page, setPage] = useState(1);
  const [reload, setReload] = useState(0);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const { id } = useParams();
  useEffect(() => {
    getQuizCategoryByID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setQuizzes(result.data.quiz);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);
  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>BK STUDY -- TRẮC NGHIỆM</span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>Trắc Nghiệm Online</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Bao gồm bài thi trắc nghiệm các môn học như giải tích, đại số, v.v. Tài liệu được cập nhật liên tục dựa trên tài liệu được
                chia sẻ và sưu tầm của các quản trị viên.
              </span>
              <Link to={'/quiz/create'}>
                <button className='hover:rounded-br-3xl hover:rounded-tl-3xl  hover:rounded-bl-[0] hover:rounded-tr-[0] duration-500 rounded-bl-3xl rounded-tr-3xl p-3 text-sm md:text-xl font-semibold text-white mt-10 bg-gradient-to-r from-[#ef146e] to-[#fea958]'>
                  Bài trắc nghiệm mới
                </button>
              </Link>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Danh Sách Bài Tập Trắc Nghiệm</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full lg:grid lg:grid-cols-3 gap-3 xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto'>
        {quizzes.map((record: any, key) => (
          <DetailCard
            date={moment(parseInt(record.createAt)).format('LL')}
            link={`/quiz/${record.id}`}
            detail={record.name}
            key={key}
            pos={key}
          />
        ))}
      </div>
    </div>
  );
}
