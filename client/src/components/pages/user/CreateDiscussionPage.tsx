import { useContext, useEffect, useState } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { createDiscussion, getAllDiscussionCategory } from '../../../api/user/discussion/request';
import { AuthContext } from '../../../context/AuthContext';

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function CreateDiscussionPage() {
  const [discussionCategories, setDiscussionCategories] = useState([]);
  const [page, setPage] = useState(1);
  const [reload, setReload] = useState(0);
  const { register, control, handleSubmit, reset } = useForm({});
  const navigate = useNavigate();
  const { token, user } = useContext(AuthContext);
  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    getAllDiscussionCategory(token, page, 9).then(result => {
      console.log(result);
      if (result.success) {
        setDiscussionCategories(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const onSubmit = async (data: any) => {
    const result = await createDiscussion(token, {
      ...data,
      userID: user!.id
    });
    if (result.success) {
      toast.success('Create new discussion successful!');
      console.log(result);
      navigate(`/discussion-detail/${result.data.id}`);
    } else {
      if (result.message == 'Unauthorized') {
        toast.error('You are not login!');
      } else {
        toast.error(result.message);
      }
    }
  };

  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>BK STUDY -- THẢO LUẬN</span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>Thảo Luận Bài Tập</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Các bạn có thể vào đây để nêu ra các khó khăn của bạn. Mọi người sẽ giúp đỡ bạn hoàn thành nó.
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Tạo bài thảo luận mới</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full gap-3 xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto'>
        <div className=' p-4 border border-solid bg-white rounded-xl' style={shadow}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <label htmlFor='title' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
              Tiêu đề bài thảo luận
            </label>
            <textarea
              id='title'
              {...register('title')}
              placeholder='Please input your title'
              className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            />
            <label htmlFor='title' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
              Nội dung bài thảo luận
            </label>
            <textarea
              {...register('content')}
              placeholder='Please input your content'
              rows={10}
              cols={50}
              className='shadow appearance-none border rounded w-full py-5 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            />
            <label htmlFor='discussion-category' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
              Chủ đề bài thảo luận
            </label>
            <select
              id='discussion-category'
              className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
              {...register('discussionCategoryID')}
            >
              {discussionCategories.map((discussionCategory: any, index) => (
                <option value={discussionCategory.id} className='bg-white w-full' key={index}>
                  {discussionCategory.name}
                </option>
              ))}
            </select>
            <div className='w-full p-1 flex justify-center'>
              <button
                type='submit'
                className='hover:rounded-br-3xl hover:rounded-tl-3xl  hover:rounded-bl-[0] hover:rounded-tr-[0] duration-500 rounded-bl-3xl rounded-tr-3xl p-3 text-sm md:text-xl font-semibold text-white mt-10 bg-gradient-to-r from-[#ef146e] to-[#fea958]'
              >
                Tạo mới bài thảo luận
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
