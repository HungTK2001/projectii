import moment from 'moment';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { createNewComment, getDiscussionByID } from '../../../api/user/discussion/request';
import { AuthContext } from '../../../context/AuthContext';
import Comment from '../../molecules/Comment';
import DiscussionContent from '../../molecules/DiscussionContent';
import LoginPage from './Login';

export default function DiscussionDetailPage() {
  const { id } = useParams();
  const [discussion, setDiscussion] = useState({
    title: '',
    content: '',
    imageUrl: '',
    author: '',
    views: 0,
    discussionCategory: {
      name: ''
    },
    comment: [
      {
        id: 0,
        userID: 0,
        content: '',
        imageUrl: '',
        updateAt: '',
        author: 0
      }
    ]
  });
  const { register, control, handleSubmit, reset } = useForm({});
  const [reload, setReload] = useState(0);
  const [toggle, setToggle] = useState(false);
  const { token, user, isLogin } = useContext(AuthContext);
  const navigate = useNavigate();
  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    getDiscussionByID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setDiscussion(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);
  const onSubmit = async (data: any) => {
    if (!isLogin) {
      setToggle(true);
    } else {
      const result = await createNewComment(token, {
        content: data.content,
        discussionID: id,
        userID: user!.id
      });
      if (result.success) {
        toast.success('Create new comment successful!');
        setReload(reload + 1);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
        } else {
          toast.error(result.message);
        }
      }
    }
  };
  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>
                BK STUDY -- THẢO LUẬN -- CHI TIẾT
              </span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>
                {discussion.discussionCategory.name}
              </span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Author: {discussion.author}
              </span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Views: {discussion.views}
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Chi Tiết Bài Thảo Luận</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full 2xl:max-w-[1500px] xl:max-w-[1200px] lg:max-w-[1000px] md:max-w-[730px] sm:max-w-[600px] px-[15px] mx-auto'>
        <DiscussionContent content={discussion.content} title={discussion.title} imageUrl={discussion.imageUrl} />
        <div className='mb-8'>
          <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Danh Sách Bình Luận</span>
          <div className='flex gap-[10px] w-full justify-center'>
            <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
            <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          </div>
        </div>
        <form className='w-full' onSubmit={handleSubmit(onSubmit)}>
          <label htmlFor='content' className='block text-[#1f2471] font-medium text-[17px] leading-[1.2]'>
            Bình luận mới
          </label>
          <div className='flex gap-2 '>
            <textarea
              id='content'
              {...register('content')}
              placeholder='Please input your comment'
              className='block w-full shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            />
            <button
              type='submit'
              className='block w-24 text-sm md:text-base font-medium text-white bg-gradient-to-r from-[#ef146e] to-[#fea958]'
            >
              Tạo mới
            </button>
          </div>
        </form>
        {discussion.comment.map((record, key) => (
          <Comment
            key={key}
            pos={key}
            id={record.id}
            content={record.content}
            imageUrl={record.imageUrl}
            updateAt={moment(parseInt(record.updateAt)).format('LL')}
            userID={record.userID}
            author={record.author}
            reload={reload}
            setReload={setReload}
          />
        ))}
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <LoginPage setToggleLogin={setToggle} title='You must login to try this action!' />
        </div>
      )}
    </div>
  );
}
