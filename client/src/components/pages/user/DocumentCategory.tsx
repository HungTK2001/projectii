import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getAllDocumentCategory } from '../../../api/user/document/request';
import { AuthContext } from '../../../context/AuthContext';
import ContentCard from '../../molecules/ContentCard';

export default function UserDocumentCategory() {
  const [documentCategories, setDocumentCategories] = useState([]);
  const [page, setPage] = useState(1);
  const [reload, setReload] = useState(0);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);

  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    getAllDocumentCategory(token, page, 9).then(result => {
      console.log(result);
      if (result.success) {
        setDocumentCategories(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);
  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom'>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>BK STUDY -- TÀI LIỆU</span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>Tài Liệu Học Tập</span>
              <span className='block text-[20px] leading-[1.6] mb-[30px] font-[400] text-white'>
                Bao gồm tài liệu các môn học như giải tích, đại số, v.v. Tài liệu được cập nhật liên tục dựa trên tài liệu được chia sẻ và
                sưu tầm của các quản trị viên.
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Danh Sách Tài Liệu</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full lg:grid lg:grid-cols-2 gap-3 xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto'>
        {documentCategories.map((documentCategory: any, key) => (
          <ContentCard
            date={documentCategory.createAt}
            link={`/document-category/${documentCategory.id}`}
            detail={documentCategory.detail}
            name={documentCategory.name}
            moreDetail={documentCategory.moreDetail}
            key={key}
            pos={key}
          />
        ))}
      </div>
    </div>
  );
}
