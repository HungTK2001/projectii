import { useContext, useEffect, useState } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { findUserQuizWithQuizID, findWithQuizID } from '../../../api/user/quizzes/request';
import { AuthContext } from '../../../context/AuthContext';
import Question from '../../molecules/Question';
import LoginPage from './Login';

const viewPdfBox = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function QuizzDetailPage() {
  const [data, setData] = useState({
    name: '',
    author: '',
    timeLimit: 0,
    questions: [
      {
        multiChoice: false,
        content: '',
        answers: [
          {
            content: ''
          }
        ]
      }
    ]
  });
  const { token, isLogin } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [toggle, setToggle] = useState(false);
  useEffect(() => {
    findUserQuizWithQuizID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setData(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, []);

  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>
                BK STUDY -- TRẮC NGHIỆM -- CHI TIẾT
              </span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>{data.name}</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>Author: {data.author}</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Time limit: {data.timeLimit} minutes
              </span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Total question: {data.questions.length} questions
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Chi Tiết Bài Thi</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full 2xl:max-w-[1500px] xl:max-w-[1200px] lg:max-w-[1000px] md:max-w-[730px] sm:max-w-[600px] px-[15px] mx-auto'>
        <form>
          {data.questions.map((question, key) => (
            <div className='rounded-md w-full mt-5' style={viewPdfBox} key={key}>
              <div className='w-full p-6  '>
                <span className='text-[#1f2471] text-xl font-bold'>{question.content}</span>
              </div>
              <div className='w-full p-4'>
                {question.answers.map((answer: any, index: number) => (
                  <div className='my-2' key={index}>
                    <label className='flex items-center gap-3'>
                      <div>
                        <input type={question.multiChoice ? 'checkbox' : 'radio'} disabled></input>
                      </div>
                      <label className='text-xs  block'>{answer.content}</label>
                    </label>
                  </div>
                ))}
              </div>
            </div>
          ))}
          <div className='flex justify-center'>
            <button
              className='hover:rounded-br-3xl hover:rounded-tl-3xl  hover:rounded-bl-[0] hover:rounded-tr-[0] duration-500 rounded-bl-3xl rounded-tr-3xl p-3 text-sm md:text-xl font-semibold text-white mt-10 bg-gradient-to-r from-[#ef146e] to-[#fea958]'
              onClick={e => {
                e.preventDefault();
                if (isLogin) {
                  navigate(`/do-quiz/${id}`);
                } else {
                  setToggle(true);
                }
              }}
            >
              Làm bài thi
            </button>
          </div>
        </form>
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <LoginPage setToggleLogin={setToggle} title='You must login to do this action!' />
        </div>
      )}
    </div>
  );
}
