import { useContext, useEffect, useState } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getAllDiscussionCategory } from '../../../api/user/discussion/request';
import {
  createNewQuestion,
  createNewQuiz,
  deleteQuestionByID,
  findWithQuizID,
  getAllQuizCategory
} from '../../../api/user/quizzes/request';
import { AuthContext } from '../../../context/AuthContext';
import UserQuestionCard from '../../molecules/UserQuestionCard';
import UserUpdateQuestion from '../../molecules/UserUpdateQuestion';

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function CreateQuiz() {
  const [discussionCategories, setDiscussionCategories] = useState([]);
  const [page, setPage] = useState(1);
  const {
    register,
    control,
    handleSubmit,
    reset,

    formState: { errors }
  } = useForm({});

  const [reload, setReload] = useState(0);
  const [data, setData] = useState([]);
  const { token, user } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [toggle, setToggle] = useState(false);
  const [question, setQuestion] = useState();
  const [quizCategories, setQuizCategories] = useState([]);
  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    getAllQuizCategory(token, page, 10).then(result => {
      if (result.success) {
        setQuizCategories(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);
  const handleCreate = async (data: any) => {
    const result = await createNewQuiz(token, {
      ...data,
      timeLimit: parseInt(data.timeLimit),
      userID: user!.id
    });
    if (result.success) {
      toast.success('Create new quiz successful!');
      console.log(result);
      navigate(`/quiz-detail/${result.data.id}`);
    } else {
      if (result.message == 'Unauthorized') {
        toast.error('You are not login!');
      } else {
        toast.error(result.message);
      }
    }
  };

  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>BK STUDY -- TRẮC NGHIỆM</span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>Trắc Nghiệm Online</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Các bạn có thể vào đây để nêu ra các khó khăn của bạn. Mọi người sẽ giúp đỡ bạn hoàn thành nó.
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Tạo các bài quiz mới</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full gap-3 xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto'>
        <form onSubmit={handleSubmit(handleCreate)} className='rounded-lg bg-white p-10'>
          <div className='mb-5 text-red-500 text-xs'>
            <label htmlFor='name' className='text-sm font-medium text-gray-600'>
              Quiz name
            </label>
            <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
              <textarea
                id='name'
                className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                placeholder='Please type your quiz name '
                cols={40}
                rows={3}
                {...register('name', { required: true, minLength: 1 })}
              />
            </div>
            {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
            {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
          </div>
          <div className='mb-5 text-red-500 text-xs'>
            <label htmlFor='name' className='text-sm font-medium text-gray-600'>
              Time limit (minutes)
            </label>
            <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
              <input
                id='timeLimit'
                type='number'
                className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                placeholder='Time limit (minutes) '
                {...register('timeLimit', { required: true, min: 1 })}
              />
            </div>
            {errors?.timeLimit?.type === 'required' && <p>⚠ This field is required!</p>}
            {errors?.timeLimit?.type === 'min' && <p>⚠ Time limit must be older than 1 minute!</p>}
          </div>
          <div className='mb-5 text-red-500 text-xs'>
            <label htmlFor='quizCategoryID' className='text-sm font-medium text-gray-600'>
              Quiz category
            </label>
            <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl'>
              <select
                id='quizCategoryID'
                className=' pl-2 w-full outline-none border-none hover:cursor-pointer text-base text-[#757575] bg-white'
                {...register('quizCategoryID', { required: true, minLength: 1 })}
              >
                {quizCategories.map((quizCategory: any, index) => (
                  <option value={quizCategory.id} className='bg-white w-full' key={index}>
                    {quizCategory.name}
                  </option>
                ))}
              </select>
            </div>
            {errors?.documentCategoryID?.type === 'required' && <p>⚠ This field is required!</p>}
          </div>
          <div className='flex justify-center'>
            <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium'>
              Go to create question page
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
