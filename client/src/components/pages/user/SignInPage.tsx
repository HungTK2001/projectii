import { useContext } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { registerUser } from '../../../api/user/auth/request';
import { AuthContext } from '../../../context/AuthContext';

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function SignInPage(props: any) {
  const { setToggleSignUp } = props;
  const { user } = useContext(AuthContext);
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({});
  const onSubmit = async (data: any) => {
    if (data.password !== data.repeat_password) {
      toast.error('Password must compare with repeat password!');
    } else {
      const result = await registerUser({ ...data, roleID: 2 });
      if (result.success) {
        toast.success('Create new user successful!');
        setToggleSignUp(false);
      } else {
        toast.error('Failed to create new user!');
      }
    }
  };
  return (
    <div className=' w-full flex justify-center py-[10px] gap-6' id='signup'>
      <div className='lg:w-2/5 md:1/2 w-full p-[10px] '>
        <div className='p-[50px] bg-white rounded-md h-full' style={shadow}>
          <div className='flex justify-end'>
            <i className='bi bi-x-lg hover:cursor-pointer' onClick={() => setToggleSignUp(false)}></i>
          </div>
          <div className='mb-8'>
            <span className='block text-[#1f2471]  font-bold text-[45px] mb-2 leading-[1.2]'>Join with us</span>
            <div className='flex gap-[10px] w-full'>
              <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
              <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
            </div>
          </div>

          <form onSubmit={handleSubmit(onSubmit)}>
            <div className='mb-5'>
              <input
                type='text'
                {...register('fullName', { required: true, minLength: 1 })}
                id='fullName'
                placeholder='Full Name'
                style={shadow}
                className='w-full rounded-bl-3xl rounded-tr-3xl  border border-[#e0e0e0] focus:border-[#ef146e] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none  focus:shadow-md'
              />
              {errors?.fullName?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.fullName?.type === 'minLength' && <p>⚠ Full name cannot be less than 6 characters!</p>}
            </div>
            <div className='mb-5'>
              <input
                type='email'
                {...register('email', { required: true, minLength: 1 })}
                id='email'
                placeholder='Email Address'
                style={shadow}
                className='w-full rounded-bl-3xl rounded-tr-3xl  border border-[#e0e0e0] focus:border-[#ef146e] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none  focus:shadow-md'
              />
              {errors?.email?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.email?.type === 'minLength' && <p>⚠ Email cannot be less than 6 characters!</p>}
            </div>
            <div className='mb-5'>
              <input
                type='text'
                {...register('username', { required: true, minLength: 6 })}
                id='username'
                placeholder='Username'
                style={shadow}
                className='w-full rounded-bl-3xl rounded-tr-3xl  border border-[#e0e0e0] focus:border-[#ef146e] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none  focus:shadow-md'
              />
              {errors?.username?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.username?.type === 'minLength' && <p>⚠ Username cannot be less than 6 characters!</p>}
            </div>
            <div className=' flex gap-5'>
              <div className='mb-5'>
                <input
                  type='password'
                  {...register('password', { required: true, minLength: 6 })}
                  id='password'
                  placeholder='Password'
                  style={shadow}
                  className='w-full rounded-bl-3xl rounded-tr-3xl  border border-[#e0e0e0] focus:border-[#ef146e] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none  focus:shadow-md'
                />
                {errors?.password?.type === 'required' && <p>⚠ This field is required!</p>}
                {errors?.password?.type === 'minLength' && <p>⚠ Password cannot be less than 6 characters!</p>}
              </div>
              <div className='mb-5'>
                <input
                  type='password'
                  {...register('repeat_password', { required: true, minLength: 6 })}
                  id='repeat_password'
                  placeholder='Repeat password'
                  style={shadow}
                  className='w-full rounded-bl-3xl rounded-tr-3xl  border border-[#e0e0e0] focus:border-[#ef146e] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none  focus:shadow-md'
                />
                {errors?.repeat_password?.type === 'required' && <p>⚠ This field is required!</p>}
                {errors?.repeat_password?.type === 'minLength' && <p>⚠ Repeat password cannot be less than 6 characters!</p>}
              </div>
            </div>
            <div>
              <button
                type='submit'
                className='w-full text-center hover:rounded-bl-3xl hover:rounded-tr-3xl  hover:rounded-br-[0] hover:rounded-tl-[0] duration-500 rounded-br-3xl rounded-tl-3xl py-[12px] px-[35px] text-[17px] font-[500] text-white mt-10 bg-gradient-to-r from-[#ef146e] to-[#fea958] '
              >
                Sign up now
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
