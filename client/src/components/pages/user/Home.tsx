import { useEffect } from 'react';
import Banner from '../../organisms/user/HomePage/Banner';
import Category from '../../organisms/user/HomePage/Category';
import DiscussionCategory from '../../organisms/user/HomePage/DiscussionCategory';
import DocumentCategory from '../../organisms/user/HomePage/DocumentCategory';
import QuizzCategory from '../../organisms/user/HomePage/QuizzCategory';
import SignIn from '../../organisms/user/HomePage/SignIn';

export default function UserHomePage() {
  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  }, []);
  return (
    <div className='font-ssp'>
      <Banner />
      <div className='w-full 2xl:max-w-[1500px] xl:max-w-[1200px] lg:max-w-[1000px] md:max-w-[730px] sm:max-w-[600px] px-[15px] mx-auto'>
        <Category />
        <DocumentCategory />
        <QuizzCategory />
        <DiscussionCategory />
        <SignIn />{' '}
      </div>
    </div>
  );
}
