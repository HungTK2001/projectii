import { useContext, useEffect, useState } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getAllDiscussionCategory } from '../../../api/user/discussion/request';
import {
  createNewQuestion,
  deleteQuestionByID,
  deleteQuizByID,
  findWithQuizID,
  getAllQuizCategory,
  updateQuizByID
} from '../../../api/user/quizzes/request';
import { AuthContext } from '../../../context/AuthContext';
import UserQuestionCard from '../../molecules/UserQuestionCard';
import UserUpdateQuestion from '../../molecules/UserUpdateQuestion';
import QuizCategory from '../admin/Quizzes/QuizzCategory';

const shadow = {
  boxShadow: '0 0 10px rgb(31 66 135 / 30%)'
};
export default function CreateQuizPage() {
  const [discussionCategories, setDiscussionCategories] = useState([]);
  const [quizCategories, setQuizCategories] = useState([]);
  const [page, setPage] = useState(1);
  const {
    register,
    control,
    handleSubmit,
    reset,

    formState: { errors }
  } = useForm({});
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [reload, setReload] = useState(0);
  const [data, setData] = useState({
    name: '',
    quizCategoryID: 0,
    questions: []
  });
  const { token } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [updateData, setUpdateData] = useState({
    name: '',
    timeLimit: '',
    quizCategoryID: 0
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'answers'
  });
  const [toggle, setToggle] = useState(false);
  const [question, setQuestion] = useState();

  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    reset();
    findWithQuizID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setData(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
    getAllQuizCategory(token, page, 10).then(result => {
      if (result.success) {
        setQuizCategories(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const handleDelete = async (data: any) => {
    const result = await deleteQuestionByID(token, data.id);
    if (result.success) {
      toast.success('Delete question successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete question!');
    }
  };

  const handleDeleteQuiz = async (data: any) => {
    const result = await deleteQuizByID(token, parseInt(id!));
    if (result.success) {
      toast.success('Delete quiz successful!');
      navigate('/quiz-category');
    } else {
      toast.error('Failed to delete question!');
    }
  };

  const handleUpdateChange = (event: any) => {
    setUpdateData({ ...updateData, [event.target.name]: event.target.value });
  };
  const handleClickUpdate = async (data: any) => {
    setToggle(true);
    setQuestion(data);
  };

  const handleClickUpdateQuiz = async (data: any) => {
    setUpdateData({
      timeLimit: data.timeLimit,
      name: data.name,
      quizCategoryID: parseInt(data.quizCategoryID)
    });
    setToggleUpdate(true);
  };

  const handleUpdate = async (e: any) => {
    e.preventDefault();
    const result = await updateQuizByID(token, parseInt(id!), {
      name: updateData.name,
      timeLimit: parseInt(updateData.timeLimit),
      quizCategoryID: updateData.quizCategoryID
    });
    if (result) {
      toast.success('Update quiz successful!');
      setToggleUpdate(false);
      setReload(reload + 1);
    } else {
      toast.error('Failed to update quiz!');
    }
  };

  const onSubmit = async (data: any) => {
    const newQuestion = { ...data, quizID: parseInt(id!) };
    const result = await createNewQuestion(token, newQuestion);
    if (result.success) {
      toast.success('Create new question successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to create new question!');
    }
  };

  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>BK STUDY -- TRẮC NGHIỆM</span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>Trắc Nghiệm Online</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Các bạn có thể vào đây để nêu ra các khó khăn của bạn. Mọi người sẽ giúp đỡ bạn hoàn thành nó.
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Tạo các bài quiz mới</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full gap-3 xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto'>
        <div className='my-4 p-4 border border-solid bg-white rounded-xl' style={shadow}>
          <span className='block p-2 w-full'>{data.name}</span>
          <div className='flex justify-between'>
            <div className='p-2'>
              {(quizCategories.filter((quizCategory: any) => quizCategory.id === data.quizCategoryID)[0] as any)?.name}
            </div>
            <div className='flex items-end p-2 gap-3'>
              <i className='bi bi-pencil-square cursor-pointer text-blue-600' onClick={() => handleClickUpdateQuiz(data)}></i>
              <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDeleteQuiz(data)}></i>
            </div>
          </div>
        </div>
        {data.questions.map((record, index) => {
          return <UserQuestionCard data={record} key={index} handleDelete={handleDelete} handleClickUpdate={handleClickUpdate} />;
        })}
        <div className=' p-4 border border-solid bg-white rounded-xl' style={shadow}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <textarea
              {...register('content')}
              placeholder='Please input your question'
              className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            />
            <div className='my-2 border-black border-t p-2'>
              {fields.map(({ id }, index) => {
                return (
                  <div key={id} className='flex w-full gap-2 mb-2'>
                    <input
                      {...register(`answers.${index}.content`)}
                      className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                      placeholder='Please input your answer'
                    />
                    <select
                      {...register(`answers.${index}.isCorrect`)}
                      className='hover:cursor-pointer form-select appearance-none block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none'
                    >
                      <option value={0}>False</option>
                      <option value={1}>True</option>
                    </select>

                    <button
                      type='button'
                      className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                      onClick={() => remove(index)}
                    >
                      Remove
                    </button>
                  </div>
                );
              })}
              <button
                type='button'
                className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                onClick={() => append({})}
              >
                Add answer
              </button>
            </div>
            <input
              type='submit'
              className='block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded hover:cursor-pointer'
            />
          </form>
        </div>
      </div>
      {toggle && <UserUpdateQuestion question={question} setToggle={setToggle} setReload={() => setReload(reload + 1)} r />}
      {toggleUpdate && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggleUpdate(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Quiz name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  name='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  value={updateData.name}
                  placeholder='Please type your quiz name '
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='timeLimit' className='text-sm font-medium text-gray-600'>
                Time limit (minutes)
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='timeLimit'
                  name='timeLimit'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='number'
                  value={updateData.timeLimit}
                  placeholder='Time limit (minutes)'
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.timeLimit?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.timeLimit?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='quizCategoryID' className='text-sm font-medium text-gray-600'>
                Quiz category
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl'>
                <select
                  id='quizCategoryID'
                  value={updateData.quizCategoryID}
                  name='quizCategoryID'
                  className=' pl-2 w-full outline-none border-none hover:cursor-pointer text-base text-[#757575] bg-white'
                  onChange={e => handleUpdateChange(e)}
                >
                  {quizCategories.map((quizCategory: any, index) => (
                    <option value={quizCategory.id} className='bg-white w-full' key={index}>
                      {quizCategory.name}
                    </option>
                  ))}
                </select>
              </div>
              {errors?.documentCategoryID?.type === 'required' && <p>⚠ This field is required!</p>}
            </div>
            <div className='flex justify-center'>
              <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium' onClick={e => handleUpdate(e)}>
                Update
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
