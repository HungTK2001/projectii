import moment from 'moment';
import { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getDocumentCategoryByID } from '../../../api/user/document/request';
import { AuthContext } from '../../../context/AuthContext';
import DocumentDetails from '../../molecules/DocumentDetails';

export default function DocumentDetailPage() {
  const { id } = useParams();
  const [documents, setDocuments] = useState([]);
  const [page, setPage] = useState(1);
  const [reload, setReload] = useState(0);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    getDocumentCategoryByID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setDocuments(result.data.document);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);
  return (
    <div className='w-full'>
      <div className='mb-64 md:mb-0 w-full z-0 bg-banner_img xl:bg-left-bottom bg-bottom '>
        <div className='w-full xl:max-w-[1199px] lg:max-w-[960px] md:max-w-[720px] sm:max-w-[540px] px-[15px] mx-auto xl:h-[850px] h-[750px]'>
          <div className='grid grid-cols-1  md:grid-cols-2 pt-[120px] md:pt-[220px]'>
            <div className='col-span-1 p-4 font-ssp'>
              <span className='block text-[10px] md:text-[18px] font-[600] text-[#ffa808] mb-[12px]'>
                BK STUDY -- TÀI LIỆU -- DANH SÁCH
              </span>
              <span className='block text-[3.375rem] leading-[1.2] font-bold mb-[20px] text-white'>Tài Liệu Học Tập Môn Giải Tích III</span>
              <span className='block text-[20px] leading-[1.6] mb-[20px] md:mb-[30px] font-[400] text-white'>
                Tổng hợp tài liệu, đề thi và đáp án các đề thi môn học giải tích 3 của các năm học gần đây được tổng hợp từ các nguồn tài
                liệu ở trong và ngoài trường đại học bách khoa Hà Nội.
              </span>
            </div>
            <img src='/assets/images/banner_img.png' alt='' className='col-span-1 animate-pulse' />
          </div>
        </div>
      </div>
      <div className='mb-8'>
        <span className='block text-[#1f2471] p-3 font-bold text-[45px] text-center mb-2 leading-[1.2]'>Danh Sách Tài Liệu</span>
        <div className='flex gap-[10px] w-full justify-center'>
          <div className='w-[45px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
          <div className='w-[95px] h-[6px] bg-gradient-to-r from-[#ef146e] to-[#fea958] rounded-[15px]'></div>
        </div>
      </div>
      <div className='w-full 2xl:max-w-[1500px] xl:max-w-[1200px] lg:max-w-[1000px] md:max-w-[730px] sm:max-w-[600px] px-[15px] mx-auto'>
        {documents.map((document: any, key) => (
          <DocumentDetails
            key={key}
            name={document.name}
            title={document.title}
            googleDriveLink={document.googleDriveLink}
            updateAt={moment(parseInt(document.updateAt)).format('LL')}
          />
        ))}
      </div>
    </div>
  );
}
