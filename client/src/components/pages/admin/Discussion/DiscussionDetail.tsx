import moment from 'moment';
import { useContext, useEffect, useState } from 'react';
import { useForm, useFieldArray, useWatch } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { deleteCommentByID, deleteDiscussionByID, getDiscussionByID } from '../../../../api/admin/discussion/request';
import { createNewQuestion, deleteQuestionByID, findWithQuizID } from '../../../../api/admin/quizzes/request';
import { AuthContext } from '../../../../context/AuthContext';
import QuestionCard from '../../../molecules/QuestionCard';
import WelcomeBanner from '../../../organisms/admin/WelcomeBanner';
export default function DiscussionDetail() {
  const [reload, setReload] = useState(0);
  const [data, setData] = useState({ createAt: '', updateAt: '', views: 0, comment: [], content: '' });
  const { token } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [toggle, setToggle] = useState(false);
  const [question, setQuestion] = useState();

  useEffect(() => {
    getDiscussionByID(token, parseInt(id!)).then(result => {
      if (result.success) {
        console.log(result.data);
        setData(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const handleDeleteDiscussion = async (data: any) => {
    const result = await deleteDiscussionByID(token, data.id);
    if (result.success) {
      toast.success('Delete discussion successful!');
      navigate('/admin/discussion');
    } else {
      toast.error('Failed to delete question!');
    }
  };
  const handleDeleteComment = async (data: any) => {
    const result = await deleteCommentByID(token, data.id);
    if (result.success) {
      toast.success('Delete comment successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete comment!');
    }
  };

  return (
    <div className='w-full'>
      <WelcomeBanner pageTitle='Quizzes manager' />
      <div className='w-full '>
        <div className='flex justify-between pr-3 border-b border-gray-200'>
          <div className=' py-2 px-5 w-full'>
            <span className='block text-4xl font-normal leading-[1.35]'>How to kill a process on a port on ubuntu</span>
            <span className='block'>
              Asked in {moment(parseInt(data.createAt)).format('LL')} Modified at {moment(parseInt(data.updateAt)).format('LL')} View{' '}
              {data.views}
            </span>
          </div>
          <div className='flex items-end p-2 gap-3'>
            <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDeleteDiscussion(data)}></i>
          </div>
        </div>
        <div className='px-5 py-5 border-b border-gray-200'>
          <span className='block text-2xl mb-2'>Content</span>
          <span className='block text-sm font-normal leading-[1.35] px-5'>{data.content}</span>
        </div>
        <div className='px-5'>
          <span className='block text-2xl '>Comment</span>
          {data.comment.map((record: any, index) => (
            <div className='flex justify-between pr-3 border-b border-gray-200' key={index}>
              <div className='px-5 py-5'>
                <span className='block text-sm font-normal leading-[1.35] mb-2'>{record.content}</span>
                <span className='block text-xs text-gray-600'>
                  Asked in {moment(parseInt(record.createAt)).format('LL')} Modified at {moment(parseInt(record.updateAt)).format('LL')}
                </span>
              </div>
              <div className='flex items-end p-2 gap-3'>
                <i className='bi bi-trash-fill cursor-pointer text-red-500' onClick={() => handleDeleteComment(record)}></i>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
