import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { deleteDiscussionByID, getAllDiscussion } from '../../../../api/admin/discussion/request';
import { AuthContext } from '../../../../context/AuthContext';
import Table from '../../../molecules/Table';
import WelcomeBanner from '../../../organisms/admin/WelcomeBanner';

export default function Discussion() {
  const DISCUSSION_HEADER = ['ID', 'Title', 'View', 'Edit'];
  const keys = ['title', 'views'];
  const [discussionDetails, setDiscussionDetails] = useState([]);
  const [updateData, setUpdateData] = useState({
    title: '',
    views: 0
  });

  const [uploading, setUploading] = useState(false);
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [updateID, setUpdateID] = useState(0);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [toggle, setToggle] = useState(false);
  const [reload, setReload] = useState(0);
  const [hasNextPage, setHasNextPage] = useState(false);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({});

  useEffect(() => {
    getAllDiscussion(token, page, 10).then(result => {
      if (result.success) {
        setDiscussionDetails(result.data.data);
        setTotal(result.data.total);
        setHasNextPage(result.data.hasNextPage);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload, page]);

  const handleUpdateChange = (event: any) => {
    setUpdateData({ ...updateData, [event.target.name]: event.target.value });
  };

  const handleCreate = async (data: any) => {
    setUploading(true);
    setUploading(false);
  };

  const handleUpdate = async (e: any) => {
    e.preventDefault();
    setUploading(true);
    navigate(`/admin/discussions/${updateID}`);
    setUploading(false);
  };

  const handleDelete = async (data: any) => {
    const result = await deleteDiscussionByID(token, data.id);
    if (result.success) {
      toast.success('Delete discussion successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete discussion!');
    }
  };

  const handleToggleUpdate = async (data: any) => {
    setUpdateData(data);
    setToggleUpdate(true);
    setUpdateID(data.id);
  };

  return (
    <div>
      <WelcomeBanner pageTitle='Discussion manager' />
      <div className='flex bg-gray-50 justify-between border-solid border-b-0 border p-2 mx-2'>
        <button className='filled-btn-success items-center' onClick={() => navigate('/admin/discussion-category')}>
          + Category
        </button>
      </div>
      <Table
        headers={DISCUSSION_HEADER}
        records={discussionDetails}
        keys={keys}
        handleDelete={handleDelete}
        handleToggleUpdate={handleToggleUpdate}
      />
      ;
      <div className='sm:flex-1 sm:flex sm:items-center bg-gray-50 border-solid border sm:justify-between p-2 mxA-2 work-sans'>
        <div>
          <p className='text-sm  leading-5 text-blue-700'>
            Showing
            <span className='font-medium'> {(page - 1) * 10 + 1} </span>
            to
            <span className='font-medium'> {(page - 1) * 10 + discussionDetails.length} </span>
            of
            <span className='font-medium'> {total} </span>
            results
          </p>
        </div>
        <div>
          <nav className='relative z-0 inline-flex shadow-sm'>
            <div onClick={() => setPage(page > 1 ? page - 1 : 1)}>
              <div
                className='relative inline-flex items-center px-2 py-1  border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
                aria-label='Previous'
              >
                <svg className='h-5 w-5' viewBox='0 0 20 20' fill='currentColor'>
                  <path
                    fillRule='evenodd'
                    d='M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z'
                    clipRule='evenodd'
                  />
                </svg>
              </div>
            </div>
            <div>
              <div className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-700 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'>
                {page}
              </div>
              <div className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-600 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'>
                {page + 1}
              </div>
              <div className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-600 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'>
                {page + 2}
              </div>
            </div>
            <div onClick={() => setPage(hasNextPage ? page + 1 : page)}>
              <div
                className='-ml-px relative inline-flex items-center px-2 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
                aria-label='Next'
              >
                <svg className='h-5 w-5' viewBox='0 0 20 20' fill='currentColor'>
                  <path
                    fillRule='evenodd'
                    d='M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z'
                    clipRule='evenodd'
                  />
                </svg>
              </div>
            </div>
          </nav>
        </div>
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleCreate)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggle(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Document name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your document name: '
                  {...register('name', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='title' className='text-sm font-medium text-gray-600'>
                Document title
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='title'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your document title: '
                  {...register('title', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.title?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.title?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='flex justify-center'>
              <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium' disabled={uploading}>
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
      {toggleUpdate && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggleUpdate(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Discussion title
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='title'
                  name='title'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your document name: '
                  value={updateData.title}
                  onChange={e => handleUpdateChange(e)}
                  disabled
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='title' className='text-sm font-medium text-gray-600'>
                Num of views
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='views'
                  name='views'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  value={updateData.views}
                  onChange={e => handleUpdateChange(e)}
                  disabled
                />
              </div>
              {errors?.title?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.title?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='flex justify-center'>
              <button onClick={e => handleUpdate(e)} type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium'>
                View comment
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
