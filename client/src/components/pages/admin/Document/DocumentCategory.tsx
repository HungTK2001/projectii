import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  createNewDocumentCategory,
  deleteDocumentCategoryByID,
  getAllDocumentCategory,
  updateDocumentCategoryByID
} from '../../../../api/admin/document/request';
import { AuthContext } from '../../../../context/AuthContext';
import Table from '../../../molecules/Table';
import WelcomeBanner from '../../../organisms/admin/WelcomeBanner';
import { useForm } from 'react-hook-form';
export default function DocumentCategory() {
  const DOCUMENT_HEADER = ['ID', 'Name', 'Description', 'Edit'];
  const keys = ['name', 'detail'];

  const [documentDetails, setDocumentDetails] = useState([]);
  const [updateData, setUpdateData] = useState({
    name: '',
    detail: '',
    moreDetail: ''
  });

  const [uploading, setUploading] = useState(false);
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [updateID, setUpdateID] = useState(0);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [toggle, setToggle] = useState(false);
  const [reload, setReload] = useState(0);
  const [hasNextPage, setHasNextPage] = useState(false);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm();

  useEffect(() => {
    getAllDocumentCategory(token, page, 10).then(result => {
      if (result.success) {
        setDocumentDetails(result.data.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const handleSubmitFile = async (data: any) => {
    setUploading(true);
    const result = await createNewDocumentCategory(token, data);
    if (result.success) {
      toast.success('Create new document category successful!');
      setToggle(false);
      setReload(reload + 1);
    } else {
      toast.error('Failed to create new document category');
    }
    setUploading(false);
  };

  const handleUpdateChange = (event: any) => {
    setUpdateData({ ...updateData, [event.target.name]: event.target.value });
  };

  const handleDelete = async (data: any) => {
    const result = await deleteDocumentCategoryByID(token, data.id);
    if (result.success) {
      toast.success('Delete document category successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete document category !');
    }
  };

  const handleToggleUpdate = async (data: any) => {
    const { name, detail, moreDetail } = data;
    setUpdateData({ name, detail, moreDetail });
    setToggleUpdate(true);
    setUpdateID(data.id);
  };

  const handleUpdate = async (e: any) => {
    e.preventDefault();
    setUploading(true);
    const result = await updateDocumentCategoryByID(token, updateID, updateData);
    if (result.success) {
      toast.success('Update document category successful!');
      setReload(reload + 1);
      setToggleUpdate(false);
    } else {
      toast.error('Name of document category is duplicate');
    }
    setUploading(false);
  };

  return (
    <div>
      <div>
        <WelcomeBanner pageTitle='Document category manager' />
        <div className='flex bg-gray-50 justify-end border-solid border-b-0 border p-2'>
          <button className='filled-btn-primary items-center' onClick={() => setToggle(true)}>
            Create new
          </button>
        </div>
        <Table
          headers={DOCUMENT_HEADER}
          records={documentDetails}
          keys={keys}
          handleDelete={handleDelete}
          handleToggleUpdate={handleToggleUpdate}
        />
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleSubmitFile)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggle(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Category name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Document category name '
                  {...register('name', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='detail' className='text-sm font-medium text-gray-600'>
                Category details
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='detail'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Document category details '
                  {...register('detail', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.detail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.detail?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='detail' className='text-sm font-medium text-gray-600'>
                Document detail
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <textarea
                  id='moreDetail'
                  className=' pl-2 w-full outline-none border-none text-base h-60 text-[#757575]'
                  placeholder='More details'
                  {...register('moreDetail', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.moreDetail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.moreDetail?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='flex justify-center'>
              <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium' disabled={uploading}>
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
      {toggleUpdate && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleSubmitFile)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggleUpdate(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Category name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  name='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Document category name '
                  value={updateData.name}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='detail' className='text-sm font-medium text-gray-600'>
                Category detail
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='detail'
                  name='detail'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Document category detail '
                  value={updateData.detail}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.detail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.detail?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='moreDetail' className='text-sm font-medium text-gray-600'>
                More detail
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <textarea
                  id='moreDetail'
                  name='moreDetail'
                  className=' pl-2 w-full outline-none border-none text-base h-60 text-[#757575]'
                  placeholder='More details'
                  value={updateData.moreDetail}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.moreDetail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.moreDetail?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='flex justify-center'>
              <button
                type='submit'
                className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium'
                disabled={uploading}
                onClick={e => handleUpdate(e)}
              >
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
