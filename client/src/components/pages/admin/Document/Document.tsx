import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  createNewDocument,
  deleteDocumentByID,
  getAllDocument,
  getAllDocumentCategory,
  updateDocumentByID,
  uploadDocument
} from '../../../../api/admin/document/request';
import { AuthContext } from '../../../../context/AuthContext';
import Table from '../../../molecules/Table';
import WelcomeBanner from '../../../organisms/admin/WelcomeBanner';
import { useForm } from 'react-hook-form';
export default function Document() {
  const DOCUMENT_HEADER = ['ID', 'Name', 'Drive link', 'View', 'Edit'];
  const keys = ['name', 'googleDriveLink', 'views'];
  const [documentDetails, setDocumentDetails] = useState([]);
  const [selectedFile, setSelectedFile] = useState<any>();
  const [updateFile, setUpdateFile] = useState<any>();
  const [documentCategories, setDocumentCategories] = useState([]);
  const [updateData, setUpdateData] = useState({
    name: '',
    googleDriveLink: '',
    title: '',
    documentCategoryID: '',
    fileID: ''
  });

  const [uploading, setUploading] = useState(false);
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [updateID, setUpdateID] = useState(0);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [toggle, setToggle] = useState(false);
  const [reload, setReload] = useState(0);
  const [hasNextPage, setHasNextPage] = useState(false);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({});

  useEffect(() => {
    getAllDocument(token, page, 10).then(result => {
      if (result.success) {
        setDocumentDetails(result.data.data);
        setHasNextPage(result.data.hasNextPage);
        setTotal(result.data.total);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
    getAllDocumentCategory(token, page, 10).then(result => {
      if (result.success) {
        setDocumentCategories(result.data.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload, page]);

  const onChangeFile = (event: any) => {
    setSelectedFile(event.target.files[0]);
  };

  const onChangeUpdateFile = (event: any) => {
    setUpdateFile(event.target.files[0]);
  };

  const handleUpdateChange = (event: any) => {
    setUpdateData({ ...updateData, [event.target.name]: event.target.value });
  };

  const handleSubmitFile = async (data: any) => {
    setUploading(true);
    if (selectedFile) {
      const formData = new FormData();
      formData.append('file', selectedFile);
      const result = await uploadDocument(token, formData);
      if (result.success) {
        console.log(result.data);
        const newDocument = {
          ...data,
          documentCategoryID: parseInt(data.documentCategoryID),
          googleDriveLink: result.data.webViewLink,
          fileID: result.data.fileID
        };
        const createNew = await createNewDocument(token, newDocument);
        if (createNew.success) {
          toast.success('Create new document successful!');
          setToggle(false);
          setSelectedFile(undefined);
          setReload(reload + 1);
        } else {
          toast.error('Failed to create new document');
        }
      } else {
        toast.error('Failed to upload file!');
      }
    } else {
      toast.error('Please select file to upload');
    }
    setUploading(false);
  };

  const handleUpdateFile = async (e: any) => {
    e.preventDefault();
    setUploading(true);
    if (updateFile) {
      const formData = new FormData();
      formData.append('file', updateFile);
      const result = await uploadDocument(token, formData);
      if (result.success) {
        const updateDocument = {
          ...updateData,
          documentCategoryID: parseInt(updateData.documentCategoryID),
          googleDriveLink: result.data.webViewLink,
          fileID: result.data.fileID
        };
        const updateResult = await updateDocumentByID(token, updateDocument, updateID);
        if (updateResult.success) {
          toast.success('Update document successful!');
          setToggleUpdate(false);
          setReload(reload + 1);
        } else {
          toast.error('Failed to update document');
        }
      } else {
        toast.error('Failed to upload file!');
      }
    } else {
      const updateDocument = { ...updateData, documentCategoryID: parseInt(updateData.documentCategoryID) };
      const updateResult = await updateDocumentByID(token, updateDocument, updateID);
      if (updateResult.success) {
        toast.success('Update document successful!');
        setToggleUpdate(false);
        setReload(reload + 1);
      } else {
        toast.error('Failed to update document!');
      }
    }
    setUploading(false);
  };

  const handleDelete = async (data: any) => {
    const result = await deleteDocumentByID(token, data.id, data.fileID);
    if (result.success) {
      toast.success('Delete document successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete document!');
    }
  };

  const handleToggleUpdate = async (data: any) => {
    setUpdateData(data);
    setToggleUpdate(true);
    setUpdateID(data.id);
  };

  return (
    <div>
      <div>
        <WelcomeBanner pageTitle='Document manager' />
        <div className='flex bg-gray-50 justify-between border-solid border-b-0 border p-2 mx-2'>
          <button className='filled-btn-success items-center' onClick={() => navigate('/admin/document-category')}>
            + Category
          </button>
          <button className='filled-btn-primary items-center' onClick={() => setToggle(true)}>
            New +
          </button>
        </div>
        <Table
          headers={DOCUMENT_HEADER}
          records={documentDetails}
          keys={keys}
          handleDelete={handleDelete}
          handleToggleUpdate={handleToggleUpdate}
        />
        <div className='sm:flex-1 sm:flex sm:items-center bg-gray-50 border-solid border sm:justify-between p-2 mxA-2 work-sans'>
          <div>
            <p className='text-sm  leading-5 text-blue-700'>
              Showing
              <span className='font-medium'> {(page - 1) * 10 + 1} </span>
              to
              <span className='font-medium'> {(page - 1) * 10 + documentDetails.length} </span>
              of
              <span className='font-medium'> {total} </span>
              results
            </p>
          </div>
          <div>
            <nav className='relative z-0 inline-flex shadow-sm'>
              <div onClick={() => setPage(page > 1 ? page - 1 : 1)}>
                <div
                  className='relative inline-flex items-center px-2 py-1  border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
                  aria-label='Previous'
                >
                  <svg className='h-5 w-5' viewBox='0 0 20 20' fill='currentColor'>
                    <path
                      fillRule='evenodd'
                      d='M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z'
                      clipRule='evenodd'
                    />
                  </svg>
                </div>
              </div>
              <div>
                <div className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-700 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'>
                  {page}
                </div>
                <div className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-600 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'>
                  {page + 1}
                </div>
                <div className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-600 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'>
                  {page + 2}
                </div>
              </div>
              <div onClick={() => setPage(hasNextPage ? page + 1 : page)}>
                <div
                  className='-ml-px relative inline-flex items-center px-2 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
                  aria-label='Next'
                >
                  <svg className='h-5 w-5' viewBox='0 0 20 20' fill='currentColor'>
                    <path
                      fillRule='evenodd'
                      d='M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z'
                      clipRule='evenodd'
                    />
                  </svg>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleSubmitFile)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggle(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Document name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your document name: '
                  {...register('name', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='title' className='text-sm font-medium text-gray-600'>
                Document description
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <textarea
                  id='title'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  placeholder='Please type document description '
                  {...register('title', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.title?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.title?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='documentCategoryID' className='text-sm font-medium text-gray-600'>
                Document category
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl'>
                <select
                  id='documentCategoryID'
                  className=' pl-2 w-full outline-none border-none hover:cursor-pointer text-base text-[#757575] bg-white'
                  {...register('documentCategoryID', { required: true, minLength: 1 })}
                >
                  {documentCategories.map((documentCategory: any, index) => (
                    <option value={documentCategory.id} className='bg-white w-full' key={index}>
                      {documentCategory.name}
                    </option>
                  ))}
                </select>
              </div>
              {errors?.documentCategoryID?.type === 'required' && <p>⚠ This field is required!</p>}
            </div>
            <div className='flex justify-center'>
              <div className='mb-3 w-96'>
                <label htmlFor='formFile' className='form-label inline-block mb-2 text-gray-700'>
                  Choose your document
                </label>
                <input
                  className='form-control hover:cursor-pointer block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none'
                  type='file'
                  id='formFile'
                  onChange={onChangeFile}
                />
              </div>
            </div>
            <div className='flex justify-center'>
              <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium' disabled={uploading}>
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
      {toggleUpdate && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggleUpdate(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Document name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  name='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your document name: '
                  value={updateData.name}
                  onChange={e => handleUpdateChange(e)}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='title' className='text-sm font-medium text-gray-600'>
                Document description
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <textarea
                  id='title'
                  name='title'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  value={updateData.title}
                  placeholder='Please type your document title: '
                  onChange={e => handleUpdateChange(e)}
                />
              </div>
              {errors?.title?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.title?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='documentCategoryID' className='text-sm font-medium text-gray-600'>
                Document category
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl'>
                <select
                  id='documentCategoryID'
                  value={updateData.documentCategoryID}
                  name='documentCategoryID'
                  className=' pl-2 w-full outline-none border-none hover:cursor-pointer text-base text-[#757575] bg-white'
                  onChange={e => handleUpdateChange(e)}
                >
                  {documentCategories.map((documentCategory: any, index) => (
                    <option value={documentCategory.id} className='bg-white w-full' key={index}>
                      {documentCategory.name}
                    </option>
                  ))}
                </select>
              </div>
              {errors?.documentCategoryID?.type === 'required' && <p>⚠ This field is required!</p>}
            </div>
            <div className='flex justify-center'>
              <div className='mb-3 w-96'>
                <label htmlFor='formFile' className='form-label inline-block mb-2 text-gray-700'>
                  Choose your document
                </label>
                <input
                  className='form-control hover:cursor-pointer block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none'
                  type='file'
                  id='formFile'
                  onChange={onChangeUpdateFile}
                />
              </div>
            </div>
            <div className='flex justify-center'>
              <button
                onClick={e => handleUpdateFile(e)}
                type='submit'
                className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium'
                disabled={uploading}
              >
                {uploading ? 'Processing' : 'Update'}
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
