import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { deleteUserByID, getAllUser, registerUser, updateUserByID } from '../../../api/admin/account/request';
import { AuthContext } from '../../../context/AuthContext';
import Table from '../../molecules/Table';
import WelcomeBanner from '../../organisms/admin/WelcomeBanner';

export default function User() {
  const USER_HEADER = ['ID', 'Username', 'Full name', 'Email', 'Edit'];
  const keys = ['username', 'fullName', 'email'];
  const [userDetails, setUserDetails] = useState([]);
  const [updateData, setUpdateData] = useState({
    fullName: '',
    email: '',
    roleID: '1'
  });

  const [uploading, setUploading] = useState(false);
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [updateID, setUpdateID] = useState(0);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [toggle, setToggle] = useState(false);
  const [reload, setReload] = useState(0);
  const [hasNextPage, setHasNextPage] = useState(false);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({});

  useEffect(() => {
    getAllUser(token, page, 10).then(result => {
      if (result.success) {
        setUserDetails(result.data.data);
        setHasNextPage(result.data.hasNextPage);
        setTotal(result.data.total);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload, page]);

  const handleUpdateChange = (event: any) => {
    setUpdateData({ ...updateData, [event.target.name]: event.target.value });
  };

  const handleCreate = async (data: any) => {
    setUploading(true);
    if (data.password !== data.repeatPassword) {
      toast.error('Password must compare with repeat password!');
    } else {
      const result = await registerUser(data);
      if (result.success) {
        toast.success('Create new user successful!');
        setReload(reload + 1);
        setToggle(false);
      } else {
        toast.error('Failed to create new user!');
      }
    }
    setUploading(false);
  };

  const handleUpdate = async (e: any) => {
    e.preventDefault();
    setUploading(true);
    const result = await updateUserByID(token, updateID, { ...updateData, roleID: parseInt(updateData.roleID) });
    if (result.success) {
      toast.success('Update user successful!');
      setReload(reload + 1);
      setToggleUpdate(false);
    } else {
      toast.error('Failed to update user!');
    }
    setUploading(false);
  };

  const handleDelete = async (data: any) => {
    const result = await deleteUserByID(token, data.id);
    if (result.success) {
      toast.success('Delete user successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete user!');
    }
  };

  const handleToggleUpdate = async (data: any) => {
    const { email, fullName, roleID } = data;
    setUpdateData({ email, fullName, roleID });
    setToggleUpdate(true);
    setUpdateID(data.id);
  };

  return (
    <div>
      <div>
        <WelcomeBanner pageTitle='User manager' />
        <div className='flex bg-gray-50 justify-end border-solid border-b-0 border p-2 mx-2'>
          <button className='filled-btn-primary items-center' onClick={() => setToggle(true)}>
            Create new
          </button>
        </div>
        <Table
          headers={USER_HEADER}
          records={userDetails}
          keys={keys}
          handleDelete={handleDelete}
          handleToggleUpdate={handleToggleUpdate}
        />
        <div className='sm:flex-1 sm:flex sm:items-center bg-gray-50 border-solid border sm:justify-between p-2 mxA-2 work-sans'>
          <div>
            <p className='text-sm  leading-5 text-blue-700'>
              Showing
              <span className='font-medium'> {(page - 1) * 10 + 1} </span>
              to
              <span className='font-medium'> {(page - 1) * 10 + userDetails.length} </span>
              of
              <span className='font-medium'> {total} </span>
              results
            </p>
          </div>
          <div>
            <nav className='relative z-0 inline-flex shadow-sm'>
              <div onClick={() => setPage(page > 1 ? page - 1 : 1)}>
                <a
                  href='#'
                  className='relative inline-flex items-center px-2 py-1  border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
                  aria-label='Previous'
                >
                  <svg className='h-5 w-5' viewBox='0 0 20 20' fill='currentColor'>
                    <path
                      fillRule='evenodd'
                      d='M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z'
                      clipRule='evenodd'
                    />
                  </svg>
                </a>
              </div>
              <div>
                <a
                  href='#'
                  className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-700 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'
                >
                  {page}
                </a>
                <a
                  href='#'
                  className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-600 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'
                >
                  {page + 1}
                </a>
                <a
                  href='#'
                  className='-ml-px relative inline-flex items-center px-4 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-blue-600 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-tertiary active:text-gray-700 transition ease-in-out duration-150 hover:bg-tertiary'
                >
                  {page + 2}
                </a>
              </div>
              <div onClick={() => setPage(hasNextPage ? page + 1 : page)}>
                <a
                  href='#'
                  className='-ml-px relative inline-flex items-center px-2 py-1 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
                  aria-label='Next'
                >
                  <svg className='h-5 w-5' viewBox='0 0 20 20' fill='currentColor'>
                    <path
                      fillRule='evenodd'
                      d='M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z'
                      clipRule='evenodd'
                    />
                  </svg>
                </a>
              </div>
            </nav>
          </div>
        </div>
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleCreate)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggle(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='username' className='text-sm font-medium text-gray-600'>
                Username
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='username'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your username: '
                  {...register('username', { required: true, minLength: 6 })}
                />
              </div>
              {errors?.username?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.username?.type === 'minLength' && <p>⚠ Name cannot be less than 6 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='fullName' className='text-sm font-medium text-gray-600'>
                Full name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='fullName'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your full name: '
                  {...register('fullName', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.fullName?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.fullName?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='email' className='text-sm font-medium text-gray-600'>
                Email
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='email'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your email: '
                  {...register('email', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.email?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.email?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='password' className='text-sm font-medium text-gray-600'>
                Password
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='password'
                  placeholder='******'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='password'
                  {...register('password', { required: true, minLength: 6 })}
                />
              </div>
              {errors?.password?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.password?.type === 'minLength' && <p>⚠ Title cannot be less than 6 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='repeatPassword' className='text-sm font-medium text-gray-600'>
                Repeat password
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='repeatPassword'
                  placeholder='******'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='password'
                  {...register('repeatPassword', { required: true, minLength: 6 })}
                />
              </div>
              {errors?.repeatPassword?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.repeatPassword?.type === 'minLength' && <p>⚠ Title cannot be less than 6 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='roleID' className='text-sm font-medium text-gray-600'>
                Role
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl'>
                <select
                  id='roleID'
                  className=' pl-2 w-full outline-none border-none hover:cursor-pointer text-base text-[#757575] bg-white'
                  {...register('roleID', { required: true, minLength: 1 })}
                >
                  <option value={1} className='bg-white w-full'>
                    Admin
                  </option>
                  <option value={2} className='bg-white w-full'>
                    User
                  </option>
                </select>
              </div>
              {errors?.roleID?.type === 'required' && <p>⚠ This field is required!</p>}
            </div>
            <div className='flex justify-center'>
              <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium' disabled={uploading}>
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
      {toggleUpdate && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggleUpdate(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='fullName' className='text-sm font-medium text-gray-600'>
                Full name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='fullName'
                  name='fullName'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your full name: '
                  value={updateData.fullName}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.fullName?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.fullName?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='email' className='text-sm font-medium text-gray-600'>
                Email
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 text-gray-400'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                  />
                </svg>
                <input
                  id='email'
                  name='email'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='Please type your email: '
                  value={updateData.email}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.email?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.email?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='roleID' className='text-sm font-medium text-gray-600'>
                Role
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl'>
                <select
                  id='roleID'
                  name='roleID'
                  className=' pl-2 w-full outline-none border-none hover:cursor-pointer text-base text-[#757575] bg-white'
                  value={updateData.roleID}
                  onChange={handleUpdateChange}
                >
                  <option value={1} className='bg-white w-full'>
                    Admin
                  </option>
                  <option value={2} className='bg-white w-full'>
                    User
                  </option>
                </select>
              </div>
              {errors?.roleID?.type === 'required' && <p>⚠ This field is required!</p>}
            </div>
            <div className='flex justify-center'>
              <button
                type='submit'
                className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium'
                disabled={uploading}
                onClick={e => handleUpdate(e)}
              >
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
