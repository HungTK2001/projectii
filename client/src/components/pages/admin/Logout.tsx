import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getAllUser } from '../../../api/admin/account/request';
import { AuthContext } from '../../../context/AuthContext';
import Table from '../../molecules/Table';
import WelcomeBanner from '../../organisms/admin/WelcomeBanner';

export default function Logout() {
  const { clearAuthData } = useContext(AuthContext);
  const navigate = useNavigate();
  useEffect(() => {
    clearAuthData?.();
    toast.success('Logout successful!');
    navigate('/admin/login');
  });

  return <div>Logout</div>;
}
