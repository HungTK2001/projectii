import { useContext } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ChangePasswordByUserID } from '../../../api/admin/account/request';
import { AuthContext } from '../../../context/AuthContext';
import WelcomeBanner from '../../organisms/admin/WelcomeBanner';

export default function ChangePassword() {
  const { token } = useContext(AuthContext);

  const user = JSON.parse(localStorage.getItem('user')!);
  const navigate = useNavigate();
  const onSubmit = async (data: any) => {
    if (data.password != data.repeatPassword) {
      toast.error('Repeat password must compare with new password');
    } else {
      const result = await ChangePasswordByUserID(token, user.id, {
        oldPassword: data.oldPassword,
        password: data.password
      });
      if (result.success) {
        toast.success('Change password successful, please login again!');
        navigate('/admin/login');
      } else {
        toast.error(result.message);
      }
    }
  };
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({});
  return (
    <div className='w-full h-full'>
      <WelcomeBanner pageTitle='Change password page' />
      <div className='bg-white mx-auto my-auto shadow-md border  border-gray-500 rounded-lg max-w-sm p-4 sm:p-6 lg:p-8 dark:bg-gray-800 dark:border-gray-700'>
        <form className='space-y-6' onSubmit={handleSubmit(onSubmit)}>
          <h3 className='text-xl font-medium text-gray-900 dark:text-white'>Change password</h3>
          <div>
            <label htmlFor='oldPassword' className='text-sm font-medium text-gray-900 block mb-2 dark:text-gray-300'>
              Old password
            </label>
            <input
              type='password'
              {...register('oldPassword', { required: true, minLength: 6 })}
              id='oldPassword'
              className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white'
              placeholder='******'
            />
            {errors?.oldPassword?.type === 'required' && <p>⚠ This field is required!</p>}
            {errors?.oldPassword?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
          </div>
          <div>
            <label htmlFor='password' className='text-sm font-medium text-gray-900 block mb-2 dark:text-gray-300'>
              New password
            </label>
            <input
              type='password'
              {...register('password', { required: true, minLength: 6 })}
              id='password'
              placeholder='*******'
              className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white'
            />
            {errors?.password?.type === 'required' && <p>⚠ This field is required!</p>}
            {errors?.password?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
          </div>
          <div>
            <label htmlFor='repeatPassword' className='text-sm font-medium text-gray-900 block mb-2 dark:text-gray-300'>
              Repeat password
            </label>
            <input
              type='password'
              {...register('repeatPassword', { required: true, minLength: 6 })}
              id='repeatPassword'
              placeholder='*******'
              className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white'
            />
            {errors?.repeatPassword?.type === 'required' && <p>⚠ This field is required!</p>}
            {errors?.repeatPassword?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
          </div>
          <button
            type='submit'
            className='w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'
          >
            Change password
          </button>
        </form>
      </div>
    </div>
  );
}
