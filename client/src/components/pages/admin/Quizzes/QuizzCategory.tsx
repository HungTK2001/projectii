import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AuthContext } from '../../../../context/AuthContext';
import Table from '../../../molecules/Table';
import WelcomeBanner from '../../../organisms/admin/WelcomeBanner';
import { useForm } from 'react-hook-form';
import {
  createNewQuizCategory,
  deleteQuizCategoryByID,
  getAllQuizCategory,
  updateQuizCategoryByID
} from '../../../../api/admin/quizzes/request';
export default function QuizCategory() {
  const quiz_HEADER = ['ID', 'Name', 'Description', 'Edit'];
  const keys = ['name', 'detail'];

  const [quizDetails, setQuizDetails] = useState([]);
  const [updateData, setUpdateData] = useState({
    name: '',
    detail: '',
    moreDetail: ''
  });

  const [uploading, setUploading] = useState(false);
  const [toggleUpdate, setToggleUpdate] = useState(false);
  const [updateID, setUpdateID] = useState(0);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [toggle, setToggle] = useState(false);
  const [reload, setReload] = useState(0);
  const [hasNextPage, setHasNextPage] = useState(false);

  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm();

  useEffect(() => {
    getAllQuizCategory(token, page, 10).then(result => {
      if (result.success) {
        setQuizDetails(result.data.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const handleSubmitFile = async (data: any) => {
    setUploading(true);
    const result = await createNewQuizCategory(token, data);
    if (result.success) {
      toast.success('Create new quiz category successful!');
      setToggle(false);
      setReload(reload + 1);
    } else {
      toast.error('Failed to create new quiz category');
    }
    setUploading(false);
  };

  const handleUpdateChange = (event: any) => {
    setUpdateData({ ...updateData, [event.target.name]: event.target.value });
  };

  const handleDelete = async (data: any) => {
    const result = await deleteQuizCategoryByID(token, data.id);
    if (result.success) {
      toast.success('Delete quiz category successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete quiz category !');
    }
  };

  const handleToggleUpdate = async (data: any) => {
    const { name, detail, moreDetail } = data;
    setUpdateData({ name, detail, moreDetail });
    setToggleUpdate(true);
    setUpdateID(data.id);
  };

  const handleUpdate = async (e: any) => {
    e.preventDefault();
    setUploading(true);
    const result = await updateQuizCategoryByID(token, updateID, updateData);
    if (result.success) {
      toast.success('Update quiz category successful!');
      setReload(reload + 1);
      setToggleUpdate(false);
    } else {
      toast.error('Name of quiz category is duplicate');
    }
    setUploading(false);
  };

  return (
    <div>
      <div>
        <WelcomeBanner pageTitle='quiz category manager' />
        <div className='flex bg-gray-50 justify-end border-solid border-b-0 border p-2'>
          <button className='filled-btn-primary items-center' onClick={() => setToggle(true)}>
            Create new
          </button>
        </div>
        <Table
          headers={quiz_HEADER}
          records={quizDetails}
          keys={keys}
          handleDelete={handleDelete}
          handleToggleUpdate={handleToggleUpdate}
        />
      </div>
      {toggle && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleSubmitFile)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggle(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Category name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='quiz category name '
                  {...register('name', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='detail' className='text-sm font-medium text-gray-600'>
                Category details
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='detail'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='quiz category details '
                  {...register('detail', { required: true, minLength: 1 })}
                />
              </div>
              {errors?.detail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.detail?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='detail' className='text-sm font-medium text-gray-600'>
                quiz detail
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <textarea
                  id='moreDetail'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  placeholder='More details'
                  {...register('moreDetail', { required: true, minLength: 1 })}
                  rows={10}
                />
              </div>
              {errors?.moreDetail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.moreDetail?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='flex justify-center'>
              <button type='submit' className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium' disabled={uploading}>
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
      {toggleUpdate && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
          <form onSubmit={handleSubmit(handleSubmitFile)} className='rounded-lg bg-white p-10'>
            <div className='w-full flex justify-end'>
              <i className='bi bi-x-lg hover: cursor-pointer' onClick={() => setToggleUpdate(false)}></i>
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='name' className='text-sm font-medium text-gray-600'>
                Category name
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='name'
                  name='name'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='quiz category name '
                  value={updateData.name}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.name?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.name?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='detail' className='text-sm font-medium text-gray-600'>
                Category detail
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <input
                  id='detail'
                  name='detail'
                  className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                  type='text'
                  placeholder='quiz category detail '
                  value={updateData.detail}
                  onChange={handleUpdateChange}
                />
              </div>
              {errors?.detail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.detail?.type === 'minLength' && <p>⚠ Name cannot be less than 1 characters!</p>}
            </div>
            <div className='mb-5 text-red-500 text-xs'>
              <label htmlFor='moreDetail' className='text-sm font-medium text-gray-600'>
                More detail
              </label>
              <div className='flex items-center border-2 mb-1 py-2 px-3 rounded-2xl hover:cursor-pointer'>
                <textarea
                  id='moreDetail'
                  name='moreDetail'
                  className=' pl-2 w-full outline-none border-none text-base h-60 text-[#757575]'
                  placeholder='More details'
                  value={updateData.moreDetail}
                  onChange={handleUpdateChange}
                  rows={10}
                />
              </div>
              {errors?.moreDetail?.type === 'required' && <p>⚠ This field is required!</p>}
              {errors?.moreDetail?.type === 'minLength' && <p>⚠ Title cannot be less than 1 characters!</p>}
            </div>
            <div className='flex justify-center'>
              <button
                type='submit'
                className='bg-blue-500 py-1 px-2 rounded-md mt-2 text-white font-medium'
                disabled={uploading}
                onClick={e => handleUpdate(e)}
              >
                {uploading ? 'Creating' : 'Submit'}
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}
