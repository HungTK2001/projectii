import { useContext, useEffect, useState } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  createNewAnswer,
  deleteAnswerMultiple,
  getQuestionByID,
  updateAnswerByID,
  updateQuestionByID
} from '../../../../api/admin/quizzes/request';
import { AuthContext } from '../../../../context/AuthContext';

export default function UpdateQuestion(props: any) {
  const { token } = useContext(AuthContext);
  const navigate = useNavigate();
  const { question, setToggle, setReload } = props;
  const [reloadData, setReloadData] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [questionContent, setQuestionContent] = useState(question.content);
  const [answers, setAnswers] = useState(question.answers);
  useEffect(() => {
    getQuestionByID(token, question.id).then(result => {
      if (result.success) {
        reset(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reloadData]);
  const { register, control, handleSubmit, reset } = useForm({
    defaultValues: {
      content: questionContent,
      answers: answers
    }
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'answers'
  });

  const handleDeleteAnswer = async (data: any) => {
    if (data?.id) {
      const result = await deleteAnswerMultiple(token, [data.id]);
      if (result.success) {
        toast.success('Delete answer successful!');
        setReloadData(reloadData + 1);
      } else {
        toast.error('Failed to delete!');
      }
    }
  };

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    const { id } = question;
    const { answers } = data;
    const updateQuestion = await updateQuestionByID(token, id, { content: data.content });
    if (updateQuestion.success) {
      for (let i = 0; i < answers.length; i++) {
        if (answers[i]?.id) {
          await updateAnswerByID(token, answers[i]?.id, {
            content: answers[i].content,
            isCorrect: answers[i].isCorrect
          });
        } else {
          await createNewAnswer(token, { ...answers[i], questionID: id });
        }
      }
      toast.success('Update question successful!');
      setReloadData(reloadData + 1);
      setReload();
      setToggle(false);
    } else {
      toast.error('Update question failed!');
    }
    setIsLoading(false);
  };

  return (
    <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center'>
      <form onSubmit={handleSubmit(onSubmit)} className='rounded-lg bg-white p-10'>
        <div className='w-full flex justify-end'>
          <i
            className='bi bi-x-lg hover: cursor-pointer'
            onClick={() => {
              setToggle(false);
              setReload();
            }}
          ></i>
        </div>
        <textarea
          {...register('content')}
          placeholder='Please input your question'
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
        />
        <div className='my-2 border-black border-t p-2'>
          {fields.map(({ id }, index) => {
            return (
              <div key={id} className='flex w-full gap-2 mb-2'>
                <input
                  {...register(`answers.${index}.content`)}
                  className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                  placeholder='Please input your answer'
                />
                <select
                  {...register(`answers.${index}.isCorrect`)}
                  className='form-select appearance-none block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none'
                >
                  <option value={0}>False</option>
                  <option value={1}>True</option>
                </select>
                <button
                  type='button'
                  className='bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded'
                  onClick={() => {
                    handleDeleteAnswer(answers[index]);
                    remove(index);
                  }}
                >
                  Remove
                </button>
              </div>
            );
          })}
          <button type='button' className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded' onClick={() => append({})}>
            Add answer
          </button>
        </div>
        <input
          type='submit'
          className='block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded hover:cursor-pointer'
          disabled={isLoading}
        />
      </form>
    </div>
  );
}
