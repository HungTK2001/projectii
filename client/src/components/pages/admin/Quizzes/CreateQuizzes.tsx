import { useContext, useEffect, useState } from 'react';
import { useForm, useFieldArray, useWatch } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { createNewQuestion, deleteQuestionByID, findWithQuizID } from '../../../../api/admin/quizzes/request';
import { AuthContext } from '../../../../context/AuthContext';
import QuestionCard from '../../../molecules/QuestionCard';
import WelcomeBanner from '../../../organisms/admin/WelcomeBanner';
import UpdateQuestion from './UpdateQuestion';
export default function CreateQuizzes() {
  const { register, control, handleSubmit, reset } = useForm({
    defaultValues: {
      content: '',
      answers: [{ content: '', isCorrect: 0 }]
    }
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'answers'
  });

  const [reload, setReload] = useState(0);
  const [data, setData] = useState({
    questions: []
  });
  const { token } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();
  const [toggle, setToggle] = useState(false);
  const [question, setQuestion] = useState();

  useEffect(() => {
    reset();
    findWithQuizID(token, parseInt(id!)).then(result => {
      if (result.success) {
        setData(result.data);
      } else {
        if (result.message == 'Unauthorized') {
          toast.error('You are not login!');
          navigate('/admin/login');
        } else {
          toast.error(result.message);
        }
      }
    });
  }, [reload]);

  const handleDelete = async (data: any) => {
    const result = await deleteQuestionByID(token, data.id);
    if (result.success) {
      toast.success('Delete question successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to delete question!');
    }
  };

  const handleClickUpdate = async (data: any) => {
    setToggle(true);
    setQuestion(data);
  };

  const onSubmit = async (data: any) => {
    const newQuestion = { ...data, quizID: parseInt(id!) };
    const result = await createNewQuestion(token, newQuestion);
    if (result.success) {
      toast.success('Create new question successful!');
      setReload(reload + 1);
    } else {
      toast.error('Failed to create new question!');
    }
  };

  return (
    <div>
      <div className='w-full'>
        <WelcomeBanner pageTitle='Quizzes detail manage page' />
        <div className='grid justify-center gap-2'>
          {data.questions.map((record, index) => {
            return <QuestionCard data={record} key={index} handleDelete={handleDelete} handleClickUpdate={handleClickUpdate} />;
          })}
          <div className=' p-3 border border-solid border-gray-400 rounded-xl'>
            <form onSubmit={handleSubmit(onSubmit)}>
              <textarea
                {...register('content')}
                placeholder='Please input your question'
                className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
              />
              <div className='my-2 border-black border-t p-2'>
                {fields.map(({ id }, index) => {
                  return (
                    <div key={id} className='flex w-full gap-2 mb-2'>
                      <input
                        {...register(`answers.${index}.content`)}
                        className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
                        placeholder='Please input your answer'
                      />
                      <select
                        {...register(`answers.${index}.isCorrect`)}
                        className='hover:cursor-pointer form-select appearance-none block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none'
                      >
                        <option value={0}>False</option>
                        <option value={1}>True</option>
                      </select>

                      <button
                        type='button'
                        className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                        onClick={() => remove(index)}
                      >
                        Remove
                      </button>
                    </div>
                  );
                })}
                <button
                  type='button'
                  className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                  onClick={() => append({})}
                >
                  Add answer
                </button>
              </div>
              <input
                type='submit'
                className='block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded hover:cursor-pointer'
              />
            </form>
          </div>
        </div>
      </div>
      {toggle && <UpdateQuestion question={question} setToggle={setToggle} setReload={() => setReload(reload + 1)} r />}
    </div>
  );
}
