import { useContext } from 'react';
import { AuthContext } from '../../../context/AuthContext';
import Card from '../../molecules/Card';
import DashboardCard03 from '../../molecules/DashboardCard03';
import Table from '../../molecules/Table';
import WelcomeBanner from '../../organisms/admin/WelcomeBanner';

export default function Home() {
  const { token } = useContext(AuthContext);
  console.log(token);
  return (
    <div>
      <WelcomeBanner pageTitle='Welcome back' />
      <div className='flex w-full gap-5 h-60 p-4 md:'>
        <DashboardCard03 date='12/2/2022' percent={12} total={2022} header='Number of visiters' />
        <DashboardCard03 date='10/2/2022' percent={20} total={2000} header='Number of documents' />
        <DashboardCard03 date='15/8/2022' percent={20} total={2099} header='Number of quizzes' />
      </div>
    </div>
  );
}
