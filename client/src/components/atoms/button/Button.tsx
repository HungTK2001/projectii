interface ButtonProps {
  /**
   * Type of button
   */
  type?: 'button' | 'submit' | 'reset';
  /**
   * Button contents
   */
  label: string;
  /**
   * Optional style
   */
  properties?: string;
  /**
   * Optional click handler
   */
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const Button = ({ type = 'button', label, properties, ...props }: ButtonProps) => {
  return (
    <button type={type} className={properties} {...props}>
      {label}
    </button>
  );
};
