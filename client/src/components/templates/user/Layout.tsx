import { useContext, useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import Banner from '../../organisms/user/Layout/Banner';
import Copyright from '../../organisms/user/Layout/Copyright';
import Footer from '../../organisms/user/Layout/Footer';
import Navbar from '../../organisms/user/Layout/Navbar';
import NewFooter from '../../organisms/user/Layout/NewFooter';
import Sidebar from '../../organisms/user/Layout/Sidebar';
import { useScrollTo } from 'react-use-window-scroll';
import { AuthContext } from '../../../context/AuthContext';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { userLogin } from '../../../api/user/auth/request';

import '../../pages/user/login.css';
import LoginPage from '../../pages/user/Login';
import SignIn from '../../organisms/user/HomePage/SignIn';
import SignInPage from '../../pages/user/SignInPage';
export default function UserLayout() {
  const [toggleNavbar, setToggleNavbar] = useState(false);
  const [toggleLogin, setToggleLogin] = useState(false);
  const [toggleSignUp, setToggleSignUp] = useState(false);
  const scrollTo = useScrollTo();
  useEffect(() => {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  }, []);

  return (
    <div className='font-ssp cursor-default scroll-auto'>
      <div className='w-full flex'>{toggleNavbar && <Sidebar toggleState={[toggleNavbar, setToggleNavbar]} />}</div>
      <Navbar
        toggleState={[toggleNavbar, setToggleNavbar]}
        toggleLogin={[toggleLogin, setToggleLogin]}
        toggleSignUp={[toggleSignUp, setToggleSignUp]}
      />
      <div className='mb-5 bg-[#f8f5ff]'>
        <Outlet />
      </div>
      <NewFooter />
      <Copyright />
      <button
        className='w-[50px] h-[50px] bg-[#f01f75] fixed bottom-[15px] flex justify-center items-center right-[15px] rounded-md'
        onClick={() => scrollTo({ top: 0, left: 0, behavior: 'smooth' })}
      >
        <i className='bi bi-arrow-up-short text-white text-[2xl] '></i>
      </button>
      {toggleLogin && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <LoginPage setToggleLogin={setToggleLogin} setToggleSignUp={setToggleSignUp} title='' />
        </div>
      )}
      {toggleSignUp && (
        <div className='fixed top-0 left-0 w-full h-full bg-black/60 flex justify-center items-center z-50'>
          <SignInPage setToggleSignUp={setToggleSignUp} />
        </div>
      )}
    </div>
  );
}
