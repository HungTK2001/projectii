import { Outlet } from 'react-router-dom';
import Navbar from '../../organisms/admin/Navbar';
import Sidebar from '../../organisms/admin/Sidebar';

export default function Layout() {
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div className='h-full w-full fixed -z-50 top-0 left-0'></div>
      <div className='lg:ml-[300px] h-screen py-[60px]'>
        <Outlet />
      </div>
    </div>
  );
}
