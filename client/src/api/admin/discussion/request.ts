import axios from 'axios';
const baseAdminURL = `${process.env.REACT_APP_BE_HOST}api/v1/`;

export const getAllDiscussion = async (token: string | null, page: number, limit: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}discussions?page=${page}&limit=${limit}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get list discussion successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const getDiscussionByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}discussions/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get discussion successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteDiscussionByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}discussions/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete discussion successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete discussion!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteCommentByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}comments/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete comment successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete comment!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const getAllDiscussionCategory = async (token: string | null, page: number, limit: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}discussion-categories?page=${page}&limit=${limit}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get list discussion category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const createNewDiscussionCategory = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}discussion-categories`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Create new discussion category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new discussion category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateDiscussionCategoryByID = async (token: string | null, id: number, data: any) => {
  try {
    const result = await axios.patch(`${baseAdminURL}discussion-categories/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Update discussion category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to update discussion category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteDiscussionCategoryByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}discussion-categories/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete discussion category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete discussion category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
