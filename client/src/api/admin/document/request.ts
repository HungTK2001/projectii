import axios from 'axios';
const baseAdminURL = `${process.env.REACT_APP_BE_HOST}api/v1/`;

export const getAllDocument = async (token: string | null, page: number, limit: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}documents?page=${page}&limit=${limit}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get list document successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const getAllDocumentCategory = async (token: string | null, page: number, limit: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}document-categories?page=${page}&limit=${limit}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get list document category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const createNewDocumentCategory = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}document-categories`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Create new document category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new document category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateDocumentByID = async (token: string | null, data: any, id: number) => {
  try {
    const result = await axios.patch(`${baseAdminURL}documents/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Update document successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      console.log(error);
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: error.message
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateDocumentCategoryByID = async (token: string | null, id: number, data: any) => {
  try {
    const result = await axios.patch(`${baseAdminURL}document-categories/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Update document category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to update document category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
export const deleteDocumentByID = async (token: string | null, id: number, fileID: string) => {
  try {
    const result = await axios.delete(`${baseAdminURL}documents?id=${id}&file_id=${fileID}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete document successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete document!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteDocumentCategoryByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}document-categories/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete document category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete document category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
export const createNewDocument = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}documents/`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Create new document successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new document!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const uploadDocument = async (token: string | null, file: FormData) => {
  try {
    const result = await axios.post(`${baseAdminURL}documents/upload`, file, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Upload file successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to upload file!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
