import axios from 'axios';
const baseAdminURL = `${process.env.REACT_APP_BE_HOST}api/v1/`;

export const getAllQuizzes = async (token: string | null, page: number, limit: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}quizzes?page=${page}&limit=${limit}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get list quizzes successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteQuizByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}quizzes/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete quiz successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete quiz!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const createNewQuiz = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}quizzes`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Create new quiz successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new quiz!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
export const checkAnswer = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}quizzes/check-answer`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Check answer successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to check answer!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const createNewQuestion = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}questions`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Create new question successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new question!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const findUserQuizWithQuizID = async (token: string | null, id: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}quizzes/${id}/user-details`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get quiz successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get quiz!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
export const findWithQuizID = async (token: string | null, id: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}quizzes/${id}/details`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get quiz successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get quiz!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteQuestionByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}questions/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete question successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete question!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const getQuestionByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}questions/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get question by ID successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get question!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteAnswerMultiple = async (token: string | null, ids: Array<number>) => {
  try {
    const result = await axios.post(`${baseAdminURL}answers/del-multiple`, ids, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete answer successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to answer question!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateAnswerByID = async (token: string | null, id: number, data: any) => {
  try {
    const result = await axios.patch(`${baseAdminURL}answers/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Update answer successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to update answer!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateQuestionByID = async (token: string | null, id: number, data: any) => {
  try {
    const result = await axios.patch(`${baseAdminURL}questions/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Update question successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to update question!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateQuizByID = async (token: string | null, id: number, data: any) => {
  try {
    const result = await axios.patch(`${baseAdminURL}quizzes/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Update quiz successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to update quiz!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
export const createNewAnswer = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}answers`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Create answer successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new answer!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const getAllQuizCategory = async (token: string | null, page: number, limit: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}quiz-categories?page=${page}&limit=${limit}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Get list quiz category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to get data!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
export const getQuizByUserID = async (token: string | null, userID: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}quizzes/filterBy/${userID}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get quiz by user id successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      console.log(error);
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: error.message
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const getQuizCategoryByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.get(`${baseAdminURL}quiz-categories/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Get quiz category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      console.log(error);
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: error.message
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const createNewQuizCategory = async (token: string | null, data: any) => {
  try {
    const result = await axios.post(`${baseAdminURL}quiz-categories`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Create new quiz category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to create new quiz category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const updateQuizCategoryByID = async (token: string | null, id: number, data: any) => {
  try {
    const result = await axios.patch(`${baseAdminURL}quiz-categories/${id}`, data, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data.data,
      message: 'Update quiz category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to update quiz category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};

export const deleteQuizCategoryByID = async (token: string | null, id: number) => {
  try {
    const result = await axios.delete(`${baseAdminURL}quiz-categories/${id}`, {
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
      }
    });
    return {
      success: true,
      data: result.data,
      message: 'Delete quiz category successful!'
    };
  } catch (error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error?.response?.status === 401) {
        return {
          success: false,
          data: null,
          message: 'Unauthorized'
        };
      } else {
        return {
          success: false,
          data: null,
          message: 'Failed to delete quiz category!'
        };
      }
    } else {
      return {
        success: false,
        message: 'Network error',
        data: null
      };
    }
  }
};
